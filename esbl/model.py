from functools import partial
from typing import Dict, List, Tuple

import diffrax
import jax
import jax.numpy as jnp
import numpy as np

jax.config.update("jax_enable_x64", True)


def scmic(p, tmic=20):
    return p["k1"] * np.exp(p["da"] * p["mu"] * tmic / p["gamma"])


def tolerance(p):
    return p["kb"] * p["bin"] / p["db"]


def sir(p, inoculum=5e-4, time=24, od=0.1):
    ts = np.linspace(0, time)
    control = Experiment(0, inoculum, 1, ts, np.ones(ts.shape))
    sol = control.simulate(p)
    solt = sol.sol(time)
    if solt[0] * solt[1] + solt[5] + solt[6] < od:
        return "X"
    s_i = Experiment(1, inoculum, 1, ts, np.ones(ts.shape))
    sol = s_i.simulate(p)
    solt = sol.sol(time)
    if solt[0] * solt[1] + solt[5] + solt[6] < od:
        return "S"
    i_r = Experiment(2, inoculum, 1, ts, np.ones(ts.shape))
    sol = i_r.simulate(p)
    solt = sol.sol(time)
    if solt[0] * solt[1] + solt[5] + solt[6] < od:
        return "I"
    return "R"


# def yinf_hallwake(g, b, alpha, x, nbterms=10):
#    a = alpha * b / g
#    k = mpmath.qp(1 / alpha)
#    y = 0
#    for n in range(nbterms):
#        y += alpha**n * np.exp(-a * alpha**n * x) / mpmath.qp(alpha, n=n)
#    return a / k * y


def yinf_nodeath(g: float, f: float, l: float) -> float:
    if l < 1 / 2:
        return 0
    if l < 1:
        return (f + g) / f * (l ** (f / g) - 2 ** (-f / g)) / l ** (2 + f / g)
    return (f + g) / f * (1 - 2 ** (-f / g)) / l ** (2 + f / g)


@jax.jit
def ysupx(nu: float, x: float) -> float:
    """Compute the normalized Y_>(x) function."""
    factor = jax.lax.min(x, 1.0) ** nu - jax.lax.min(x / 2.0, 1.0) ** nu
    return jax.lax.clamp(0.0, x - 1.0, 1.0) + x * factor / nu


@jax.jit
def lsupx(nu: float, x: float) -> float:
    """Compute the normalized L_>(x) function."""
    factor = jax.lax.min(x, 1.0) ** nu - jax.lax.min(x / 2.0, 1.0) ** nu
    log2 = jnp.log(2.0)
    return x * jnp.log(jax.lax.clamp(1.0, x, 2.0)) / log2 + x * factor / (nu * log2)


@jax.jit
def model(t: float, y: np.ndarray, params: Dict[str, float]):
    """Compute the RHS of the ODE system."""
    n, l, s, a, b, c, cr = y
    s = jax.lax.max(s, 0.0)
    g = params["mu"] * s / (params["ks"] + s)
    # g = jax.lax.max(g, 0.0)
    nu = params["beta"] / g
    f = params["beta"] / (1.0 + (a / params["k1"]) ** params["h1"])
    l0 = (1.0 + params["mu"] / params["beta"]) * jnp.log(2.0)
    # lm = params["lmin"] + (params["lmax"] - params["lmin"]) / (1 + a / params["k2"])
    lm = params["lmax"] * ((params["k2"] + a * params["zeta"]) / (params["k2"] + a))
    # lm = params["lmax"] * ((params["k1"] + a * params["zeta"]) / (params["k1"] + a))
    ysup = ysupx(nu, l / (l0 * lm))
    lsup = l0 * lm * lsupx(nu, l / (l0 * lm))
    dndt = n * (f * (l / jnp.log(2.0) - 1.0) - params["gamma"] * ysup)
    dldt = l * (g - f * (l / jnp.log(2.0) - 1.0)) - params["gamma"] * (lsup - l * ysup)
    dsdt = -g / params["lambda"] * n * l
    dadt = -params["kb"] * b * a - params["da"] * a
    dbdt = params["gamma"] * params["bin"] * n * lsup - params["db"] * b
    dcdt = params["gamma"] * (1.0 - params["pc"]) * n * lsup - params["dc"] * c
    dcrdt = params["gamma"] * params["pc"] * n * lsup
    return jnp.stack([dndt, dldt, dsdt, dadt, dbdt, dcdt, dcrdt])




@jax.jit
def make_y0(a0: float, od0: float, s0: float, params: Dict[str, float]):
    """Create a vector of the initial condition."""
    l0 = jnp.log(2.0) * (1.0 + params["mu"] / params["beta"])
    n0 = od0 / l0
    b0, c0, cr0 = 0.0, 0.0, 0.0
    y0 = jnp.array([n0, l0, s0, a0, b0, c0, cr0])
    return y0


@partial(jax.jit, static_argnames=["return_yf", "adaptive"])
def _simulate_no_injs(
    y0: jnp.ndarray,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    params: Dict[str, float],
    return_yf: bool,
    adaptive: bool,
):
    terms = diffrax.ODETerm(model)
    solver = diffrax.Tsit5()
    saveat = diffrax.SaveAt(ts=times, t1=return_yf)
    pid_control = diffrax.PIDController(rtol=1e-6, atol=1e-6)
    constant_steps = diffrax.ConstantStepSize()
    sol = diffrax.diffeqsolve(
        terms=terms,
        solver=solver,
        t0=t0t1[0],
        t1=t0t1[1],
        dt0=None if adaptive else 0.0002,
        max_steps=int(1e6),
        y0=y0,
        args=params,
        saveat=saveat,
        stepsize_controller=pid_control if adaptive else constant_steps,
    )
    if return_yf:
        return sol.ys[:-1].T, sol.ys[-1]
    return sol.ys.T


@partial(jax.jit, static_argnames=["adaptive"])
def _simulate(
    y0: jnp.ndarray,
    times: List[np.ndarray],
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    oldt = 0.0
    tmax = times[-1][-1]
    yss = []
    for tms, tinj, vinj in zip(times, timesinjs, volsinjs):
        ys, yf = _simulate_no_injs(
            y0, (oldt, tinj), tms, params, return_yf=True, adaptive=adaptive
        )
        yss.append(ys)
        n0, l0, sf, af, bf, c0, cr0 = yf
        s0 = sf * v0 / (v0 + vinj)
        b0 = bf * v0 / (v0 + vinj)
        a0 = (af * v0 + 1000.0 * concinj * vinj) / (v0 + vinj)
        v0 = v0 + vinj
        oldt = tinj
        y0 = jnp.array([n0, l0, s0, a0, b0, c0, cr0])
    ys = _simulate_no_injs(
        y0, (oldt, tmax), times[-1], params, return_yf=False, adaptive=adaptive
    )
    yss.append(ys)
    return jnp.hstack(yss)



def simulate_no_injs(
    y0: jnp.array,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    params: Dict[str, float],
):
    try:
        return _simulate_no_injs(
            y0, t0t1, times, params, return_yf=False, adaptive=True
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _simulate_no_injs(
            y0, t0t1, times, params, return_yf=False, adaptive=False
        )


def simulate(
    y0: jnp.array,
    times: List[np.ndarray],
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
):
    try:
        return _simulate(
            y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=True
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _simulate(
            y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=False
        )

### residuals based on OD ###

@partial(jax.jit, static_argnames=["adaptive"])
def _residuals_no_injs(
    y0: jnp.ndarray,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    lods: np.ndarray,
    stdlods: np.ndarray,
    params: Dict[str, float],
    adaptive: bool,
):
    sol = _simulate_no_injs(y0, t0t1, times, params, return_yf=False, adaptive=adaptive)
    ods_sim = sol[0] * sol[1] + sol[5] + sol[6]
    res = (jnp.log(ods_sim) - lods) / stdlods
    return res


def residuals_no_injs(
    y0: jnp.ndarray,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    lods: np.ndarray,
    stdlods: np.ndarray,
    params: Dict[str, float],
):
    try:
        return _residuals_no_injs(y0, t0t1, times, lods, stdlods, params, adaptive=True)
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _residuals_no_injs(
            y0, t0t1, times, lods, stdlods, params, adaptive=False
        )


@partial(jax.jit, static_argnames=["adaptive"])
def _residuals(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    sol = _simulate(
        y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=adaptive
    )
    ods_sim = sol[0] * sol[1] + sol[5] + sol[6]
    res = (jnp.log(ods_sim) - lods) / stdlods
    return res / jnp.sqrt(len(res))


def residuals(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
):
    try:
        return _residuals(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=True,
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _residuals(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=False,
        )

### general OD cost functions ###

@partial(jax.jit, static_argnames=["adaptive"])
def _cost_no_injs(
    y0: jnp.ndarray,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    lods: np.ndarray,
    stdlods: np.ndarray,
    params: Dict[str, float],
    adaptive: bool,
):
    res = _residuals_no_injs(y0, t0t1, times, lods, stdlods, params, adaptive=adaptive)
    return 0.5 * jnp.sum(res**2)


def cost_no_injs(
    y0: np.ndarray,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    lods: np.ndarray,
    stdlods: np.ndarray,
    params: Dict[str, float],
) -> float:
    try:
        cst = _cost_no_injs(y0, t0t1, times, lods, stdlods, params, adaptive=True)
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        cst = _cost_no_injs(y0, t0t1, times, lods, stdlods, params, adaptive=False)
    return float(cst)


@partial(jax.jit, static_argnames=["adaptive"])
def _cost(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    res = _residuals(
        y0,
        times,
        lods,
        stdlods,
        timesinjs,
        volsinjs,
        concinj,
        v0,
        params,
        adaptive=adaptive,
    )
    return 0.5 * jnp.sum(res**2)

def cost(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
):
    try:
        cst = _cost(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=True,
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        cst = _cost(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=False,
        )
    return float(cst)

### CFU ###
bounds_cfus = (10 / 5 * 200, 25e6 / 5 * 200)

@partial(jax.jit, static_argnames=["adaptive"])
def _residuals_cfus(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timescfus: np.ndarray,
    lcfus: np.ndarray,
    stdlcfus: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    sol = _simulate(
        y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=adaptive
    )
    # select positions for comparison
    times_flat = jnp.concatenate(times)
    args = jnp.argmin(jnp.abs(jnp.repeat(timescfus.reshape((timescfus.size, 1)),  times_flat.size, axis = 1) - times_flat), axis = 1)
    cfu_sim = sol[0][args] / params["eta"]
    # cfu_sim_1 = np.max(cfu_sim, np.full(cfu_sim.shape, bounds_cfus[0]))
    # cfu_sim_2 = np.min(cfu_sim_1, np.full(cfu_sim_1.shape, bounds_cfus[1]))
    res_cfu = (jnp.log(cfu_sim) - lcfus) #/ stdlcfus
    res_cfu = res_cfu  / jnp.sqrt(len(res_cfu))
    return res_cfu


def residuals_cfus(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timescfus: np.ndarray,
    lcfus: np.ndarray,
    stdlcfus: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float]
):
    try:
        return _residuals_cfus(y0, times, lods, stdlods, timescfus, lcfus, stdlcfus, timesinjs, volsinjs, concinj, v0, params, adaptive=True)
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _residuals_cfus(y0, times, lods, stdlods, timescfus, lcfus, stdlcfus, timesinjs, volsinjs, concinj, v0,
                               params, adaptive=False)


@partial(jax.jit, static_argnames=["adaptive"])
def _cost_cfus(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    timescfus: np.ndarray,
    lcfus: np.ndarray,
    stdlcfus: np.ndarray,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    res = _residuals_cfus(
        y0,
        times,
        lods,
        stdlods,
        timescfus,
        lcfus,
        stdlcfus,
        timesinjs,
        volsinjs,
        concinj,
        v0,
        params,
        adaptive=adaptive,
    )
    return 0.5 * jnp.sum(res**2)

def cost_cfus(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    timescfus: np.ndarray,
    lcfus: np.ndarray,
    stdlcfus: np.ndarray,
    v0: float,
    params: Dict[str, float],
):
    try:
        cst = _cost_cfus(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            timescfus,
            lcfus,
            stdlcfus,
            v0,
            params,
            adaptive=True,
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        cst = _cost_cfus(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            timescfus,
            lcfus,
            stdlcfus,
            v0,
            params,
            adaptive=False,
        )
    return float(cst)



### rich media ###
'''
during the exponential growth phase: initial model 
when OD (or N ? )  reaches begining of saturation phase, switch to Baranyi model: 
Total biomass = still growing subpopulation + subpopulation that is already in steady state  
'''


# def OD_baranyi (t, mu, nu, x0, xsat) :
#     '''
#     total biomass after beginning of saturation x = x_g (growing) + x_ng (not growing)
#     x_g' = mu * x_g - nu * x_g
#     x_ng' = nu * x_g
#     x_g(t=0) = x0; x_ng(t=0) = 0
#     '''
#     x = x0 * np.exp(mu * t)
#     t1 = np.log(xsat / x0) / mu
#     x[x > xsat] = xsat / (mu  - nu) * (- nu + mu * np.exp((mu - nu) * (t[x > xsat] - t1)))
#     return x

#params_rich_media = initial params + ['musat', 'ODsat']

@jax.jit
def model_rich_media (t: float, y: np.ndarray, params: Dict[str, float]):
    """Compute the RHS of the ODE system."""
    n, l, s, a, b, c, cr = y
    s = jax.lax.max(s, 0.0)

    # monod
    # g = params["mu"] * s / (params["ks"] + s)

    # gompertz
    # g = params["mu"]  * jnp.exp(- params['kappa'] * t)

    #logistic
    g = params["mu"] * (params['kappa'] - n * l) / params['kappa']


    # g = jax.lax.max(g, 0.0)
    nu = params["beta"] / g
    f = params["beta"] / (1.0 + (a / params["k1"]) ** params["h1"])
    l0 = (1.0 + params["mu"] / params["beta"]) * jnp.log(2.0)
    # lm = params["lmin"] + (params["lmax"] - params["lmin"]) / (1 + a / params["k2"])
    lm = params["lmax"] * ((params["k2"] + a * params["zeta"]) / (params["k2"] + a))
    ysup = ysupx(nu, l / (l0 * lm))
    lsup = l0 * lm * lsupx(nu, l / (l0 * lm))

    dndt = n * (f * (l / jnp.log(2.0) - 1.0) - params["gamma"] * ysup)

    # n = ng + nsat
    # dndt = dngdt + dnsatdt
    # if (ng + nsat <= params['nsat']) :
    #     dngdt = ng * (f * (l / jnp.log(2.0) - 1.0) - params["gamma"] * ysup)
    #     dnsatdt = 0
    # else:
    #     dngdt = ng * (f * (l / jnp.log(2.0) - 1.0) - params["gamma"] * ysup - params['musat'])
    #     dnsatdt = params['musat'] * ng

    dldt = l * (g - f * (l / jnp.log(2.0) - 1.0)) - params["gamma"] * (lsup - l * ysup)
    dsdt = -g / params["lambda"] * n * l
    dadt = -params["kb"] * b * a - params["da"] * a
    dbdt = params["gamma"] * params["bin"] * n * lsup - params["db"] * b
    dcdt = params["gamma"] * (1.0 - params["pc"]) * n * lsup - params["dc"] * c
    dcrdt = params["gamma"] * params["pc"] * n * lsup
    return jnp.stack([dndt, dldt, dsdt, dadt, dbdt, dcdt, dcrdt])



@partial(jax.jit, static_argnames=["return_yf", "adaptive"])
def _simulate_no_injs_rich_media(
    y0: jnp.ndarray,
    t0t1: Tuple[float, float],
    times: np.ndarray,
    params: Dict[str, float],
    return_yf: bool,
    adaptive: bool,
):
    terms = diffrax.ODETerm(model_rich_media)
    solver = diffrax.Tsit5()
    saveat = diffrax.SaveAt(ts=times, t1=return_yf)
    pid_control = diffrax.PIDController(rtol=1e-3, atol=1e-3) #rtol=1e-6, atol=1e-6)
    constant_steps = diffrax.ConstantStepSize()
    sol = diffrax.diffeqsolve(
        terms=terms,
        solver=solver,
        t0=t0t1[0],
        t1=t0t1[1],
        dt0= None if adaptive else 0.0002,
        max_steps=int(1e9), #1e6
        y0=y0,
        args=params,
        saveat=saveat,
        stepsize_controller=pid_control if adaptive else constant_steps,
    )
    if return_yf:
        return sol.ys[:-1].T, sol.ys[-1]
    return sol.ys.T

@partial(jax.jit, static_argnames=["adaptive"])
def _simulate_rich_media(
    y0: jnp.ndarray,
    times: List[np.ndarray],
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    oldt = 0.0
    tmax = times[-1][-1]
    yss = []
    for tms, tinj, vinj in zip(times, timesinjs, volsinjs):
        ys, yf = _simulate_no_injs_rich_media(
            y0, (oldt, tinj), tms, params, return_yf=True, adaptive=adaptive
        )
        yss.append(ys)
        n0, l0, sf, af, bf, c0, cr0 = yf
        s0 = sf * v0 / (v0 + vinj)
        b0 = bf * v0 / (v0 + vinj)
        a0 = (af * v0 + 1000.0 * concinj * vinj) / (v0 + vinj)
        v0 = v0 + vinj
        oldt = tinj
        y0 = jnp.array([n0, l0, s0, a0, b0, c0, cr0])
    ys = _simulate_no_injs_rich_media(
        y0, (oldt, tmax), times[-1], params, return_yf=False, adaptive=adaptive
    )
    yss.append(ys)
    return jnp.hstack(yss)

def simulate_rich_media(
    y0: jnp.array,
    times: List[np.ndarray],
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
):
    try:
        return _simulate_rich_media(
            y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=True
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _simulate_rich_media(
            y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=False
        )


@partial(jax.jit, static_argnames=["adaptive"])
def _residuals_rich_media(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    sol = _simulate_rich_media(
        y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=adaptive
    )
    ods_sim = sol[0]* sol[1] + sol[5] + sol[6]
    res = (jnp.log(ods_sim) - lods) / stdlods
    return res / jnp.sqrt(len(res))

def residuals_rich_media(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
):
    try:
        return _residuals_rich_media(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=True,
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _residuals_rich_media(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=False,
        )

@partial(jax.jit, static_argnames=["adaptive"])
def _residuals_rich_media_cfus(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timescfus: np.ndarray,
    lcfus: np.ndarray,
    stdlcfus: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    sol = _simulate_rich_media(
        y0, times, timesinjs, volsinjs, concinj, v0, params, adaptive=adaptive
    )
    # select positions for comparison
    times_flat = jnp.concatenate(times)
    args = jnp.argmin(jnp.abs(jnp.repeat(timescfus.reshape((timescfus.size, 1)),  times_flat.size, axis = 1) - times_flat), axis = 1)
    cfu_sim = sol[0][args] / params["eta"]
    # cfu_sim_1 = np.max(cfu_sim, np.full(cfu_sim.shape, bounds_cfus[0]))
    # cfu_sim_2 = np.min(cfu_sim_1, np.full(cfu_sim_1.shape, bounds_cfus[1]))
    res_cfu = (jnp.log(cfu_sim) - lcfus) #/ stdlcfus
    res_cfu = res_cfu  / jnp.sqrt(len(res_cfu))
    return res_cfu


def residuals_rich_media_cfus(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timescfus: np.ndarray,
    lcfus: np.ndarray,
    stdlcfus: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float]
):
    try:
        return _residuals_rich_media_cfus(y0, times, lods, stdlods, timescfus, lcfus, stdlcfus, timesinjs, volsinjs, concinj, v0, params, adaptive=True)
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        return _residuals_rich_media_cfus(y0, times, lods, stdlods, timescfus, lcfus, stdlcfus, timesinjs, volsinjs, concinj, v0,
                               params, adaptive=False)


@partial(jax.jit, static_argnames=["adaptive"])
def _cost_rich_media(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
    adaptive: bool,
):
    res = _residuals_rich_media(
        y0,
        times,
        lods,
        stdlods,
        timesinjs,
        volsinjs,
        concinj,
        v0,
        params,
        adaptive=adaptive,
    )
    return 0.5 * jnp.sum(res**2)

def cost_rich_media(
    y0: jnp.array,
    times: List[np.ndarray],
    lods: np.ndarray,
    stdlods: np.ndarray,
    timesinjs: List[float],
    volsinjs: List[float],
    concinj: float,
    v0: float,
    params: Dict[str, float],
):
    try:
        cst = _cost_rich_media(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=True,
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        cst = _cost_rich_media(
            y0,
            times,
            lods,
            stdlods,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=False,
        )
    return float(cst)



@partial(jax.jit, static_argnames=["adaptive"])
def _cost_rich_media_cfus(
                    y0: jnp.array,
                    times: List[np.ndarray],
                    lods: np.ndarray,
                    stdlods: np.ndarray,
                    timescfus: np.ndarray,
                    lcfus: np.ndarray,
                    stdlcfus: np.ndarray,
                    timesinjs: List[float],
                    volsinjs: List[float],
                    concinj: float,
                    v0: float,
                    params: Dict[str, float],
                    adaptive: bool
                ):
    res = _residuals_rich_media_cfus(
        y0,
        times,
        lods,
        stdlods,
        timescfus,
        lcfus,
        stdlcfus,
        timesinjs,
        volsinjs,
        concinj,
        v0,
        params,
        adaptive=adaptive,
    )
    return 0.5 * jnp.sum(res ** 2)


def cost_rich_media_cfus(
                    y0: jnp.array,
                    times: List[np.ndarray],
                    lods: np.ndarray,
                    stdlods: np.ndarray,
                    timescfus: np.ndarray,
                    lcfus: np.ndarray,
                    stdlcfus: np.ndarray,
                    timesinjs: List[float],
                    volsinjs: List[float],
                    concinj: float,
                    v0: float,
                    params: Dict[str, float],
                ):
    try:
        cst = _cost_rich_media_cfus(
            y0,
            times,
            lods,
            stdlods,
            timescfus,
            lcfus,
            stdlcfus,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=True,
        )
    except ValueError:
        print("Adaptive solver failed, retrying with fixed steps.")
        cst = _cost_rich_media_cfus(
            y0,
            times,
            lods,
            stdlods,
            timescfus,
            lcfus,
            stdlcfus,
            timesinjs,
            volsinjs,
            concinj,
            v0,
            params,
            adaptive=False,
        )
    return float(cst)