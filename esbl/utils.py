import numpy as np
import pandas as pd

import esbl


def savefig(fig, name, path="docs/thesis/figures/", ext="pdf"):
    fig.savefig(esbl.ROOT / path / f"{name}.{ext}", bbox_inches="tight")


def texplot():
    rc("font", family="serif", serif=["Computer Modern Roman"], size=16)
    rc("text", usetex=True)
    rc(
        "text.latex",
        preamble="\n".join(
            [
                r"\usepackage{physics}",
                r"\usepackage{stmaryrd}",
                r"\usepackage{textgreek}",
                r"\usepackage{siunitx}",
            ]
        ),
    )


def plt_reset_colors(ax=None):
    if ax:
        ax.set_prop_cycle(None)
    else:
        plt.gca().set_prop_cycle(None)


def smooth(ys):
    threshold = 0.002
    peaks = ((ys[1:-1] - ys[:-2]) > threshold) & ((ys[1:-1] - ys[2:]) > threshold)
    avgs = (ys[:-2] + ys[2:]) / 2
    ys2 = np.array(ys)
    ys2[1:-1][peaks] = avgs[peaks]
    return ys2
    # return savgol_filter(ys2, 5, 2)
    # spl = UnivariateSpline(xs, ys)
    # spl.set_smoothing_factor(0)
    # xxs = np.linspace(0, max(xs), 1000)
    # yys = spl(xxs)
    # # yys2 = yys
    # yys2 = savgol_filter(yys, 25, 2)
    # spl2 = UnivariateSpline(xxs, yys2)
    # spl2.set_smoothing_factor(0)
    # return spl2(xs)
    # assert xs.shape == ys.shape
    # spl = UnivariateSpline(xs, ys)
    # if factor is not None:
    #     spl.set_smoothing_factor(factor)
    # return spl(xs)


def remove_background(ods, od0=0.05 / 100, avgrange=None, factor=1.5, peaks=True):
    if not avgrange:
        avgrange = range(3, 8)
    shifted = ods - ods[avgrange].mean(axis=0) + od0 * factor
    if peaks:
        return smooth(shifted)
    return shifted


def read_excel(name):
    data = np.array(pd.read_excel(name))
    start = np.where(data[:, 0] == "Cycle Nr.")[0][0] + 1
    stop = np.where(data[:, 0] == "End Time")[0][-1] - 1
    times = np.array(data[start:stop, 1], dtype=np.float)
    plates = np.array(data[start:stop, 3:], dtype=np.float)
    return times / 3600, plates


def read_abs(path="."):
    datas = []
    for p in sorted(Path(path).glob("20*_abs.csv")):
        with open(p, "r") as f:
            d = np.loadtxt(f, delimiter=",")[::3]
        datas.append(d)
    data = np.concatenate(datas)
    times = (data[:, 0] - data[0, 0]) / 3600
    plates = data[:, 1:]
    return times, plates
