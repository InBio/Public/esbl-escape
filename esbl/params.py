import copy
from typing import Dict, List, Optional

import jax
import numpy as np
import numpy.random as npr

BOUNDS = {
    "gamma": ("lin", 0, 20),
    "beta": ("lin", 0, 20),
    "mu": ("lin", 0, 2),
    "ks": ("log", 1e-4, 1e-3),
    "lambda": ("log", 1e6, 1e10),
    "k1": ("log", 1e-3, 1e4),
    "h1": ("lin", 0, 10),
    "k2": ("log", 1e-3, 1e4),
    "bin": ("lin", 0, 2e-8),
    "kb": ("log", 1e2, 1e7),
    "da": ("lin", 0, 0.1),
    "db": ("lin", 0, 20),
    "dc": ("lin", 0, 20),
    "pc": ("lin", 0, 1),
    "zeta": ("lin", 0, 1),
    "lmax": ("lin", 0, 500),
    "eta": ("lin", 0, 2e-7),
}

BOUNDS_rich_media = BOUNDS | {"kappa" : ('lin', 0, 20)}


NICENAMES = {
    "gamma": r"$\gamma$",
    "beta": r"$\beta$",
    "mu": r"$\mu$",
    "ks": r"$K_\mathrm{s}$",
    "lambda": r"$\lambda$",
    "k1": r"$k_1$",
    "k2": r"$k_2$",
    "h1": r"$h_1$",
    "bin": r"$B_\mathrm{in}$",
    "kb": r"$k_\mathrm{b}$",
    "da": r"$d_\mathrm{a}$",
    "db": r"$d_\mathrm{b}$",
    "dc": r"$d_\mathrm{c}$",
    "pc": r"$p_\mathrm{c}$",
    "zeta": r"$\zeta$",
    "lmax": r"$L_\mathrm{max}$",
    "eta": r"$\eta$",
}


class ParamScaler:
    """
    Class to transform the 17 "real-world" parameter dictionary into a
    (possibly shorter) vector of parameters adapted for search.  It applies the
    linear or logarithmic transformations specified in `BOUNDS` and fixes
    values of fixed parameters.
    """

    def __init__(self, **kwargs):
        print(
            f"Initializing ParamScaler with {len(BOUNDS) - len(kwargs)} "
            f"variables and {len(kwargs)} fixed parameters."
        )
        self.fixed = {}
        for name, value in kwargs.items():
            if name not in BOUNDS:
                raise ValueError(f"{name} is not a parameter")
            self.fixed[name] = float(value)

    def random(self, rng: npr.Generator) -> Dict[str, float]:
        return self.detransform(10 * rng.random(len(BOUNDS) - len(self.fixed)))

    def middle(self) -> Dict[str, float]:
        return self.detransform([5.0] * (len(BOUNDS) - len(self.fixed)))

    def transform(self, param_dict: Dict[str, float]) -> List[float]:
        """
        "human" (dict) -> "machine" (list)
        """
        param_list = []
        for name, (scale, lbound, ubound) in BOUNDS.items():
            value = float(param_dict[name])
            if name in self.fixed and value != self.fixed[name]:
                raise ValueError(
                    f"{name} is fixed to {self.fixed[name]}, value {value} given"
                )
            if not (lbound <= value <= ubound):
                raise ValueError(
                    f"{name} is not in [{lbound}, {ubound}], value {value} given"
                )
            if scale == "lin":
                x = 10 * (value - lbound) / (ubound - lbound)
            elif scale == "log":
                x = 10 * np.log(value / lbound) / np.log(ubound / lbound)
            if name not in self.fixed:
                param_list.append(x)
        return param_list

    def detransform(self, param_list: List[float]) -> Dict[str, float]:
        """
        "machine" (list) -> "human" (dict)
        """
        # param_list_copy = copy.deepcopy(param_list)
        # for key in self.fixed:
        #     if key in param_list_copy:
        #         del param_list_copy[key]
        assert len(param_list) == len(BOUNDS) - len(self.fixed)
        if not all(0 <= x <= 10 for x in param_list):
            raise ValueError(f"All values must be in [0, 10]: {param_list}")
        return detransform(param_list, self.fixed)

    def last_cmaes(self) -> Dict[str, float]:
        """
        Read the last cmaes result and return a detransformed version.
        """
        xrecentbest = np.loadtxt("outcmaes/xrecentbest.dat", skiprows=1)
        return self.detransform(xrecentbest[-1, 5:])

    def detransform_curvatures(self, paramsx, hessianx) -> dict:
       """
       Curvatures of the cost function
       """
       curvatures = {}
       # for name, x in zip([n for n in BOUNDS if n not in fixed])
       for i, name in enumerate([n for n in BOUNDS if n not in self.fixed]):
           scale, lbound, ubound = BOUNDS[name]
           # if name == "lmin" and "lmax" in self.fixed:
           #     b = min(b, self.fixed["lmax"])
           if scale == "lin":
               cur = hessianx[i, i] * (10 / (ubound - lbound)) ** 2
           elif scale == "log":
               value = lbound * (ubound / lbound) ** (paramsx[i] / 10)
               cur = hessianx[i, i] * (10 / (value * np.log(ubound / lbound))) ** 2
           curvatures[name] = float(cur)
       return curvatures

    def detransform_covariances(self, paramsx, hessianx) -> dict:
       """
       Covariance matrix of the cost function
       """
       hessian = np.array(hessianx)
       for i, name in enumerate([n for n in BOUNDS if n not in self.fixed]):
           scale, lbound, ubound = BOUNDS[name]
           # if name == "lmin" and "lmax" in self.fixed:
           #     b = min(b, self.fixed["lmax"])
           if scale == "lin":
               factor = 10 / (ubound - lbound)
           elif scale == "log":
               v = lbound * (ubound / lbound) ** (paramsx[i] / 10)
               factor = 10 / (value * np.log(ubound / lbound))
           hessian[:, i] *= factor
           hessian[i, :] *= factor
       cov_mat = np.linalg.pinv(-hessian)
       covariances = {}
       for i, namei in enumerate([n for n in BOUNDS if n not in self.fixed]):
           for j, namej in enumerate([n for n in BOUNDS if n not in self.fixed]):
               covariances[namei, namej] = cov_mat[i, j]
       return covariances

    def detransform_correlations(self, paramsx, hessianx) -> dict:
       """
       Correlation matrix of the cost function
       """
       hessian = np.array(hessianx)
       for i, name in enumerate([n for n in PARAMLIST if n not in self.fixed]):
           scale, lbound, ubound = BOUNDS[name]
           # if name == "lmin" and "lmax" in self.fixed:
           #     b = min(b, self.fixed["lmax"])
           if scale == "lin":
               factor = 10 / (ubound - lbound)
           elif scale == "log":
               v = lbound * (ubound / lbound) ** (paramsx[i] / 10)
               factor = 10 / (value * np.log(ubound / lbound))
           hessian[:, i] *= factor
           hessian[i, :] *= factor
       cov_mat = np.linalg.pinv(hessian)
       rsqdiag = np.diag(1 / np.sqrt(np.diag(cov_mat)))
       corr_mat = rsqdiag.dot(cov_mat).dot(rsqdiag)
       correlations = {}
       for i, namei in enumerate([n for n in BOUNDS if n not in self.fixed]):
           for j, namej in enumerate([n for n in BOUNDS if n not in self.fixed]):
               correlations[namei, namej] = corr_mat[i, j]
       return correlations


@jax.jit
def detransform(
    param_list: List[float], fixed: Optional[Dict[str, float]] = None
) -> Dict[str, float]:
    """
    "machine" (list) -> "human" (dict)
    """
    fixed = fixed or {}
    param_dict = dict(fixed)
    for name, x in zip([n for n in BOUNDS if n not in fixed], param_list):
        scale, lbound, ubound = BOUNDS[name]
        if scale == "lin":
            value = lbound + (ubound - lbound) * x / 10
        elif scale == "log":
            value = lbound * (ubound / lbound) ** (x / 10)
        param_dict[name] = value
    return param_dict



class ParamScaler_rich_media (ParamScaler):

    def __init__(self, **kwargs):
        print(
            f"Initializing ParamScaler with {len(BOUNDS) - len(kwargs)} "
            f"variables and {len(kwargs)} fixed parameters."
        )
        self.fixed = {}
        for name, value in kwargs.items():
            if name not in BOUNDS_rich_media:
                raise ValueError(f"{name} is not a parameter")
            self.fixed[name] = float(value)

    def random(self, rng: npr.Generator) -> Dict[str, float]:
        return self.detransform(10 * rng.random(len(BOUNDS_rich_media) - len(self.fixed)))

    def middle(self) -> Dict[str, float]:
        return self.detransform([5.0] * (len(BOUNDS_rich_media) - len(self.fixed)))

    def transform(self, param_dict: Dict[str, float]) -> List[float]:
        """
        "human" (dict) -> "machine" (list)
        """
        param_list = []
        for name, (scale, lbound, ubound) in BOUNDS_rich_media.items():
            value = float(param_dict[name])
            if name in self.fixed and value != self.fixed[name]:
                raise ValueError(
                    f"{name} is fixed to {self.fixed[name]}, value {value} given"
                )
            if not (lbound <= value <= ubound):
                raise ValueError(
                    f"{name} is not in [{lbound}, {ubound}], value {value} given"
                )
            if scale == "lin":
                x = 10 * (value - lbound) / (ubound - lbound)
            elif scale == "log":
                x = 10 * np.log(value / lbound) / np.log(ubound / lbound)
            if name not in self.fixed:
                param_list.append(x)
        return param_list

    def detransform(self, param_list: List[float]) -> Dict[str, float]:
        """
        "machine" (list) -> "human" (dict)
        """
        # param_list_copy = copy.deepcopy(param_list)
        # for key in self.fixed:
        #     if key in param_list_copy:
        #         del param_list_copy[key]
        assert len(param_list) == len(BOUNDS_rich_media) - len(self.fixed)
        if not all(0 <= x <= 10 for x in param_list):
            raise ValueError(f"All values must be in [0, 10]: {param_list}")


        return detransform_rich_media(param_list, self.fixed)


@jax.jit
def detransform_rich_media(
    param_list: List[float], fixed: Optional[Dict[str, float]] = None
) -> Dict[str, float]:
    """
    "machine" (list) -> "human" (dict)
    """
    fixed = fixed or {}
    param_dict = dict(fixed)
    for name, x in zip([n for n in BOUNDS_rich_media if n not in fixed], param_list):
        scale, lbound, ubound = BOUNDS_rich_media[name]
        if scale == "lin":
            value = lbound + (ubound - lbound) * x / 10
        elif scale == "log":
            value = lbound * (ubound / lbound) ** (x / 10)
        param_dict[name] = value
    return param_dict
