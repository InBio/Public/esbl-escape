import multiprocessing as mp
import sys
from pathlib import Path

import cma
import diffrax
import jax
import jax.numpy as jnp
import matplotlib.pyplot as plt
import mpmath
import numpy as np
import pandas as pd
import pymc as pm
import scipy.integrate as ode
import scipy.optimize as opt
import scipy.stats as st
import yaml
from matplotlib import rc
from matplotlib.lines import Line2D

# ROOT = Path("/mnt/c/Users/vgross/PhD/data/results/esbl-2.0")

from . import model, params, utils
from .params import BOUNDS, ParamScaler, ParamScaler_rich_media, BOUNDS_rich_media

# with open(ROOT / "data/20190923_cefotaxime_ib31/ib31.yaml", "r") as file:
#     PIB31 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20190923_cefotaxime_ib31/ib31_onlyod.yaml", "r") as file:
#     PIB31OD = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20190909_cefotaxime/ib32.yaml", "r") as file:
#     PIB32 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20190909_cefotaxime/ib32_onlyod.yaml", "r") as file:
#     PIB32OD = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191127_ib34-35_ctx/ib34.yaml", "r") as file:
#     PIB34 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191127_ib34-35_ctx/ib35.yaml", "r") as file:
#     PIB35 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191120_ib34-37_ctx/ib37.yaml", "r") as file:
#     PIB37 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191123_ib38-311_ctx/ib38.yaml", "r") as file:
#     PIB38 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191123_ib38-311_ctx/ib39.yaml", "r") as file:
#     PIB39 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191123_ib38-311_ctx/ib310.yaml", "r") as file:
#     PIB310 = {k: float(v) for k, v in yaml.safe_load(file).items()}
#
# with open(ROOT / "data/20191123_ib38-311_ctx/ib311.yaml", "r") as file:
#     PIB311 = {k: float(v) for k, v in yaml.safe_load(file).items()}

# with open(ROOT / "src/esbl/esbl/super.yaml", "r") as file:
#     PSUPER = {k: float(v) for k, v in yaml.safe_load(file).items()}


def pl1(exps, name, *args, **kwargs):
    return exps.profile_likelihood_one(name, *args, **kwargs)


class Experiments:
    def __init__(self, exps_list=None):
        self.exps = [] if not exps_list else exps_list

    def __iter__(self):
        for exp in self.exps:
            yield exp

    def add(self, exp):
        self.exps.append(exp)

    def residuals(self, params, odejax: bool = False, include_rich_media: bool = False):
        return np.concatenate(
            [exp.residuals(params, odejax=odejax, include_rich_media=include_rich_media) for exp in self.exps]
        ) / np.sqrt(len(self.exps))

    def residuals_with_cfu(self, params, odejax: bool = False, include_rich_media: bool = False):
        return np.concatenate(
            [exp.residuals_with_cfu(params, odejax=odejax, include_rich_media=include_rich_media) for exp in self.exps]
        ) / np.sqrt(len(self.exps))


    def cost(self, params, odejax: bool = False, include_rich_media: bool = False):
        return np.mean([exp.cost(params, odejax=odejax, include_rich_media=include_rich_media) for exp in self.exps])

    def cost_with_cfu(self, params, odejax: bool = False, include_rich_media: bool = False):
        return np.mean([exp.cost(params, odejax = odejax, include_rich_media=include_rich_media) +
                        exp.cost_cfu(params, odejax = odejax, include_rich_media=include_rich_media) for exp in self.exps] )

    def _fake_minimize(self, x0, ps, *args, **kwargs):
        res = opt.least_squares(
            lambda x: self.residuals(ps.detransform(x)),
            x0,
            bounds=(0, 10),
            verbose=0,
            *args,
            **kwargs,
        )
        res.fun = res.cost
        return res

    def search(
        self,
        params=None,
        method="cmaes",
        sigma=2,
        fixed=None,
        samples=20,
        save=True,
        verbose=2,
        odejax: bool = False,
        include_cfus: bool = False,
        include_rich_media : bool = False,
        media_condition=None, verb_filenameprefix=None,
        *args,
        **kwargs,
    ):
        if fixed is None:
            fixed = {}
        if include_rich_media:
            ps = ParamScaler_rich_media(**fixed)
        else:
            ps = ParamScaler(**fixed)
        if params is None:
            params = ps.middle()
        exp0 = [exp for exp in self.exps if exp.a0 == 0][0]
        if include_cfus and 'eta' not in fixed.keys() and exp0.timescfus.size > 0:
            eta0 = exp0.ods[np.where(np.abs(exp0.times - exp0.timescfus[0]) < 0.25)[0][0]] / exp0.cfus[0]
            params['eta'] = eta0
        x0 = ps.transform(params)
        # print(x0)
        # print(ps.fixed)
        # print(ps.detransform(x0))
        if verbose > 0:
            print(f"Starting cost (without jax): {self.cost(params, odejax=False)}")
            print(f"Starting cost (with    jax): {self.cost(params, odejax=True)}")
        if method == "cmaes":
            cmaopts = cma.CMAOptions()
            cmaopts["bounds"] = [0, 10]
            # cmaopts["seed"] = 42
            # cmaopts['maxiter'] = 2000

            if verb_filenameprefix is not None:
                cmaopts['verb_filenameprefix'] = verb_filenameprefix

            print(params)
            if include_cfus:
                # exp0 = [exp for exp in self.exps if exp.a0 == 0][0]
                # eta0 = exp0.ods[np.where(np.abs(exp0.times - exp0.timescfus[0]) < 0.25)[0][0]] / exp0.cfus[0]
                # params['eta'] = eta0
                sol = cma.fmin(
                    lambda x: self.cost_with_cfu(ps.detransform(x), odejax=odejax, include_rich_media = include_rich_media),
                    x0,
                    sigma,
                    options=cmaopts,
                )
            else:
                sol = cma.fmin(
                    lambda x: self.cost(ps.detransform(x), odejax=odejax, include_rich_media = include_rich_media),
                    x0,
                    sigma,
                    options=cmaopts,
                )
            res = ps.detransform(sol[0])
            res = {name: float(value) for name, value in res.items()}
        elif method in ["trf", "dogbox", "lm"]:
            sol = opt.least_squares(
                lambda x: self.residuals(ps.detransform(x)),
                x0,
                bounds=(0, 10),
                method=method,
                verbose=verbose,
                *args,
                **kwargs,
            )
            res = ps.detransform(sol.x)
        elif method == "dual_annealing":
            sol = opt.dual_annealing(
                lambda x: self.cost(ps.detransform(x), odejax=odejax),
                [(0, 10) for name in BOUNDS if name not in fixed],
                callback=(lambda x, f, context: print(f, context)),
                x0=x0,
            )
            res = ps.detransform(sol.x)
            res = {name: float(value) for name, value in res.items()}
        elif method == "direct":
            sol = opt.direct(
                lambda x: self.cost(ps.detransform(x), odejax=odejax),
                [(0, 10) for name in BOUNDS if name not in fixed],
                callback=(
                    lambda xk: print(self.cost(ps.detransform(xk), odejax=odejax))
                ),
            )
            res = ps.detransform(sol.x)
            res = {name: float(value) for name, value in res.items()}
        elif method == "shgo":
            sol = opt.shgo(
                lambda x: self.cost(ps.detransform(x), odejax=odejax),
                [(0.5, 10) for name in BOUNDS if name not in fixed],
                callback=(
                    lambda xk: print(self.cost(ps.detransform(xk), odejax=odejax))
                ),
            )
            res = ps.detransform(sol.x)
            res = {name: float(value) for name, value in res.items()}
        elif method == "differential_evolution":
            sol = opt.differential_evolution(
                lambda x: self.cost(ps.detransform(x), odejax=odejax),
                [(0, 10) for name in BOUNDS if name not in fixed],
                callback=(
                    lambda xk, convergence: print(
                        self.cost(ps.detransform(xk), odejax=odejax), convergence
                    )
                ),
                disp=True,
            )
            res = ps.detransform(sol.x)
            res = {name: float(value) for name, value in res.items()}
        elif method == "basinhopping":
            sol = opt.basinhopping(
                lambda x: self.cost(ps.detransform(x)),
                x0,
                disp=True,
                callback=(lambda x, f, accept: print(f)),
                *args,
                **kwargs,
            )
            res = ps.detransform(sol.x)
        elif method == "lhs":
            best = None
            for i, x in enumerate(st.qmc.LatinHypercube(len(x0)).random(samples)):
                try:
                    if include_cfus:
                        sol = opt.least_squares(
                            lambda x: self.residuals_with_cfu(ps.detransform(x), odejax=odejax, include_rich_media = include_rich_media),
                            10 * x,
                            bounds=(0, 10),
                            method="trf",
                            verbose=verbose,
                            *args,
                            **kwargs,
                        )
                    else:
                        sol = opt.least_squares(
                            lambda x: self.residuals(ps.detransform(x), odejax=odejax, include_rich_media = include_rich_media),
                            10 * x,
                            bounds=(0, 10),
                            method="trf",
                            verbose=verbose,
                            *args,
                            **kwargs,
                        )
                    if best is None or sol.cost < best:
                        print(f"{i+1}/{samples}: New best: {sol.cost}")
                        best = sol.cost
                        if save:
                            res = ps.detransform(sol.x)
                            res = {name: float(v) for name, v in res.items()}
                            with open(f"res_{method}_{media_condition}.yaml", "w") as file:
                                yaml.dump(res, file)
                except ValueError:
                    pass
                print(f"{i+1}/{samples}: Best: {best}")
        else:
            sol = opt.minimize(
                lambda x: self.cost(ps.detransform(x)),
                x0,
                bounds=[(0, 10)] * len(x0),
                method=method,
                *args,
                **kwargs,
            )
            res = ps.detransform(sol.x)
        print(f"Final cost: {self.cost(res, odejax=odejax, include_rich_media = include_rich_media)}")
        if save:
            if media_condition is not None:
                with open(f"res_{method}_{media_condition}.yaml", "w") as file:
                    yaml.dump(res, file)
            else:
                with open(f"res_{method}.yaml", "w") as file:
                    yaml.dump(res, file)
        return res

    def curvatures(self, params, fixed=None, odejax = False, *args, **kwargs):
        if fixed is None:
            fixed = {}
        ps = ParamScaler(**fixed)
        x0 = ps.transform(params)
        sol = opt.least_squares(
            lambda x: self.residuals(ps.detransform(x), odejax = odejax),
            x0,
            bounds=(0, 10),
            method="trf",
            verbose=2,
            jac="3-point",
            *args,
            **kwargs,
        )
        res = ps.detransform(sol.x)
        curvatures = ps.detransform_curvatures(sol.x, sol.jac.T.dot(sol.jac))
        return res, curvatures

    def covariances(self, params, fixed=None, odejax = False, *args, **kwargs):
        if fixed is None:
            fixed = {}
        ps = ParamScaler(**fixed)
        x0 = ps.transform(params)
        sol = opt.least_squares(
            lambda x: self.residuals(ps.detransform(x), odejax = odejax ),
            x0,
            bounds=(0, 10),
            method="trf",
            verbose=2,
            jac="3-point",
            *args,
            **kwargs,
        )
        res = ps.detransform(sol.x)
        covariances = ps.detransform_covariances(sol.x, sol.jac.T.dot(sol.jac))
        return res, covariances

    def correlations(self, params, fixed=None, odejax = False, *args, **kwargs):
        if fixed is None:
            fixed = {}
        ps = ParamScaler(**fixed)
        x0 = ps.transform(params)
        sol = opt.least_squares(
            lambda x: self.residuals(ps.detransform(x), odejax = odejax),
            x0,
            bounds=(0, 10),
            method="trf",
            verbose=2,
            jac="3-point",
            *args,
            **kwargs,
        )
        res = ps.detransform(sol.x)
        correlations = ps.detransform_correlations(sol.x, sol.jac.T.dot(sol.jac))
        return res, correlations

    def profile_likelihood_one(self, name, params, path, fixed=None, odejax = False, *args, **kwargs):
        print(f"[PL {name}] Initial adjustment")
        p0, curvatures = self.curvatures(params, fixed=fixed, *args, **kwargs)
        cost0 = self.cost(p0)
        deltacostone = st.chi2.ppf(0.95, 1) / 2
        deltacostall = st.chi2.ppf(0.95, len(PARAMLIST) - len(fixed)) / 2
        value = p0[name]
        print(f"[PL {name}] starting at {value}")
        cost = cost0
        costs = [(p0[name], cost0, curvatures[name] / 2)]
        # epsilon set to hopefully get to each side in 10 hops
        epsilon = np.sqrt(2 * deltacostall / curvatures[name]) / 10
        print(f"[PL {name}] epsilon = {epsilon}")
        p = dict(p0)
        for _ in range(150):
            p[name] += epsilon
            if p[name] > 2 * p0[name]:
                break
            if name == "lmin" and p[name] > p["lmax"]:
                break
            s1 = self.cost(p)
            print(f"[PL {name}>] Fixed at {p[name]}, starting cost {s1}")
            p = self.search(
                *args,
                params=p,
                method="trf",
                fixed={**fixed, **{name: p[name]}},
                save=False,
                verbose=0,
                **kwargs,
            )
            cost = self.cost(p)
            costs.append((p[name], cost, 0))
            delta = (cost - cost0) / deltacostall
            print(f"[PL {name}>] Final cost: {cost}  {100*delta:5.1f}%")
            if cost - cost0 > deltacostone:
                realcurv = 2 * (cost - cost0) / (p[name] - p0[name]) ** 2
                epsilon = np.sqrt(2 * deltacostall / realcurv) / 20
            if delta > 1.1:
                break
        epsilon = np.sqrt(2 * deltacostall / curvatures[name]) / 10
        p = dict(p0)
        for _ in range(150):
            p[name] -= epsilon
            if p[name] < 0:
                break
            if name == "lmin" and p[name] > p["lmax"]:
                break
            s1 = self.cost(p)
            print(f"[PL <{name}] Fixed at {p[name]}, starting cost {s1}")
            p = self.search(
                *args,
                params=p,
                method="trf",
                fixed={**fixed, **{name: p[name]}},
                save=False,
                verbose=0,
                **kwargs,
            )
            cost = self.cost(p)
            costs.append((p[name], cost, 0))
            delta = (cost - cost0) / deltacostall
            print(f"[PL <{name}] Final cost: {cost}  {100*delta:5.1f}%")
            if cost - cost0 > deltacostone:
                realcurv = 2 * (cost - cost0) / (p[name] - p0[name]) ** 2
                epsilon = np.sqrt(2 * deltacostall / realcurv) / 20
            if delta > 1.1:
                break
        costs.sort()
        np.savetxt(path / f"PL_{name}.csv", costs, delimiter=",")
        return (name, costs)

    def profile_likelihood_mp(self, params, fixed=None, prefix=None, *args, **kwargs):
        if fixed is None:
            fixed = {}
        if prefix:
            path = Path(prefix)
            path.mkdir(exist_ok=True)
        else:
            path = Path(".")
        processes = [
            mp.Process(
                target=pl1,
                args=(self, name, params, path, *args),
                kwargs={"fixed": fixed, **kwargs},
            )
            for name in PARAMLIST
        ]
        for process in processes:
            process.start()
        for process in processes:
            process.join()

    def profile_likelihood(self, params, fixed=None, prefix=None, *args, **kwargs):
        if fixed is None:
            fixed = {}
        if prefix:
            path = Path(prefix)
            path.mkdir(exist_ok=True)
        else:
            path = Path(".")
        for name in params:
            self.profile_likelihood_one(
                name, params, path, *args, fixed=fixed, **kwargs
            )

    def plot(self, ax, *args, p=None, **kwargs):
        for exp in self:
            exp.plotod(ax[0])
            if exp.timescfus is not None:
                exp.plotn(ax[1], "o")
        if p is not None:
            plt_reset_colors(ax[0])
            plt_reset_colors(ax[1])
            for exp in self:
                exp.plotod_sim(p, ax[0], "--")
                exp.plotn_sim(p, ax[1], "--")
                exp.plotl_sim(p, ax[2], "--")
        ax[0].set_yscale("log")
        ax[1].set_yscale("log")
        ax[2].set_yscale("log")
        ax[1].set_ylim(0.1, None)
        ax[0].set_ylabel("Optical Density")
        ax[1].set_ylabel("Cell Number")
        ax[2].set_ylabel("Cell Length")

    def plotlegend(self, fig, colorkey="a0"):
        a0s = sorted(
            list(set((getattr(exp, colorkey), exp.color(colorkey)) for exp in self))
        )
        lines = [Line2D([], [], color=col) for _, col in a0s]
        if colorkey == "a0":
            labels = []
            for a0, _ in a0s:
                if a0 >= 1:
                    labels.append(f"{a0:.3g}" + r" \si{\milli\gram/\liter}")
                elif 1e3 * a0 >= 1:
                    labels.append(f"{1e3*a0:.3g}" + r" \si{\micro\gram/\liter}")
                else:
                    labels.append(f"{1e6*a0:.3g}" + r" \si{\nano\gram/\liter}")
        elif colorkey == "s0":
            labels = [f"{a0:.3g}" + r" \si{\gram/\liter}" for a0, _ in a0s]
        fig.legend(lines, labels)


class Experiment:
    def __init__(
        self,
        a0,
        od0,
        s0,
        times,
        ods,
        timescfus=None,
        cfus=None,
        stdcfus=None,
        timesinjs=None,
        volsinjs=None,
        concinj=0.0,
        v0=200.0,
        media='1 g/l glucose', strain=None
    ):
        times = np.array(times)
        ods = np.array(ods)
        self.a0 = a0
        self.od0 = od0
        self.s0 = s0
        self.media = media
        self.strain = strain
        self.times = times[ods > 0]
        self.ods = ods[ods > 0]
        self.lods = np.log(self.ods)
        self.stdods = 0.02 * self.ods + 1e-4
        self.stdlods = self.stdods / self.ods
        self.m_dil = 0.05
        self.timescfus = np.array(
            timescfus if timescfus is not None else [], dtype=np.float64
        )
        self.cfus = np.array(cfus if cfus is not None else [], dtype=np.float64)
        self.lcfus = np.log(self.cfus)
        self.stdcfus = np.array(stdcfus if stdcfus is not None else 0.1 * self.cfus + 400 , dtype=np.float64)  #0.1 * self.cfus + 400   #np.array(stdcfus if stdcfus is not None else [], dtype=np.float64)
        self.stdcfus[np.isnan(self.stdcfus)] = 0.1 * self.cfus[np.isnan(self.stdcfus)] + 400
        self.stdlcfus = self.stdcfus / self.cfus
        self.timesinjs = np.array(
            timesinjs if timesinjs is not None else [], dtype=np.float64
        )
        self.volsinjs = np.array(
            volsinjs if volsinjs is not None else [], dtype=np.float64
        )
        self.concinj = concinj
        self.v0 = v0
        if 0 in self.timesinjs:
            raise ValueError("Do not make an injection at time 0, use a0 instead.")
        self.timess = [
            self.times[(t0 <= self.times) & (self.times < t1)]
            for t0, t1 in zip(
                [0] + list(self.timesinjs), list(self.timesinjs) + [self.times[-1] + 1]
            )
        ]

    def cost(self, params, odejax: bool = False, include_rich_media: bool = False):
        if not include_rich_media:
            if odejax:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                return model.cost(
                    y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps,
                )
            res = self.residuals(params)
            cost = 0.5 * np.sum(res**2)
            return cost
        else:
            if odejax:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                return model.cost_rich_media(
                    y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps,
                )

    def cost_cfu(self, params, odejax: bool = False, include_rich_media: bool = False):
        if not include_rich_media:
            if odejax:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                return model.cost_cfus(
                    y0 = y0,
                    times = self.timess,
                    lods = self.lods,
                    stdlods = self.stdlods,
                    timescfus= self.timescfus,
                    lcfus=self.lcfus,
                    stdlcfus=self.stdlcfus,
                    timesinjs = self.timesinjs,
                    volsinjs = self.volsinjs,
                    concinj = self.concinj,
                    v0 = self.v0,
                    params = ps,
                )
        else:
            if odejax:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                return model.cost_rich_media_cfus(
                    y0=y0,
                    times=self.timess,
                    lods=self.lods,
                    stdlods=self.stdlods,
                    timescfus=self.timescfus,
                    lcfus=self.lcfus,
                    stdlcfus=self.stdlcfus,
                    timesinjs=self.timesinjs,
                    volsinjs=self.volsinjs,
                    concinj=self.concinj,
                    v0=self.v0,
                    params=ps,
                )


    def synthetic(self, params, dilnoise=False, measnoise=True, odejax: bool = False):
        """
        Returns a new experiment similar to this one, but simulated.
        """
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        # no eta since N, c and cr are already rescaled so it is eta / eta = 1
        ods_sim = solts[0] * solts[1] + solts[5] + solts[6]
        if measnoise:
            ods_std = 0.02 * ods_sim + 1e-4
            ods = np.random.normal(loc=ods_sim, scale=ods_std)
        else:
            ods = ods_sim
        # rng = np.random.default_rng()
        if self.timescfus is None:
            return Experiment(
                self.a0,
                self.od0,
                self.s0,
                self.times,
                ods,
                self.timescfus,
                self.cfus,
                self.stdcfus,
                self.timesinjs,
                self.volsinjs,
                self.concinj,
            )
        solts = sol.sol(self.timescfus)
        # eta because of the numerical rescaling
        cfus_sim = solts[0] / params["eta"]
        cfus_std = 0.02 * cfus_sim
        cfus_std[cfus_std < 0] = 0
        if measnoise:
            cfus = np.random.normal(loc=cfus_sim, scale=cfus_std)
        else:
            cfus = cfus_sim
        return Experiment(
            self.a0,
            self.od0,
            self.s0,
            self.times,
            ods,
            self.timescfus,
            cfus,
            cfus_std,
            self.timesinjs,
            self.volsinjs,
            self.concinj,
        )

    def residuals(self, params, odejax: bool = False, include_rich_media: bool = False):
        if not include_rich_media:
            if odejax:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                return model.residuals(
                    y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps,
                )
            sol = self.simulate(params, odejax=False)
            solts = sol if odejax else sol.sol(self.times)
            # no eta since N, c and cr are already rescaled so it is eta / eta = 1
            ods_sim = solts[0] * solts[1] + solts[5] + solts[6]
            ods_res = (np.log(ods_sim) - self.lods) / self.stdlods
            ods_res /= np.sqrt(len(ods_res))
            if len(self.timescfus) == 0:
                return ods_res
            solts = sol.sol(self.timescfus)
            # eta because of the numerical rescaling
            cfus_sim = solts[0] / params["eta"]
            cfus_res = (np.log(cfus_sim) - self.lcfus) / self.stdlcfus
            # cfus_res /= np.sqrt(len(cfus_res))
            # print(f'costOD = {0.5*np.sum(ods_res**2)}, ' +
            #       f'costCFU = {0.5*np.sum(cfus_res**2)}')
            # print(f'costODn= {0.5*np.sum(ods_res**2)/len(ods_res)}, ' +
            #       f'costCFUn= {0.5*np.sum(cfus_res**2)/len(cfus_res)}')
            return np.concatenate((ods_res, cfus_res))
        else:
            if odejax:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                return model.residuals_rich_media(
                    y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps,
                )


    def residuals_with_cfu(self, params, odejax: bool = False, include_rich_media : bool = False):
        if odejax:
            if not include_rich_media:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                res_ods =  model.residuals(
                    y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps,
                )
                cfu_res = model.residuals_cfus(y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timescfus,
                    self.lcfus,
                    self.stdlcfus,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps)
                return np.concatenate((res_ods, cfu_res))
            else:
                y0 = model.make_y0(self.a0, self.od0, self.s0, params)
                ps = dict(params)
                ps["lambda"] *= ps["eta"]
                ps["bin"] /= ps["eta"]
                ps["eta"] /= ps["eta"]
                res_ods = model.residuals_rich_media(
                    y0,
                    self.timess,
                    self.lods,
                    self.stdlods,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    ps,
                )
                cfu_res = model.residuals_rich_media_cfus(y0,
                                               self.timess,
                                               self.lods,
                                               self.stdlods,
                                               self.timescfus,
                                               self.lcfus,
                                               self.stdlcfus,
                                               self.timesinjs,
                                               self.volsinjs,
                                               self.concinj,
                                               self.v0,
                                               ps)
                return np.concatenate((res_ods, cfu_res))
        sol = self.simulate(params, odejax=False)
        solts = sol if odejax else sol.sol(self.times)
        # no eta since N, c and cr are already rescaled so it is eta / eta = 1
        ods_sim = solts[0] * solts[1] + solts[5] + solts[6]
        ods_res = (np.log(ods_sim) - self.lods) / self.stdlods
        ods_res /= np.sqrt(len(ods_res))
        if len(self.timescfus) == 0:
            return ods_res
        solts = sol.sol(self.timescfus)
        # eta because of the numerical rescaling
        cfus_sim = solts[0] / params["eta"]
        cfus_res = (np.log(cfus_sim) - self.lcfus) / self.stdlcfus
        # cfus_res /= np.sqrt(len(cfus_res))
        # print(f'costOD = {0.5*np.sum(ods_res**2)}, ' +
        #       f'costCFU = {0.5*np.sum(cfus_res**2)}')
        # print(f'costODn= {0.5*np.sum(ods_res**2)/len(ods_res)}, ' +
        #       f'costCFUn= {0.5*np.sum(cfus_res**2)/len(cfus_res)}')
        return np.concatenate((ods_res, cfus_res))


    def minimize_totala(self, finaln, nb_inj):
        """
        Returns the injection schedule that minimises the total concentration
        of antibiotic used while keeping the final n value under lower than
        `finaln`.
        """
        pass

    def color(self, colorkey="a0"):
        if getattr(self, colorkey) == 0:
            return "black"
        la0 = np.log2(getattr(self, colorkey))
        col = int(la0 + 1) % 10
        if abs(la0 - round(la0)) > 0.25:
            col = (col + 5) % 10
        return f"C{col}"

    def plotod(self, ax, *args, colorkey="a0", **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        return ax.plot(self.times, self.ods, *args, **kwargs)

    def plotn(self, ax, *args, colorkey="a0", **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        return ax.plot(self.timescfus, self.cfus, *args, **kwargs)

    def plotod_sim(
        self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs
    ):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        # no eta since N, c and cr are already rescaled
        ods = solts[0] * solts[1] + solts[5] + solts[6]
        if ods[-1] > 1:
            print(solts)
        ax.set_yscale("log")
        return ax.plot(self.times, ods, *args, **kwargs)

    def plotnl_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[0] * solts[1] / params["eta"], *args, **kwargs)

    def plotlm_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        lm = params["lmin"] + (params["lmax"] - params["lmin"]) / (
            1 + (solts[3] / params["k2"])
        )
        return ax.plot(self.times, lm, *args, **kwargs)

    def plotn_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[0] / params["eta"], *args, **kwargs)

    def plotl_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[1], *args, **kwargs)

    def plots_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[2], *args, **kwargs)

    def plota_sim(self, params, ax, *args, odejax=False, colorkey="a0", **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=False, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[3], *args, **kwargs)

    def plotb_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, 1000 * solts[4], *args, **kwargs)

    def plotccr_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[5] + solts[6], *args, **kwargs)

    def plotc_sim(self, params, ax, *args, dilnoise=False, colorkey="a0", odejax=False, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.color(colorkey)
        sol = self.simulate(params, dilnoise=dilnoise, odejax=odejax)
        solts = sol if odejax else sol.sol(self.times)
        return ax.plot(self.times, solts[5], *args, **kwargs)

    def simulate(self, params, dilnoise=False, tmax=None, odejax: bool = False, include_rich_media: bool = False):
        if tmax is None:
            tmax = max(self.times)
            if len(self.timescfus) > 0:
                tmax = max(tmax, max(self.timescfus))
            if len(self.timesinjs) > 0:
                tmax = max(tmax, max(self.timesinjs))
        params = dict(params)
        params["lambda"] *= params["eta"]
        params["bin"] /= params["eta"]
        params["eta"] /= params["eta"]
        if dilnoise:
            od0 = np.random.normal(loc=self.od0, scale=self.od0 * self.m_dil)
            a0 = np.random.normal(loc=self.a0, scale=self.a0 * self.m_dil)
        else:
            od0 = self.od0
            a0 = self.a0
        if odejax:
            y0 = model.make_y0(a0, od0, self.s0, params)
            if not include_rich_media:
                return model.simulate(
                    y0,
                    self.timess,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    params,
                )
            else:
                return model.simulate_rich_media(
                    y0,
                    self.timess,
                    self.timesinjs,
                    self.volsinjs,
                    self.concinj,
                    self.v0,
                    params,
                )
        l0 = np.log(2) * (1 + params["mu"] / params["beta"])
        # eta = 1
        n0 = od0 / l0
        b0 = 0
        c0 = 0
        cr0 = 0
        if len(self.timesinjs) == 0:
            sol = ode.solve_ivp(
                lambda t, y: model2(t, y, params),
                [0, tmax],
                [n0, l0, self.s0, a0, b0, c0, cr0],
                method="LSODA",
                dense_output=True,
                atol=1e-9,
                rtol=1e-9,
            )
            assert sol.success and sol.status == 0, sol
            return sol
        else:
            oldt = 0
            s0 = self.s0
            sols = []
            v0 = self.v0
            for tinj, vinj in zip(
                list(self.timesinjs) + [tmax], list(self.volsinjs) + [0]
            ):
                sol = ode.solve_ivp(
                    lambda t, y: model2(t, y, params),
                    [oldt, tinj],
                    [n0, l0, s0, a0, b0, c0, cr0],  # t_eval=[tinj],
                    method="LSODA",
                    dense_output=True,
                    atol=1e-9,
                    rtol=1e-9,
                )
                assert sol.success and sol.status == 0, sol
                sols.append(sol)
                n0, l0, sf, af, bf, c0, cr0 = sol.sol(tinj)
                s0 = sf * v0 / (v0 + vinj)
                b0 = bf * v0 / (v0 + vinj)
                a0 = (af * v0 + 1000 * self.concinj * vinj) / (v0 + vinj)
                v0 = v0 + vinj
                oldt = tinj
            tss = np.concatenate(
                [sol.sol.ts[int(i > 0) :] for i, sol in enumerate(sols)]
            )
            interpolantss = np.concatenate([sol.sol.interpolants for sol in sols])
            return opt.OptimizeResult(
                t=np.concatenate([sol.t for sol in sols]),
                y=np.concatenate([sol.y for sol in sols], axis=1),
                sol=ode.OdeSolution(tss, interpolantss),
                t_events=None,
                y_events=None,
                nfev=sum(sol.nfev for sol in sols),
                njev=sum(sol.njev for sol in sols),
                nlu=sum(sol.nlu for sol in sols),
                status=0,
                message=sols[0].message,
                success=all(sol.success for sol in sols),
            )


def ysupx(nu: float, x: float) -> float:
    if x <= 1:
        if nu == 0:
            return x * np.log(2)
        return x / nu * (x**nu - (x / 2) ** nu)
    if x <= 2:
        if nu == 0:
            return x - 1 + x * np.log(2 / x)
        return x - 1 + x / nu * (1 - (x / 2) ** nu)
    return 1


def lsupx(nu: float, x: float) -> float:
    if nu == 0:
        return x
    if x <= 1:
        return x / (nu * np.log(2)) * (x**nu - (x / 2) ** nu)
    if x <= 2:
        return x * np.log(x) / np.log(2) + x / (nu * np.log(2)) * (1 - (x / 2) ** nu)
    return x


def model2(t, y, p):
    n, l, s, a, b, c, cr = y
    s = max(s, 0.0)
    lmin = p["zeta"] * p["lmax"]
    g = p["mu"] * s / (p["ks"] + s)
    if g <= 0:
        g = 0
        nu = np.inf
    else:
        nu = p["beta"] / g
    f = p["beta"] / (1 + (a / p["k1"]) ** p["h1"])
    l0 = (1 + p["mu"] / p["beta"]) * np.log(2)
    lm = lmin + (p["lmax"] - lmin) / (1 + a / p["k2"])
    # Apparently it is better with g and not mu
    # because with mu, cells continue dying when s = 0
    # ys = ysup(g/p['beta'], lm, l, fake=fake)
    # ls = lsup(g/p['beta'], lm, l, fake=fake)
    ys = ysupx(nu, l / (l0 * lm))
    ls = l0 * lm * lsupx(nu, l / (l0 * lm))
    dndt = n * (f * (l / np.log(2) - 1) - p["gamma"] * ys)
    dldt = l * (g - f * (l / np.log(2) - 1)) - p["gamma"] * (ls - l * ys)
    dsdt = -g / p["lambda"] * n * l
    dadt = -p["kb"] * b * a - p["da"] * a
    dbdt = p["gamma"] * p["bin"] * n * ls - p["db"] * b
    dcdt = p["gamma"] * (1 - p["pc"]) * n * ls - p["dc"] * c
    dcrdt = p["gamma"] * p["pc"] * n * ls
    if y[3] < 0:
        print("y", y)
        print("dydt", [dndt, dldt, dsdt, dadt, dbdt, dcdt, dcrdt])
    return [dndt, dldt, dsdt, dadt, dbdt, dcdt, dcrdt]


def cost_finalod(x, params, totala, conc_inj):
    p0, t1 = x
    a0 = p0 * totala
    timesinjs = [t1]
    v1 = (totala - a0) / (1000 * conc_inj - a0) * 200
    # print(f'to simulate: {x}: {a0} {t1} {v1}')
    volsinjs = [v1]
    ts = np.linspace(0, 12)
    exp = Experiment(
        a0,
        0.05 / 100,
        1,
        ts,
        ts,
        timesinjs=timesinjs,
        volsinjs=volsinjs,
        concinj=conc_inj,
        v0=200,
    )
    sol = exp.simulate(params, dilnoise=False)
    solt = sol.sol(12)
    return np.log(solt[0] * solt[1] + solt[5] + solt[6])


def minimize_finalod(params, totala, conc_inj=3.2):
    """
    Returns the injection schedule that minimises the final OD value
    while keeping the total antibiotic dose used under the value `totala`.
    """
    # p0, t1
    res = opt.brute(
        cost_finalod,
        [(0, 1), (1, 11)],
        Ns=10,
        args=(params, totala, conc_inj),
        disp=True,
    )
    # bounds = opt.Bounds([0, 1], [1, 11])
    # res = opt.minimize(cost_finalod, x0,
    #                    args=(params, totala, conc_inj),
    #                    bounds=bounds,
    #                    # method='trust-constr',
    #                    options={'disp': True})
    print("res.x", res)
    print("     final cost", cost_finalod(res, params, totala, conc_inj))
    print(" all start cost", cost_finalod([1, 6], params, totala, conc_inj))
    print("half start cost", cost_finalod([0.5, 6], params, totala, conc_inj))
    return res


def cost_finaln(x, params, totala, conc_inj):
    p0, t1 = x
    a0 = p0 * totala
    timesinjs = [t1]
    v1 = (totala - a0) / (1000 * conc_inj - a0) * 200
    volsinjs = [v1]
    exp = Experiment(
        a0,
        0.05 / 100,
        1,
        [24],
        [1],
        timesinjs=timesinjs,
        volsinjs=volsinjs,
        concinj=conc_inj,
        v0=200,
    )
    sol = exp.simulate(params, dilnoise=False)
    return np.log(sol.sol(24)[0])


def minimize_finaln(params, totala, nb_inj, conc_inj=3.2):
    """
    Returns the injection schedule that minimises the final n value
    while keeping the total antibiotic dose used under the value `totala`.
    """
    x0 = [totala / 1.2]
    for i in range(nb_inj):
        x0 += [i + 3, 1]
    bounds = opt.Bounds([0] * len(x0), [totala] + [24, np.inf] * nb_inj)
    # constraints = opt.LinearConstraint(
    #     np.array(
    # print(x0)
    res = opt.minimize(
        cost_finaln,
        x0,
        args=(params, conc_inj),
        bounds=bounds,
        method="TNC",
        options={"disp": True},
    )
    # res = opt.minimize_scalar(lambda x: cost_finaln([x], params, conc_inj),
    #                           bounds=[0, totala], method='brent',
    #                           bracket=[0, totala],
    #                           options={'disp': True},
    #                           )
    print(res)
    print("initial cost", cost_finaln(x0, params, conc_inj))
    print("  final cost", cost_finaln(res.x, params, conc_inj))
    print("  other cost", cost_finaln([totala], params, conc_inj))


def profile_likelihood_plot(reference=None, prefix=None, df=None):
    if prefix:
        path = Path(prefix)
    else:
        path = Path(".")
    if not df:
        df = len(PARAMLIST)
    pls = {}
    for name in PARAMLIST:
        try:
            data = np.loadtxt(path / f"PL_{name}.csv", delimiter=",")
        except OSError:
            continue
        pls[name] = data
    cost0 = min(min(pls[name][:, 1]) for name in pls)
    # dcost1 = st.chi2.ppf(0.68, 1)/2
    # dcostall = st.chi2.ppf(0.68, df)/2
    deltacost1 = st.chi2.ppf(0.95, 1) / 2
    deltacostall = st.chi2.ppf(0.95, df) / 2
    fig, ax = plt.subplots(5, 4, sharey=True, figsize=(8, 12))
    plt.subplots_adjust(hspace=0.4)
    for i, name in enumerate(PARAMLIST):
        if name not in pls:
            continue
        row = i // 4
        col = i % 4
        p0 = pls[name][pls[name][:, 2] > 0, 0]
        if reference:
            pref = reference[name]
        else:
            pref = p0
        if row == 4:
            col += 1
        ax[row, col].set_title(NICENAMES[name])
        if name in pls:
            curv = pls[name][:, 2].sum()
            parabola = cost0 + 0.5 * curv * (pls[name][:, 0] - p0) ** 2
            ax[row, col].plot(pls[name][:, 0] / pref, parabola, "--", color="C1")
            ax[row, col].plot(pls[name][:, 0] / pref, pls[name][:, 1], color="C0")
            # ax[row, col].plot(pls[name][:, 0],
            #                   [cost0+dcost1]*len(pls[name][:, 0]), '--',
            #                   color='gray')
            # ax[row, col].plot(pls[name][:, 0],
            #                   [cost0+dcostall]*len(pls[name][:, 0]), '--',
            #                   color='gray')
            ax[row, col].plot(
                pls[name][:, 0] / pref,
                [cost0 + deltacost1] * len(pls[name][:, 0]),
                "--",
                color="gray",
            )
            ax[row, col].plot(
                pls[name][:, 0] / pref,
                [cost0 + deltacostall] * len(pls[name][:, 0]),
                "--",
                color="gray",
            )
    for row in range(5):
        ax[row, 0].set_ylabel("cost")
    ax[0, 0].set_ylim(cost0 - 0.1 * deltacostall, cost0 + deltacostall * 1.1)


def plotsol(ax, ts, states, params, *args, **kwargs):
    od = states[0] * states[1] + states[5] + states[6]
    ax = ax.ravel()
    ax[0].plot(ts, od, *args, **kwargs)
    ax[0].set_ylabel("OD")
    ax[0].set_yscale("log")
    for i, (a, st) in enumerate(zip(ax.ravel()[1:], states[:7])):
        a.plot(ts, st, *args, **kwargs)
        a.set_ylabel("NLSABCR"[i])
        if i in [0, 1, 3]:
            a.set_yscale("log")
    if states.shape[0] >= 8 and ax.size > 8:
        ax[8].plot(ts, states[7], *args, **kwargs)
        ax[8].set_ylabel("Y_>")
        ax[9].plot(ts, states[8], *args, **kwargs)
        ax[9].set_ylabel("L_>")


# def experiment(params, injections, atbc, model=model):
#     a0 = params['a0']
#     l0 = np.log(2)*(1+params['mu']/params['beta'])
#     n0 = params['od0'] / l0
#     s0 = params['s0']
#     b0 = 0
#     c0 = 0
#     cr0 = 0
#     v0 = 200
#     injections = [(0, 0)] + sorted(injections) + [(42, 0)]
#     tss = np.zeros(0)
#     solss = np.zeros((7, 0))
#     for (t1, v1), (t2, _) in zip(injections, injections[1:]):
#         # print(f'simulating from {t1} to {t2} (with injection of {v1})')
#         a0 = (a0*v0 + 1000*atbc*v1) / (v0 + v1)
#         # n0 unchanged
#         # l0 unchanged
#         s0 = s0*v0 / (v0 + v1)
#         b0 = b0*v0 / (v0 + v1)
#         # c0 unchanged
#         v0 = v0 + v1
#         ts = np.linspace(t1, t2, 200)
#         sol = ode.solve_ivp(lambda t, y: model(t, y, params),
#                             [t1, t2], [n0, l0, s0, a0, b0, c0, cr0],
#                             method='LSODA', t_eval=ts, atol=1e-9, rtol=1e-9)
#         tss = np.concatenate((tss, ts))
#         solss = np.concatenate((solss, sol.y), axis=1)
#         n0, l0, s0, a0, b0, c0, cr0 = solss[:, -1]
#     return tss, solss
