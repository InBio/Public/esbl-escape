from setuptools import setup
#
# if __name__ == "__main__":
#     setuptools.setup()

setup(
   name='esbl',
   version='0.2.0',
   author='Virgile Andreani',
   packages=['esbl'],
   install_requires=[ "numpy",
    "scipy",
    "pymc",
    "matplotlib",
    "pandas",
    "mpmath",
    "cma",
    "pyyaml",
    "jax",
    "diffrax",
    "ipython",
    "equinox",
    "openpyxl",
    "pyDOE"
                    ],)