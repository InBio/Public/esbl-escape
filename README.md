# A quantitative approach to easily characterize and predict cell death and escape to beta-lactam treatments  

## About

This repository hosts the implementation of the model and the code for figure generation. 

## Structure 

This folder contains several parts: 
- esbl: Python package containing implementation of the model
- figure_generation: code that generated the figures 
- consolidated_exp_data: summary of experimental data used for model calibration and figure generation
- consolidated_fitting_results: summary of results of model calibration for figure generation
- model_fitting: code that was used to calibration the model on experimental data, assemble the results and assess the quality. 
- raw experimental data: data is available for download at zenodo : https://doi.org/10.5281/zenodo.12543534; this folder also contains small Python package ```dputolerance``` used for processing raw OD data of experiments. 

## Installation 
If you are working on Windows, you need to install WSL first: https://learn.microsoft.com/en-us/windows/wsl/install. Install Ubuntu or Linux, set up a user. When this is done, open PowerShell and launch a linux based system through WSL (command ```wsl```). 

Install conda: https://docs.conda.io/projects/conda/en/4.6.0/user-guide/install/linux.html. 

In terminal create a conda environment: 

```terminal 
conda create -n esbl-env python>=3.11
```

Activate the environment and install the esbl package: 
```terminal 
conda activate esbl-env
pip install -e . 
```

### Figure generation 

Run scripts provided in the figure_generation folder. Plots are generated in plots folder. 