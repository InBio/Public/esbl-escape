import os.path
import seaborn as sns
import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import dputolerance
from matplotlib.ticker import MultipleLocator
import os
from opt_treatment_functions import *



if __name__ == "__main__":

    ### 1/ Get experimental data ###
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    filenames = ["20230207_OD_IB302_ddose.xlsx",
                 "20230727_OD_NILS18_doubledose.xlsx",
                 '20230802_OD_IB30678_double_dose.xlsx', '20230907_OD.xlsx',
                 ]
    strains_oi = np.array(["IB302", "NILS18", "IB307"])
    strains_f3 = np.array(['IB306', 'IB307', 'IB308'])
    strains_total = np.array([])
    exps_ref_index = {}
    exps = []
    n_exps1 = 0

    for f in range(len(filenames)):
        if f == 0:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD_1')
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0,
                                 index_col=0)
        elif f == 1:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD')
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU_4', header=0,
                                     index_col=0)
        elif f == 2:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]))
        elif f == 3:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD')
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0,
                                     index_col=0)

        strains = xlsx.iloc[0, :].to_numpy()[1:]
        strains_total = np.append(strains_total, np.unique(strains))

        if f in [0, 3]:
            times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
            a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
            strains = xlsx.iloc[0, :].to_numpy()[1:]
            timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(str)  # .astype(np.float64)
            volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(str)  # .astype(np.float64)
            concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
            wells = xlsx.iloc[6:, 1:].astype(np.float64)
        else:
            times = xlsx["Unnamed: 0"].to_numpy()[5:].astype(np.float64)
            a0s = xlsx.iloc[1, :].to_numpy()[1:].astype(np.float64)
            timesinjs = xlsx.iloc[2, :].to_numpy()[1:].astype(str)
            volsinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(str)
            concinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(str)
            wells = xlsx.iloc[5:, 1:].astype(np.float64)

        od0 = 5e-4
        v0 = 200

        cond = np.unique(np.array(
            [strains[i] + '_' + str(volsinjs[i]) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size)]))
        cond = cond[~ np.char.endswith(cond, 'nanatnan')]
        n_exps = cond.size
        n_exps_1 = len(exps)
        strains_unique = np.unique(strains)
        exps = exps + [esbl.Experiments() for i in range(n_exps + strains_unique.size)] if f not in [1, 3] else exps + [
            esbl.Experiments() for i in range(2 * (n_exps + strains_unique.size))]
        already_used = {}
        for i in range(wells.shape[1]):
            a0 = a0s[i]
            strain = strains[i]
            if f == 3 and strain == 'IB302': continue
            tinjs = np.array([]) if timesinjs[i] == 'nan' else \
                np.array([float(timesinjs[i])]) if '+' not in timesinjs[i] else \
                    np.array(timesinjs[i].split('+')).astype(float)
            vinjs = np.array([]) if volsinjs[i] == 'nan' else \
                np.array([float(volsinjs[i])]) if '+' not in volsinjs[i] else \
                    np.array(volsinjs[i].split('+')).astype(float)
            concinj = float(concinjs[i])


            # od data
            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]

            #second injections data
            if tinjs.size > 0:
                exp_cond_simple = strain + '_' + str(
                    round(1000 * concinj / 200 * vinjs[0])) + 'at' + np.array2string(
                    np.round(tinjs).astype(int),
                    separator='+')[1:-1]
                exp_cond_ref = strain + '_' + np.array2string(vinjs, separator='+')[1:-1] + 'at' + \
                               np.array2string(tinjs, separator='+')[1:-1]
                rep_od = 2 if exp_cond_ref + '_' + str(a0) in list(already_used.keys()) else 1

                # cfu data
                if f == 0:
                    exp_cond_cfu = str(int(1000 * concinj / 200 * vinjs[0])) + 'at' + np.array2string(tinjs.astype(int),
                                                                                                      separator='+')[1:-1]
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)] #& (cfu_data.time > 0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 1:
                    exp_cond_cfu = str(int(1000 * concinj / 200 * vinjs)) + 'at' + str((int(tinjs)))
                    # exp_cond_ref = strain + '_' + str(int(1000 * concinj / 200 * vinjs)) + 'at' + str(tinjs)
                    rep = 1
                    if exp_cond_cfu + '_' + str(a0) in list(already_used.keys()): rep = 2
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (
                                    cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 2:
                    times_cfus = np.array([])
                    cfus = np.array([])
                # print(f, strain, exp_cond_ref, times_cfus)

                if f == 0:
                    try:
                        ind = n_exps_1 + np.where(cond == exp_cond_ref)[0][0] + 1 if f != 2 else n_exps_1 + 2 * (
                                np.where(cond == exp_cond_ref)[0][0] + 1) + rep_od - 1
                    except:
                        v, t = exp_cond_ref.split('at')
                        exp_cond_ref = v + 'at' + t[1:]
                        v, t = exp_cond_simple.split('at')
                        exp_cond_simple = v + 'at' + t[1:]
                        ind = np.where(cond == exp_cond_ref)[0][0] + 1
                else:
                    ind_s = np.where(strains_unique == strain)[0][0]
                    ind = n_exps_1 + np.where(cond == exp_cond_ref)[0][0] + 1 + ind_s if f != 1 else \
                        n_exps_1 + 2 * (np.where(cond == exp_cond_ref)[0][0] + 1) + rep_od - 1
                # print(exp_cond_ref, f, strain, ind)

                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                      v0=v0, concinj=concinj,
                                      timesinjs=tinjs, volsinjs=vinjs,
                                      timescfus=times_cfus, cfus=cfus, stdcfus=cfus,
                                      strain=strain)
                else:
                    exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                          v0=v0, concinj=concinj,
                                          timesinjs=tinjs, volsinjs=vinjs,
                                          # timescfus=times_cfus, cfus=cfus, stdcfus=cfus,
                                          strain=strain)
                exps[ind].add(exp)

                if f != 1:
                    exps_ref_index[exp_cond_simple] = ind
                else:
                    exps_ref_index[exp_cond_simple + '_' + str(rep_od - 1)] = ind
                already_used[exp_cond_ref + '_' + str(a0)] = True
            else:
                if f == 0:
                    exp_cond_cfu = 'mono'
                    rep = 1
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (
                                    cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    stdcfus = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 1:
                    exp_cond_cfu = 'mono'
                    rep = 1
                    if exp_cond_cfu + '_' + str(a0) in list(already_used.keys()): rep = 2
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (
                                    cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    stdcfus = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 2:
                    times_cfus = np.array([])
                    cfus = np.array([])
                elif f == 3:
                    ind_s = np.where(strains_unique == strain)[0][0]
                    cfu_c = cfu_data[
                        # (cfu_data['time'] > 0) &
                        (cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                                cfu_data['rep'] == 1 + int((strain + '_mono_' + str(a0)) in already_used.keys())) & (
                                    cfu_data['CFU.well'] > 0)]
                    # times_cfus = np.unique(cfu_c['time'].to_numpy())
                    temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
                    times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
                    stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
                # print(f, times_cfus)

                if times_cfus.size > 0 and times[mask].size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods,
                                      timescfus=times_cfus, cfus=cfus, stdcfus=stdcfus,
                                      strain=strain)  # [mask])
                elif times[mask].size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods,
                                          # timescfus=times_cfus, cfus=cfus, stdcfus=std,
                                          strain=strain)
                exp_cond_simple = strain + '_mono'
                rep_od = 2 if exp_cond_simple + '_' + str(a0) in list(already_used.keys()) else 1
                ind_s = np.where(strains_unique == strain)[0][0]
                if f == 1:
                    ind = n_exps_1 + rep_od - 1
                elif f == 2:
                    ind = n_exps_1 + (cond.size // strains_unique.size + 1) * ind_s
                elif f == 0:
                    ind = n_exps_1
                else:
                    ind = n_exps_1 + 2 * ind_s + rep_od - 1
                exps[ind].add(exp)
                if f != 1 and f != 3:
                    exps_ref_index[exp_cond_simple] = ind
                else:
                    exps_ref_index[exp_cond_simple + '_' + str(rep_od - 1)] = ind
                already_used[exp_cond_simple + '_' + str(a0)] = True
                # print(exp_cond_simple, f, strain, ind)

    # print(exps_ref_index)
    strains_total = np.unique(strains_total)

    ## PLOT ###
    ns = strains_oi.size
    nm = 4
    concentrations = np.array([0] + [2**i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, 10)) #max(10, concentrations.size)))

    ### Load fit summary
    fits_summary_all = pd.read_excel(
        os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/fits_summary_all_strains_ddose.xlsx'),
        header=0, index_col=0, sheet_name='full_2501')
    fits_summary_all = fits_summary_all[fits_summary_all.cost < 1000]
    paramlist = list(esbl.params.BOUNDS.keys())
    fits_summary_all = fits_summary_all.drop_duplicates(paramlist)


    ### Plot histograms

    plt.rcParams['svg.fonttype'] = 'none'
    col_order = ['IB302', 'NILS18', 'IB307']
    col_wrap = 1
    exp_cond = {
        'IB302': 'mono+16at2+16at4+16at6',
        'IB306': 'mono+64at2+64at4+64at6',
        'IB307': 'mono+128at2+128at4+128at6',
        'IB308': 'mono+64at2+64at4+128at4',
        'IB311': 'mono+32at2+32at4+32at6',
        'NILS18': 'mono+64at2+64at4+64at6'
    }
    strains = np.unique(fits_summary_all.strain.to_numpy())
    mask_hist = (fits_summary_all['exp_cond'] == 'mono') & (fits_summary_all['strain'].isin(strains_oi))  # (fits_summary_all['strain'] == strains[0]) & (fits_summary_all['exp_cond'] == exp_cond[strains[0]])
    for i in range(strains_oi.size):
        mask_hist = mask_hist | (
                    (fits_summary_all['strain'] == strains_oi[i]) & (fits_summary_all['exp_cond'] == exp_cond[strains[i]]))

    df = fits_summary_all[mask_hist]

    medians = {}
    # Loop through each subplot
    for s in strains:
        # Calculate the median for each subset of data
        median_value_mono = df.loc[(df['strain'] == s) & (df['fit_type'] == 'mono') & (
                    df['cost_cfu'] < 220), 'cost_cfu'].median()
        median_value_r2 = df.loc[
            (df['strain'] == s) & (df['fit_type'] == 'mono+3r2') & (
                        df['cost_cfu'] < 220), 'cost_cfu'].median()
        medians[s + '_mono'] = median_value_mono
        medians[s + '_mono+3r2'] = median_value_r2
        # print(ax.title.get_text().split(' ')[2], median_value_mono, median_value_r2)


    ### Delayed CFU simulations
    fits_to_plot = []
    for s in strains_oi:
        for e in ['mono', exp_cond[s]]:
            m = medians[s + '_mono' if e == 'mono' else s + '_mono+3r2']
            df = fits_summary_all[(fits_summary_all.strain == s) & (fits_summary_all.exp_cond == e) & (fits_summary_all.cost_cfu - m <= 1) & (fits_summary_all.cost_cfu - m >= -1)]
            ind_min = df['cost_all'].argmin()
            fits_to_plot.append(pd.DataFrame([df.iloc[ind_min, :]], index = [0]))
            # print(df.iloc[ind_min, :])
    fits_to_plot = pd.concat(fits_to_plot, ignore_index = True)
    paramlist = list(esbl.params.BOUNDS.keys())

    nx = 1
    ny = 2
    exp_ref = exps[exps_ref_index['IB302_mono']].exps[0]
    times_end = exp_ref.timescfus[0]
    eta = exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))] / exp_ref.cfus[0]
    exp_conds_to_plot = np.array(['IB302_mono', 'IB302_16at2', 'IB302_16at4', 'IB302_16at6'])

    totala = 16

    sns.set(font_scale=2)
    sns.set(rc={'axes.labelsize': 16,
                'axes.facecolor': 'white', 'figure.facecolor': 'white', 'axes.edgecolor': 'black',
                'axes.spines.right': False, 'axes.spines.top': False,
                'axes.grid': True, 'grid.color': 'lightgrey',
                'xtick.color': 'black', 'ytick.color': 'black', })

    f1, ax1 = plt.subplots(nx, ny, figsize=(3.5 * ny, 3.5 * nx))
    strain = 'IB302'
    cond = 'mono+16at2+16at4+16at6'

    fit = fits_to_plot.loc[(fits_to_plot.strain == strain) & (fits_to_plot.exp_cond == cond), paramlist].iloc[0].to_dict()
    sns.color_palette("husl")
    counts = 0

    for i in range(exp_conds_to_plot.size):

        if 'mono' in exp_conds_to_plot[i]:
            exp = [e for e in exps[exps_ref_index[exp_conds_to_plot[i]]].exps if e.a0 == totala][0]
            label = str(int(exp.a0)) + 'mg/L at t0'
            color = colors[np.where(concentrations == exp.a0)[0][0]]
        elif str(totala) + 'at' in exp_conds_to_plot[i]:
            exp = exps[exps_ref_index[exp_conds_to_plot[i]]].exps[0]
            label = str(totala) + 'mg/L at t' +str(int(exp.timesinjs[0]))
            color = sns.color_palette("husl")[counts] #plt.cm.lajolla(i / 10) #np.linspace(0, 1, max(10, concentrations.size)))
            # print(a, counts)
            counts += 1
        # print(exp_conds_to_plot[i])

        sns.scatterplot(ax=ax1[0], x=exp.times, y=exp.ods, s=10,  #exp_conds[i],
                        color = color , legend = False)
        # sns.lineplot(ax=ax1[1, 2 * a + 1], x=exp.timescfus, y=exp.cfus, label=label,
        #              #label=label, #exp_conds[i],
        #              color = color , )
        ax1[1].plot(exp.timescfus, exp.cfus,  linewidth = 2, color=color, )
        sns.scatterplot(ax=ax1[1], x=exp.timescfus, y=exp.cfus, s = 20, label=label, color = color)
        solts = exp.simulate(fit, odejax=True)
        ods = solts[0] * solts[1] + solts[5] + solts[6]
        ax1[0].plot(exp.times, ods,  alpha=0.6, linestyle='--', color=color, linewidth = 3)

        times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
        cfus = solts[0] / eta
        cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
        # sns.lineplot(ax=ax1[1, 2 * a + 1], x=times_selected_points, y=cfus_selected_points, color = color, # if exp_conds[i] == 'mono' else None,
        #              linewidth=3, linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))
        ax1[1].plot(times_selected_points, cfus_selected_points, color=color, linestyle='--', linewidth = 3)

    ax1[0].set(yscale='log',ylim=(2e-4, 1.2),xlim=(0, 25))
    ax1[1].set(yscale='log',ylim=(1e2, 5e9),xlim=(0, 25))
    ax1[0].xaxis.set_major_locator(MultipleLocator(2))
    ax1[1].xaxis.set_major_locator(MultipleLocator(2))

    sns.move_legend(ax1[1], "upper left", bbox_to_anchor=(1, 1))

    f1.tight_layout()
    f1.subplots_adjust(wspace=0.2)
    f1.savefig('plots/figure4a.png')
    f1.savefig('plots/figure4a.svg')

    ### Optimization IB311 ###
    fitname =  'res_lhs_IB311_mono+32at2+32at4+32at6_2.yaml' # "res_lhs_IB311_mono+32at2+32at4+32at6_cluster_23.yaml"
    strain_oi = 'IB311'
    totaltime = 24
    tota = 16
    concinj = 1

    # grid calculations
    a0s = np.linspace(0, 1, 4 * tota + 1, endpoint=True)
    t1s = np.linspace(0, 1, 4 * totaltime + 1, endpoint=True)
    opt_grid = pd.DataFrame(columns = ['strain', 'a0', 't1', 'a1', 'totala', 'totaltime',
                                             'finalcost', 'finalOD', 'fitname'])
    params = fits_summary_all.loc[(fits_summary_all['strain'] == strain_oi) &
                                  (fits_summary_all['fitname'] == fitname), paramlist].iloc[0, :].to_dict()
    # print(fitname, params)
    for a0 in a0s:
        for t1 in t1s:
            finalcost = cost_final((a0, t1), params, tota, totaltime,
                                   concinj, odejax = True, mode = 'OD')
            finalOD = np.exp(finalcost - 10)
            opt_grid = pd.concat([opt_grid,
                           pd.DataFrame({'a0' : [a0 * tota], 't1' : [t1 * totaltime],
                                         'a1' : [tota * (1 - a0)],
                                         'finalcost' : [finalcost], 'finalOD' : [finalOD],
                                        'fitname' : fitname}, index = [0])],
                          ignore_index=True)

    opt_grid['totala'] = tota
    opt_grid['totaltime'] = totaltime
    opt_grid['strain'] = strain_oi
    print(opt_grid.sort_values(by = ['finalOD', 't1'], ascending = True, axis = 0).iloc[:10, :])

    # plot grid surface
    grid_z_values = pd.DataFrame(data=[], columns=np.unique(opt_grid['a0'].to_numpy()),
                                 index=np.unique(opt_grid['t1'].to_numpy()))
    for a0 in grid_z_values.columns:
        for t1 in grid_z_values.index:
            grid_z_values.loc[t1, a0] = float(opt_grid.loc[(opt_grid.a0 == a0) & (opt_grid.t1 == t1), 'finalOD'].iloc[0])

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(projection='3d')

    X1, Y1 = np.meshgrid(a0s * tota, t1s * totaltime)
    print(X1.shape, np.min(X1), np.max(X1))
    print(Y1.shape, np.min(Y1), np.max(Y1))
    Z = grid_z_values.values

    ax.plot_surface(X1, Y1, Z, alpha=0.5, cmap='coolwarm')
    ax.set(xlabel = 'a0', ylabel = 't1', zlabel = 'final OD')
    ax.zaxis.labelpad = 15
    ax.yaxis.labelpad = 15
    ax.xaxis.labelpad = 15

    coords = np.array([[16, 0, grid_z_values.loc[0.5, 16]],
                       [11, 2, grid_z_values.loc[2, 11]],
                       [11, 4, grid_z_values.loc[4, 11]],
                       [11, 6, grid_z_values.loc[6, 11]],
                       [9, 4, grid_z_values.loc[4, 9]],
                       [13, 4, grid_z_values.loc[4, 13]]])

    for i in range(coords.shape[0]):
        ax.scatter(coords[i, 0], coords[i, 1], coords[i, 2],
                   label='(' + str(int(coords[i, 0])) + ',' + str(int(coords[i, 1])) + ',' + str(
                       round(coords[i, 2], 2)) + ',' + ')', marker='o', s=20)

    plt.legend(bbox_to_anchor=(1.25, 0.7), markerscale=3)
    ax.view_init(azim=15, elev=5)
    plt.tight_layout()
    plt.savefig('plots/figure4b_grid.png')
    plt.savefig('plots/figure4b_grid.svg', format='svg')

    ### Experimental data for different treatments
    last_file = 'Viktoriia_empty_20230725_094942.xlsx'
    well_names = [l + str(i) for l in "ABCDEFGH" for i in range(1, 13)]
    n_wells = len(well_names)
    data_0 = np.array(pd.read_excel(os.path.join(path_data, last_file)))
    start = np.where(data_0[:, 0] == 'A')[0][0]
    od_background_plate1 = np.array(data_0[start: start + 8, 1: 13], dtype=float)
    times_od_plate1, datas_od_plate1, \
    times_xy_plate1, datas_xy_plate1, \
    times_scans_plate1, datas_scans_plate1 = dputolerance.get_all_results_excel_labels(path_data,
                                                                                       pattern='Viktoriia*agitation*.xlsx')
    strains = np.array(['NILS64', 'IB301', 'IB306', 'IB307', 'IB308', 'IB310', 'IB311'])
    media = ['glu01+tw01']
    s = np.array([1])
    concentration = np.array([0] + [2 ** (i) for i in range(1, 8)])
    a0s_ib311 = np.array([16, 11, 11, 11, 9, 13])
    timesinjs_ib311 = np.array([0, 2, 4, 6, 4, 4])
    volsinjs_ib311 = np.array([0, 1, 1, 1, 1.4, 0.6])
    conc_inj = 1
    OD_plate1 = dputolerance.Plate()
    od0 = 5e-4
    for i in range(12):
        for k in range(8):
            m = media[0]
            if k < 7:
                a0 = concentration[k + 1 * int(k > 0)]
                strain = strains[i % 6]
            else:
                strain = strains[-1]
                a0 = a0s_ib311[i % 6]

            ods = np.array(datas_od_plate1[:, 12 * k + i], dtype=float) - np.median(od_background_plate1)
            well = well_names[12 * k + i]
            Well1 = dputolerance.Well(wellname=well, times=times_od_plate1, ods=ods,
                                      od0=od0, strain=strain, media=m, a0=a0)
            OD_plate1.add_well(Well1)
    experiments_classic = []
    for i in range(12 + 2):
        experiments_classic.append(esbl.Experiments())
    v0 = 200
    for i in range(96):
        well = OD_plate1.wells[i]
        x, y = i // 12, i % 12
        try:
            ods1 = esbl.smooth(well.ods)
        except:
            ods1 = well.ods
        if x < 7 or y % 6 == 0:
            ind = y if x < 7 else 12 + y // 6
            experiments_classic[ind].add(
                esbl.Experiment(well.a0, well.od0, 1, well.times, ods1, strain=well.strain, media=well.media))
        else:
            ind = 12 + y // 6
            experiments_classic[ind].add(
                esbl.Experiment(well.a0, well.od0, 1, well.times, ods1, strain=well.strain, media=well.media,
                                v0=v0, volsinjs=np.array([volsinjs_ib311[y % 6]]), concinj=conc_inj,
                                timesinjs=np.array([timesinjs_ib311[y % 6]])))

    plt.rcParams['svg.fonttype'] = 'none'
    ns = 1
    nm = 1
    f1, ax1 = plt.subplots(nm, ns, sharex=False, sharey=False, figsize=(5, 3))
    sns.set(font_scale=3)
    sns.set_theme(style='white')
    colors = plt.cm.tab10(np.linspace(0, 1, max(10, (np.array(a0s_ib311)).size)))
    for k in range(12, 13):
        for i in range(len(experiments_classic[k].exps)):
            exp = experiments_classic[k].exps[i]
            if exp.volsinjs is not None and exp.volsinjs.size > 0:
                label = str(exp.a0) + ' mg/L +' + str(round(exp.volsinjs[0] * exp.concinj / 0.2)) + ' mg/L at' + str(
                    exp.timesinjs[0]) + 'h'
                sns.scatterplot(ax=ax1, x=exp.times, y=exp.ods, s=6, color=colors[i], label=label).set(
                    yscale='log', ylim=(2e-4, 0.5), xlim=(0, 40), title='IB311', xlabel='time, h', ylabel='OD'
                )
            elif exp.volsinjs is None or exp.volsinjs.size == 0:
                label = str(exp.a0) + ' mg/L'
                sns.scatterplot(ax=ax1, x=exp.times, y=exp.ods, s=6, color=colors[i], label=label).set(
                    yscale='log', ylim=(2e-4, 0.5), xlim=(0, 40), title='IB311', xlabel='time, h', ylabel='OD'
                )
            ax1.axvline(24, color='grey', linestyle=':', alpha=0.5)
    ax1.legend(loc="upper left", bbox_to_anchor=(1, 1), markerscale=4)
    f1.tight_layout()
    f1.savefig('plots/figure4b_exp.png')
    f1.savefig('plots/figure4b_exp.svg', format='svg')








