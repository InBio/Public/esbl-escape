import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import seaborn as sns

import warnings
warnings.filterwarnings('ignore', category = RuntimeWarning)
plt.rcParams['svg.fonttype'] = 'none'




if __name__ == "__main__":
    filename = ['20230627_OD.xlsx', '20230706_OD.xlsx', '20230907_OD.xlsx', '20231109_OD.xlsx']
    exps_ref_ind = {}
    strains_oi = np.array(['NILS1', 'NILS11', 'NILS12', 'NILS19',
                           'NILS42', 'NILS56',
                           'IB306', 'IB307', 'IB308', 'IB302', 'IB311', 'NILS31',
                           'NILS64'])
    exps = []
    n_exps1 = 0

    for f in range(len(filename)):
        # elif f == 1: cfu_data = pd.read_excel(filename[f], sheet_name='CFU_1', header=0, index_col=0)
        if f == 2:
            cfu_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                     sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                    sheet_name='OD_1', header=0)
        elif f < 2:
            cfu_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                     sheet_name='CFU_2', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                    sheet_name='OD', header=0)
        else:
            cfu_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                     sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                    sheet_name='OD', header=0)

        times = od_data["Unnamed: 0"].to_numpy()[6:].astype(np.float64)

        strains = od_data.iloc[0, :].to_numpy()[1:]
        strains_unique = np.unique(strains)
        if f < 2:
            a0s = od_data.iloc[1, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[2, :].to_numpy()[1:]
        else:
            a0s = od_data.iloc[2, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[1, :].to_numpy()[1:]
        od0 = 5e-4
        v0 = 200
        wells = od_data.iloc[6:, 1:].astype(np.float64)
        for i in range(2 * strains_unique.size):
            exps.append(esbl.Experiments())

        already_used = {s + '_' + str(a0): False for s in strains_unique for a0 in np.unique(a0s)}

        bound = wells.shape[1] if f < 3 else wells.shape[1] // 2

        for i in range(min(96, bound)):
            strain = strains[i]
            a0 = a0s[i]
            m = media[i]

            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]

            ind_s = 2 * np.where(strains_unique == strain)[0][0] + int(already_used[strain + '_' + str(a0)])

            cfu_c = cfu_data[(cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                        cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) &
                             (cfu_data['CFU.well'] > 0) & (cfu_data['time'] > 0)]
            # print(cfu_c)
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
            # print(times_cfus, cfus, cfu_c.groupby('time')['CFU.well'].std().to_numpy())

            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus=times_cfus,
                                          cfus=cfus,
                                          stdcfus=cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + ind_s].add(exp)
                exps_ref_ind[strain + '_monocfu' + str(int(already_used[strain + '_' + str(a0)]))] = n_exps1 + ind_s
            # else:
            #     print(strain, a0, i)
            already_used[strain + '_' + str(a0)] = True

        n_exps1 += 2 * strains_unique.size

    ### PLOT ###
    nx = 2
    ny = 1
    concentrations = np.array([0] + [2 ** i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    fig, ax1 = plt.subplots(nx, ny, sharey=False, sharex=False, figsize=(5 * ny, 4 * nx))

    sns.reset_defaults()
    sns.set(style='whitegrid')
    # sns.set(font_scale=1.5)
    strain = 'NILS42'
    conc_to_plot = np.array([0])
    i = exps_ref_ind[strain + '_monocfu1']

    for j in range(len(exps[i].exps)):
        exp = exps[i].exps[j]
        if exp.a0 in conc_to_plot:
            sns.scatterplot(ax=ax1[0], x=exp.times, y=exp.ods, s=8, label=str(exp.a0) + 'mg/L' if exp.a0 > 0 else 'no treatment',
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=True).set(
                yscale='log', ylim=(2e-4, 1), xlim=(0, 14), title=strain)
            sns.lineplot(ax=ax1[1], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale='log', ylim=(1e2, 5e9),
                                                                                          xlim=(0, 14))
            ax1[1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

    conc_to_plot = np.array([8, 64])
    i = exps_ref_ind[strain + '_monocfu0']

    for j in range(len(exps[i].exps)):
        exp = exps[i].exps[j]
        if exp.a0 in conc_to_plot:
            sns.scatterplot(ax=ax1[0], x=exp.times, y=exp.ods, s=8, label=str(exp.a0) + 'mg/L' if exp.a0 > 0 else 'no treatment',
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=True).set(
                xlabel = 'time', ylabel = 'OD', yscale='log', ylim=(2e-4, 1), xlim=(0, 14), title=strain)
            sns.lineplot(ax=ax1[1], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale='log', ylim=(1e2, 5e9),
                                                                                          xlabel = 'time', ylabel = 'CFU',
                                                                                          xlim=(0, 14))
            ax1[1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

    sns.move_legend(ax1[0], "upper left", bbox_to_anchor=(1, 1))
    fig.tight_layout()
    fig.savefig('plots/fig1.png')
    fig.savefig('plots/fig1.svg', format = 'svg')
