import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
import yaml
import multiprocessing as mp
from pathlib import Path
import os
import seaborn as sns
import glob

import warnings
warnings.filterwarnings('ignore', category = RuntimeWarning)
plt.rcParams['svg.fonttype'] = 'none'


if __name__ == "__main__":

    ### Get experimental data

    filename = ['20230627_OD.xlsx', '20230706_OD.xlsx', '20230907_OD.xlsx', '20231109_OD.xlsx']
    exps_ref_ind = {}
    strains_oi = np.array(['NILS1', 'NILS11', 'NILS12', 'NILS19',
                           'NILS42', 'NILS56',
                           'IB306', 'IB307', 'IB308', 'IB302', 'IB311', 'NILS31',
                           'NILS64'])
    exps = []
    n_exps1 = 0

    for f in range(len(filename)):
        # elif f == 1: cfu_data = pd.read_excel(os.path.join('data', filename[f]), sheet_name='CFU_1', header=0, index_col=0)
        if f == 2:
            cfu_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                     sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                    sheet_name='OD_1', header=0)
        elif f < 2:
            cfu_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                     sheet_name='CFU_2', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                    sheet_name='OD', header=0)
        else:
            cfu_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                     sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/'+ filename[f]),
                                    sheet_name='OD', header=0)

        times = od_data["Unnamed: 0"].to_numpy()[6:].astype(np.float64)

        strains = od_data.iloc[0, :].to_numpy()[1:]
        strains_unique = np.unique(strains)
        if f < 2:
            a0s = od_data.iloc[1, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[2, :].to_numpy()[1:]
        else:
            a0s = od_data.iloc[2, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[1, :].to_numpy()[1:]
        # timesinjs = xlsx.iloc[2, :].to_numpy()[1:]
        # volsinjs = xlsx.iloc[3, :].to_numpy()[1:]
        # concinjs = xlsx.iloc[4, :].to_numpy()[1:]
        od0 = 5e-4
        v0 = 200
        wells = od_data.iloc[6:, 1:].astype(np.float64)
        for i in range(2 * strains_unique.size):
            exps.append(esbl.Experiments())

        already_used = {s + '_' + str(a0): False for s in strains_unique for a0 in np.unique(a0s)}

        bound = wells.shape[1] if f < 3 else wells.shape[1] // 2

        for i in range(min(96, bound)):
            strain = strains[i]
            a0 = a0s[i]
            m = media[i]

            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]

            ind_s = 2 * np.where(strains_unique == strain)[0][0] + int(already_used[strain + '_' + str(a0)])

            cfu_c = cfu_data[(cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                        cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) &
                             (cfu_data['CFU.well'] > 0) & (cfu_data['time'] > 0)]
            # print(cfu_c)
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
            # print(times_cfus, cfus, cfu_c.groupby('time')['CFU.well'].std().to_numpy())

            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus=times_cfus,
                                          cfus=cfus,
                                          stdcfus=cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + ind_s].add(exp)
                exps_ref_ind[strain + '_monocfu' + str(int(already_used[strain + '_' + str(a0)]))] = n_exps1 + ind_s
            # else:
            #     print(strain, a0, i)
            already_used[strain + '_' + str(a0)] = True
        n_exps1 += 2 * strains_unique.size

    ### Get summary of generated fits
    fits_summary = pd.read_excel(os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/fit_summary_monocfu.xlsx'),
                                 header = 0, index_col = 0, sheet_name = 'final_fits')
    paramlist = list(esbl.params.BOUNDS.keys())

    ### PLOT ###
    strains_to_plot = np.array(['NILS12', 'NILS56', 'IB302', 'IB307'])
    fits_to_plot = {'NILS12' : 'res_lhs_NILS12_monocfu1_fromgrowth_10.yaml',
                    'NILS56' : 'res_lhs_NILS56_monocfu1_fromgrowth_10.yaml',
                    'IB302' : 'res_cmaes_IB302_monocfu1_fromgrowth_10.yaml',
                    'IB307' : 'res_cmaes_IB307_monocfu1_fromgrowth_10.yaml'
                    }
    max_conc = {'NILS1': 8, 'NILS11': 8, 'NILS12': 32, 'NILS19': 16, 'NILS42': 64, 'IB311': 128, 'NILS31': 128}

    nx = 2
    ny = strains_to_plot.size
    concentrations = np.array([0] + [2 ** i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    plt.rcParams['svg.fonttype'] = 'none'
    sns.set(rc={'axes.labelsize': 14})
    sns.set(rc={'axes.labelsize': 14,
                'axes.facecolor': 'white', 'figure.facecolor': 'white', 'axes.edgecolor': 'black',
                'axes.spines.right': False, 'axes.spines.top': False,
                'axes.grid': False, #'grid.color': 'lightgrey',
                'xtick.color': 'black', 'ytick.color': 'black', })
    fig, ax1 = plt.subplots(nx, ny, sharey = False, sharex = False, figsize = (4 * ny, 3 * nx))
    for k in range(strains_to_plot.size):
        i = exps_ref_ind[strains_to_plot[k] + '_monocfu0']
        fit = fits_summary.loc[(fits_summary.fitname == fits_to_plot[strains_to_plot[k]]), paramlist].iloc[0].to_dict()
        for j in range (len(exps[i].exps)):
            if strains_to_plot[k] not in max_conc.keys() or exps[i].exps[j].a0 < max_conc[strains_to_plot[k]]:
                # experimental data
                exp = exps[i].exps[j]
                ind_c = np.where(concentrations == int(exp.a0))[0][0]
                sns.scatterplot(ax=ax1[0, k], x=exp.times, y=exp.ods, s=10,
                                label=(str(exp.a0) + 'mg/L' if exp.a0 > 0 else 'no treatment') if (k == ny - 1) else "",
                                color=colors[ind_c], legend= (k == ny - 1) ).set(
                    yscale='log', ylim=(2e-4, 1), xlim=(0, 14), title=strains_to_plot[k], ylabel = 'OD')
                sns.lineplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus, linewidth = 3,
                             color=colors[ind_c]).set(
                    yscale='log', ylim=(1e2, 5e9), xlim=(0, 14), ylabel = 'live cell number')
                sns.scatterplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus,
                             color=colors[ind_c])

                # simulations
                solts = exp.simulate(fit, odejax=True)
                times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                ods = solts[0] * solts[1] + solts[5] + solts[6]
                cfus = solts[0] / fit['eta']  # eta[strains_oi[k]] #fit['eta']
                cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                sns.lineplot(ax=ax1[0, k], x=exp.times, y=ods, color=colors[ind_c], linewidth=3,
                             linestyle='--')
                sns.lineplot(ax=ax1[1, k], x=times_selected_points, y=cfus_selected_points, color=colors[ind_c],
                             linewidth=2, linestyle='--')

    sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), markerscale = 3)
    fig.tight_layout()
    fig.savefig('plots/fig2.png')
    fig.savefig('plots/fig2.svg', format = 'svg')
    plt.close()
