import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from pathlib import Path
import csv
import glob

path = os.getcwd()

well_names = np.array([L + str(i) for L in "ABCDEFGH" for i in range (1, 13)])
n_wells = well_names.size

class Plate:
    def __init__(self):
        self.wells = []
        for i in range(96):
            self.wells.append(Well(wellname=well_names[i]))
    def add_well(self, well):
        ind = np.where(well_names == well.name)[0][0]
        self.wells[ind] = well

class Well:
    def __init__(self, wellname = 'A1',
                 times = None, ods = None, od0 = 5e-4, # classic OD + times
                 times_xy = None, ods_xy = None, # multiple ODs per cycle + times + mean, mean of center, meadian, min
                 times_scans = None, ods_scans = None, # pictures + times
                 times_spectrum = None, ods_spectrum = None, #OD Scan
                 strain = None, media = 'glucose', a0 = 0):
        self.od0 = od0
        self.name = wellname
        self.times = []
        self.ods = []
        self.times_spectrum = None
        self.ods_spectrum = None

        if times is not None:
            self.times = times
            # self.ods = esbl.remove_background(ods, od0)
            self.ods = ods
        if times_xy is not None:
            self.times_xy = times_xy
            # ods_xy_1 = np.zeros(ods_xy.shape)
            nc, nx, ny = ods_xy.shape
            # for i in range(ods_xy.shape[1]):
            #     for j in range(ods_xy.shape[2]):
            #         ods_xy_1[:, i, j] = esbl.remove_background(ods_xy[:, i, j], od0)
            # self.ods_xy = ods_xy_1
            self.ods_xy = ods_xy
            temp = np.where(ods_xy == 0.0, np.nan, ods_xy)
            temp = np.reshape(temp, (nc, nx * ny))
            self.medod_xy = np.nanmedian(temp, axis = 1)
            self.meanod_xy = np.nanmean(temp, axis = 1)
            self.minod_xy = np.nanmin(temp, axis=1)
            if nx % 2 == 1:
                if ny % 2 == 1:
                    self.centerod_xy = ods_xy[:, nx // 2, ny // 2]
                else:
                    self.centerod_xy = np.reshape(np.mean(ods_xy[:, nx//2, ny// 2 - 1: ny // 2 + 1], axis = 2), nc)
            else:
                if ny % 2 == 1:
                    self.centerod_xy = np.mean(np.reshape(ods_xy[:, nx // 2 - 1: nx // 2 + 1, ny // 2], (nc, 2)), axis=1)
                else:
                    self.centerod_xy = np.mean(np.reshape(ods_xy[:, nx // 2 - 1: nx // 2 + 1, ny // 2 - 1 : ny // 2 + 1], (nc, 4)),
                                               axis=1)


        if times_scans is not None:
            self.times_scans = times_scans
            self.ods_scans = ods_scans

        if times_spectrum is not None:
            self.times_spectrum = times_spectrum
            self.ods_spectrum = ods_spectrum

        if strain is not None:
            self.strain = strain
        self.media = media
        self.a0 = a0

    def add_od_xy(self, ods_xy = None, times_xy = None):
        self.times_xy = times_xy
        nc, nx, ny = ods_xy.shape
        self.ods_xy = ods_xy
        temp = np.where(ods_xy == 0.0, np.nan, ods_xy)
        temp = np.reshape(temp, (nc, nx * ny))
        self.medod_xy = np.nanmedian(temp, axis=1)
        self.meanod_xy = np.nanmean(temp, axis=1)
        self.minod_xy = np.nanmin(temp, axis=1)
        if nx % 2 == 1:
            if ny % 2 == 1:
                self.centerod_xy = ods_xy[:, nx // 2, ny // 2]
            else:
                self.centerod_xy = np.reshape(np.mean(ods_xy[:, nx // 2, ny // 2 - 1: ny // 2 + 1], axis=2), nc)
        else:
            if ny % 2 == 1:
                self.centerod_xy = np.mean(np.reshape(ods_xy[:, nx // 2 - 1: nx // 2 + 1, ny // 2], (nc, 2)), axis=1)
            else:
                self.centerod_xy = np.mean(
                    np.reshape(ods_xy[:, nx // 2 - 1: nx // 2 + 1, ny // 2 - 1: ny // 2 + 1], (nc, 4)),
                    axis=1)

def read_excel_labels (name):
    # return times, ods, times_xy, od_xy, times_scans,od_scans

    # read data from the file
    data = np.array(pd.read_excel(name))


    # get positions of different labels, end points, beginning of cycles
    end_times = np.where(data[:, 0] == 'End Time')[0][3:]
    # print(end_times)

    n_labels_max = 100

    starts_labels = np.where(np.char.find(np.array(data[:, 0], dtype = str), 'Label') != -1)[0]
    n_labels_cycles = starts_labels.size
    starts_labels = np.append(starts_labels, np.where(np.char.find(np.array(data[:, 1], dtype = str), 'Label') != -1)[0][starts_labels.size : ])
    n_labels = starts_labels.size
    labels_after_cycles = n_labels > n_labels_cycles

    drop_ind = []
    for i in range (starts_labels.size):
        s = starts_labels[i]
        if np.where(np.logical_and(end_times - s <= 5, end_times - s >= 0))[0].size != 0:
            drop_ind.append(i)

    starts_labels = np.delete(starts_labels, drop_ind)

    labels_plate = data[starts_labels, 0]
    labels_plate = np.where(labels_plate != labels_plate, data[starts_labels, 1], data[starts_labels, 0])
    # print(starts_labels, labels_plate, end_times)
    starts_cycl_well = np.where(data[:, 0] == 'Cycles / Well')[0]
    # print(starts_cycl_well)
    n_labels = starts_labels.size

    # Label 1 - OD in the center
    start_label1 = starts_labels[0] + 2
    if n_labels > 1:
        stop_label1 = starts_labels[1] - 1
    else:
        stop_label1 = end_times[-1] - 1
    times_od = np.array(data[start_label1: stop_label1, 1], dtype=float) / 3600
    plate_od = np.array(data[start_label1 : stop_label1, 3 : 3 + 96], dtype = float)
    # print(times_od.size, plate_od.shape)

    # Label 2 - OD in 12 positions
    if n_labels > 1:
        start_label2 = starts_cycl_well[0]
        if n_labels > 2 :
            stop_label2 = min(starts_labels[2] - 1, end_times[-2] -1)
        elif len(end_times) + 1 > n_labels: 
            stop_label2 = end_times[-2] - 1
        else:
            stop_label2 = end_times[-1] -1
        times_xy = np.array(data[start_label2 + 2, 1:], dtype=float) / 3600
        times_xy = times_xy[np.isfinite(times_xy)]

        ods_xy = {}
        size_image0 = starts_cycl_well[0] - 1 - starts_labels[1]
        for i in range(len(well_names)):
            well = well_names[i]
            start = np.where(data[start_label2:, 0] == well)[0][0] + start_label2
            if i == len(well_names) - 1:
                stop = stop_label2
            else:
                stop = np.where(data[start_label2:, 0] == well_names[i + 1])[0][0] + start_label2

            od_xy = np.zeros((times_xy.size, size_image0, size_image0))
            for k in range(start + 5, stop - 2):
                try: 
                    x, y = data[k, 0].split(';')
                except: 
                    print(k, start, stop, data[k, 0])
                x, y = int(x), int(y)
                value = np.array(data[k, 1:times_xy.size + 1], dtype=float)
                od_xy[:, x - 1, y - 1] = value
            ods_xy[well] = od_xy
    else:
        times_xy = np.array([])
        ods_xy = {well : np.array([]) for well in well_names }
    # print(times_xy.size, ods_xy['A1'].shape)

    # well bottom scans
    if n_labels > 2:
        size_image1 = 20

        times_scans_all = np.array([])
        od_scans_all = {}

        for i in range(n_wells):
            well = well_names[i]
            times_scan = []
            od_scans = []
            n_cycle = 0
            for j in range(2, len(labels_plate)):
                # print(labels_plate[j])
                start = np.where(data[starts_labels[j]:, 0] == well)[0][0] + starts_labels[j]
                if i == len(well_names) - 1:
                    if j < len(labels_plate) - 1:
                        stop = starts_labels[j + 1]
                        n_cycle = stop - start
                    else:
                        stop = (end_times[end_times > start])[0]
                        # print(i, j, start, stop)
                else:
                    stop = np.where(data[starts_labels[j]:, 0] == well_names[i + 1])[0][0] + starts_labels[j]
                # if j == len(labels_plate) - 1:
                #     print(data[start:stop, 0])
                if j <= len(labels_plate) - 2 or 'Time [s]' in data[start:stop, 0]:
                    size_image1 = max(starts_cycl_well[n_wells * (j - 2)] - 1 - starts_labels[j], size_image1)
                    times_scan.append(data[start + 1, 1] / 3600)
                    od_scan = np.zeros((size_image1, size_image1))
                    for k in range(start + 5, stop - 2):
                        try:
                            x, y = map(int, data[k, 0].split(';'))
                        except:
                            print(well, k, data[k, 0], start, stop)
                        value = float(data[k, 1])
                        od_scan[x - 1, y - 1] = value
                else:
                    times_scan.append(times_od[-1])
                    od_scan = np.array(data[start + 1:stop, 0:size_image1], dtype=float)
                    # if i == len(well_names) - 1:
                    #     print(od_scan)
                od_scans.append(od_scan)

            od_scans = np.array(od_scans)
            times_scan = np.array(times_scan)
            if times_scans_all.size == 0:
                times_scans_all = times_scan
            od_scans_all[well] = od_scans
    else:
        times_scans_all = np.array([])
        od_scans_all = {well : np.array([]) for well in well_names }
    # print(times_scans_all.size, od_scans_all['A1'].shape)

    ind = np.where(data[:, 0] == 'Start Time')[0][0]
    date, start_time = (data[ind, 4]).split()
    start_time = start_time.split(':')
    start_time = int(start_time[0]) * 3600 + int(start_time[1]) * 60 + int(start_time[2])

    return start_time, times_od, plate_od, times_xy, ods_xy, times_scans_all, od_scans_all



def get_all_results_excel_labels(path=path, pattern='Viktoriia*2022*kinetic*.xlsx', multiple_labels=False):
    '''
    this function gets data from each individual excel file and combines them considering
    '''
    datas_od = []
    datas_xy = {well : [] for well in well_names }
    datas_scans_all = {well : [] for well in well_names }
    times_od_all = []
    times_xy_all = []
    times_scans_all = []

    start = 0
    i = 0
    last_start_time = 0

    for p in sorted(Path(path).glob(pattern)):
        start_time0, times_od, plate_od, times_xy, ods_xy, times_scan, od_scans_all = read_excel_labels(p)

        if times_od.size > 0:
            datas_od.append(plate_od)
            start_time = start_time0 - start
            if i == 0:
                start = start_time
                start_time = 0
            elif start_time < 0:
                start_time += 24 * 3600
            elif start_time < last_start_time or start_time < times_od_all[-1][-1]:
                start_time += 24 * 3600
            times_od_all.append(times_od + start_time / 3600)
            last_start_time = start_time
            # print(p, start_time / 3600, start)
            i += 1
        if times_xy.size > 0:
            for j in range(n_wells):
                well = well_names[j]
                datas_xy[well].append(ods_xy[well])
            # start_time = start_time0 - start
            # if i == 0:
            #     start = start_time
            #     start_time = 0
            # elif start_time < 0:
            #     start_time += 24 * 3600
            # elif start_time < last_start_time:
            #     start_time += 24 * 3600
            times_xy_all.append(times_xy + start_time / 3600)
            # i += 1
        if times_scan.size > 0:
            for j in range(n_wells):
                well = well_names[j]
                datas_scans_all[well].append(od_scans_all[well])
            # start_time = start_time0 - start
            # if i == 0:
            #     start = start_time
            #     start_time = 0
            # elif start_time < 0:
            #     start_time += 24 * 3600
            # elif start_time < last_start_time:
            #     start_time += 24 * 3600
            times_scans_all.append(times_scan + start_time / 3600)
            # i += 1

    datas_od = np.concatenate(datas_od)
    times_od_all = np.concatenate(times_od_all)
    if len(times_xy_all) > 0:
        times_xy_all = np.concatenate(times_xy_all)
        for j in range(n_wells):
            well = well_names[j]
            datas_xy[well] = np.concatenate(datas_xy[well])
    else:
        times_xy_all = np.array(times_xy_all)
    if len(times_scans_all) > 0:
        times_scans_all = np.concatenate(times_scans_all)
        for j in range(n_wells):
            well = well_names[j]
            datas_scans_all[well] = np.concatenate(datas_scans_all[well])
    else:
        times_scans_all = np.array(times_scans_all)
    return times_od_all, datas_od, times_xy_all, datas_xy, times_scans_all, datas_scans_all



