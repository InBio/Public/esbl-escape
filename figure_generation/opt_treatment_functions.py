import esbl
import numpy as np
import yaml
import scipy.optimize as opt
import scipy.stats as st
import cma

# import matplotlib.cm as cm
# from collections import OrderedDict
# from scipy import stats
# import pandas as pd
# import os
# from pathlib import Path
# import warnings
# import seaborn as sns
# import scipy.integrate as ode

# import scipy.signal as signal
# import sys
# import umap
# from sklearn import manifold
# from sklearn.decomposition import PCA
# from sklearn.manifold import TSNE




'''
- given total amount of antibiotic, optimise splitting this dose into 2 to minimize the final OD 
- given total amount of antibiotic, optimise splitting this dose into 2 to minimize the final CFU
'''


def cost_final(y, params, totala, totaltime, conc_inj, odejax = True, mode = 'OD') :
    # try:
        a0, t1 = y
        a0 *= totala
        t1 *= totaltime
        # print(a0, t1)
        if a0 > totala or t1 > totaltime or a0 < 0 or t1 <= 0:
            # print(a0, totala, t1, totaltime)
            return np.inf

        exp = esbl.Experiment(a0, 0.05 / 100, 1,
                              times=np.linspace(0, totaltime, 2 * totaltime),
                              ods = np.ones(2 * totaltime),
                              timesinjs=np.array([t1]),
                              volsinjs=np.array([(totala - a0) * 0.2 / conc_inj]),
                              concinj=conc_inj,
                              v0=200)
        sol = exp.simulate(params, odejax=odejax)
        solts = sol if odejax else sol.sol(exp.times)

        if mode == 'OD':
            ods = solts[0] * solts[1] + solts[5] + solts[6]
            # print('od', ods[-1])
            return 10+np.log(ods[-1])
        elif mode == 'CFU':
            n = solts[0] / params["eta"]
            return np.log(n[-1])
    # except:
    #     return np.inf

def cost_finalCFU(y, params, totala, totaltime, conc_inj, odejax = True) :
    try:
        a0, t1 = y
        if a0 > totala or t1 > totaltime or a0 < 0 or t1 <= 0:
            return np.inf

        exp = esbl.Experiment(a0, 0.05 / 100, 1,
                              times=np.linspace(0, totaltime + 1, 2 * (totaltime + 1)),
                              ods = np.ones(2 * (totaltime + 1)),
                              timesinjs=np.array([t1]),
                              volsinjs=np.array([(totala - a0) * 0.2 / conc_inj]),
                              concinj=conc_inj,
                              v0=200)
        sol = exp.simulate(params, odejax=odejax)
        solts = sol if odejax else sol.sol(exp.times)
        n = solts[0] / params["eta"]
        return np.log(n[exp.times == totaltime])
    except:
        return np.inf

def minimize_final(params, totala, totaltime = 24, conc_inj = 1,
                   goal = 'OD', method = 'lhs',
                   sigma = 0.2, samples = 100, odejax = True, media_condition = None, verbose = 2):
    x0 = (0.5, 0.5)
    if method == "cmaes":
        cmaopts = cma.CMAOptions()
        cmaopts["bounds"] = [0, 1]
        cmaopts["seed"] = 42
        sol = cma.fmin(
            lambda x: cost_final(x, params, totala, totaltime, conc_inj, odejax=odejax, mode = goal),
            x0,
            sigma,
            options=cmaopts,
        )
        res = sol[0]
        res = {'a0' : res[0] * totala, 't1' : res[1] * totaltime}
    elif method == "lhs":
        best = None
        for i, x in enumerate(st.qmc.LatinHypercube(len(x0), seed=0).random(samples)):
            try:
                sol = opt.least_squares(
                    lambda x: cost_final(x, params, totala, totaltime, conc_inj, odejax=odejax, mode = goal),
                    x,
                    bounds=(0, 1),
                    method="trf",
                    verbose=1,
                )
                if best is None or sol.cost < best:
                    print(f"{i + 1}/{samples}: New best: {sol.cost}")
                    best = sol.cost
                    res = sol.x
                    res = {'a0' : res[0] * totala, 't1' : res[1] * totaltime}
                    with open(f"res_{method}_{media_condition}.yaml", "w") as file:
                        yaml.dump(res, file)
            except ValueError:
                pass
            print(f"{i + 1}/{samples}: Best: {best}")
    else:
        sol = opt.minimize(
            lambda x: cost_final(x, params, totala, totaltime, conc_inj, odejax=odejax, mode = goal),
            x0,
            bounds=[(0, 10)] * len(x0),
            method=method,
        )
        res = sol.x
        res = {'a0': res[0] * totala, 't1': res[1] * totaltime}

    with open(f"res_{method}_{media_condition}.yaml", "w") as file:
            yaml.dump(res, file)

    return res

