import os.path
import seaborn as sns
import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp
import yaml
import os
import glob
from pathlib import Path
import itertools




if __name__ == "__main__":

    ### 1/ Get experimental data ###
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    filenames = ["20230207_OD_IB302_ddose.xlsx",
                 "20230727_OD_NILS18_doubledose.xlsx",
                 '20230802_OD_IB30678_double_dose.xlsx', '20230907_OD.xlsx']
    strains_oi = np.array(["IB302", "NILS18", "IB307"])
    strains_f3 = np.array(['IB306', 'IB307', 'IB308'])
    strains_total = np.array([])
    exps_ref_index = {}
    exps = []
    n_exps1 = 0

    for f in range(len(filenames)):
        if f == 0:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD_1')
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0,
                                 index_col=0)
        elif f == 1:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD')
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU_4', header=0,
                                     index_col=0)
        elif f == 2:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]))
        elif f == 3:
            xlsx = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD')
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0,
                                     index_col=0)

        strains = xlsx.iloc[0, :].to_numpy()[1:]
        strains_total = np.append(strains_total, np.unique(strains))

        if f in [0, 3]:
            times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
            a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
            strains = xlsx.iloc[0, :].to_numpy()[1:]
            timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(str)  # .astype(np.float64)
            volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(str)  # .astype(np.float64)
            concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
            wells = xlsx.iloc[6:, 1:].astype(np.float64)
        else:
            times = xlsx["Unnamed: 0"].to_numpy()[5:].astype(np.float64)
            a0s = xlsx.iloc[1, :].to_numpy()[1:].astype(np.float64)
            timesinjs = xlsx.iloc[2, :].to_numpy()[1:].astype(str)
            volsinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(str)
            concinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(str)
            wells = xlsx.iloc[5:, 1:].astype(np.float64)

        od0 = 5e-4
        v0 = 200

        cond = np.unique(np.array(
            [strains[i] + '_' + str(volsinjs[i]) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size)]))
        cond = cond[~ np.char.endswith(cond, 'nanatnan')]
        n_exps = cond.size
        n_exps_1 = len(exps)
        strains_unique = np.unique(strains)
        exps = exps + [esbl.Experiments() for i in range(n_exps + strains_unique.size)] if f not in [1, 3] else exps + [
            esbl.Experiments() for i in range(2 * (n_exps + strains_unique.size))]
        already_used = {}
        for i in range(wells.shape[1]):
            a0 = a0s[i]
            strain = strains[i]
            if f == 3 and strain == 'IB302': continue
            tinjs = np.array([]) if timesinjs[i] == 'nan' else \
                np.array([float(timesinjs[i])]) if '+' not in timesinjs[i] else \
                    np.array(timesinjs[i].split('+')).astype(float)
            vinjs = np.array([]) if volsinjs[i] == 'nan' else \
                np.array([float(volsinjs[i])]) if '+' not in volsinjs[i] else \
                    np.array(volsinjs[i].split('+')).astype(float)
            concinj = float(concinjs[i])


            # od data
            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]

            #second injections data
            if tinjs.size > 0:
                exp_cond_simple = strain + '_' + str(
                    round(1000 * concinj / 200 * vinjs[0])) + 'at' + np.array2string(
                    np.round(tinjs).astype(int),
                    separator='+')[1:-1]
                exp_cond_ref = strain + '_' + np.array2string(vinjs, separator='+')[1:-1] + 'at' + \
                               np.array2string(tinjs, separator='+')[1:-1]
                rep_od = 2 if exp_cond_ref + '_' + str(a0) in list(already_used.keys()) else 1

                # cfu data
                if f == 0:
                    exp_cond_cfu = str(int(1000 * concinj / 200 * vinjs[0])) + 'at' + np.array2string(tinjs.astype(int),
                                                                                                      separator='+')[1:-1]
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0) & (cfu_data.time > 0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 1:
                    exp_cond_cfu = str(int(1000 * concinj / 200 * vinjs)) + 'at' + str((int(tinjs)))
                    # exp_cond_ref = strain + '_' + str(int(1000 * concinj / 200 * vinjs)) + 'at' + str(tinjs)
                    rep = 1
                    if exp_cond_cfu + '_' + str(a0) in list(already_used.keys()): rep = 2
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (
                                    cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 2:
                    times_cfus = np.array([])
                    cfus = np.array([])
                # print(f, strain, exp_cond_ref, times_cfus)

                if f == 0:
                    try:
                        ind = n_exps_1 + np.where(cond == exp_cond_ref)[0][0] + 1 if f != 2 else n_exps_1 + 2 * (
                                np.where(cond == exp_cond_ref)[0][0] + 1) + rep_od - 1
                    except:
                        v, t = exp_cond_ref.split('at')
                        exp_cond_ref = v + 'at' + t[1:]
                        v, t = exp_cond_simple.split('at')
                        exp_cond_simple = v + 'at' + t[1:]
                        ind = np.where(cond == exp_cond_ref)[0][0] + 1
                else:
                    ind_s = np.where(strains_unique == strain)[0][0]
                    ind = n_exps_1 + np.where(cond == exp_cond_ref)[0][0] + 1 + ind_s if f != 1 else \
                        n_exps_1 + 2 * (np.where(cond == exp_cond_ref)[0][0] + 1) + rep_od - 1
                # print(exp_cond_ref, f, strain, ind)

                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                      v0=v0, concinj=concinj,
                                      timesinjs=tinjs, volsinjs=vinjs,
                                      timescfus=times_cfus, cfus=cfus, stdcfus=cfus,
                                      strain=strain)
                else:
                    exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                          v0=v0, concinj=concinj,
                                          timesinjs=tinjs, volsinjs=vinjs,
                                          # timescfus=times_cfus, cfus=cfus, stdcfus=cfus,
                                          strain=strain)
                exps[ind].add(exp)

                if f != 1:
                    exps_ref_index[exp_cond_simple] = ind
                else:
                    exps_ref_index[exp_cond_simple + '_' + str(rep_od - 1)] = ind
                already_used[exp_cond_ref + '_' + str(a0)] = True
            else:
                if f == 0:
                    exp_cond_cfu = 'mono'
                    rep = 1
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (
                                    cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    stdcfus = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 1:
                    exp_cond_cfu = 'mono'
                    rep = 1
                    if exp_cond_cfu + '_' + str(a0) in list(already_used.keys()): rep = 2
                    cfu_i = cfu_data[
                        (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (
                                    cfu_data['rep'] == rep) & (
                                cfu_data['conc'] == a0)]
                    times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
                    cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
                    stdcfus = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
                elif f == 2:
                    times_cfus = np.array([])
                    cfus = np.array([])
                elif f == 3:
                    ind_s = np.where(strains_unique == strain)[0][0]
                    cfu_c = cfu_data[
                        (cfu_data['time'] > 0) & (cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                                cfu_data['rep'] == 1 + int((strain + '_mono_' + str(a0)) in already_used.keys())) & (
                                    cfu_data['CFU.well'] > 0)]
                    # times_cfus = np.unique(cfu_c['time'].to_numpy())
                    temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
                    times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
                    stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
                # print(f, times_cfus)

                if times_cfus.size > 0 and times[mask].size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods,
                                      timescfus=times_cfus, cfus=cfus, stdcfus=stdcfus,
                                      strain=strain)  # [mask])
                elif times[mask].size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods,
                                          # timescfus=times_cfus, cfus=cfus, stdcfus=std,
                                          strain=strain)
                exp_cond_simple = strain + '_mono'
                rep_od = 2 if exp_cond_simple + '_' + str(a0) in list(already_used.keys()) else 1
                ind_s = np.where(strains_unique == strain)[0][0]
                if f == 1:
                    ind = n_exps_1 + rep_od - 1
                elif f == 2:
                    ind = n_exps_1 + (cond.size // strains_unique.size + 1) * ind_s
                elif f == 0:
                    ind = n_exps_1
                else:
                    ind = n_exps_1 + 2 * ind_s + rep_od - 1
                exps[ind].add(exp)
                if f != 1 and f != 3:
                    exps_ref_index[exp_cond_simple] = ind
                else:
                    exps_ref_index[exp_cond_simple + '_' + str(rep_od - 1)] = ind
                already_used[exp_cond_simple + '_' + str(a0)] = True
                # print(exp_cond_simple, f, strain, ind)

    # print(exps_ref_index)
    strains_total = np.unique(strains_total)

    ## PLOT ###
    ns = strains_oi.size
    nm = 4
    concentrations = np.array([0] + [2**i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, 10)) #max(10, concentrations.size)))

    f1, ax1 = plt.subplots(ns, nm, sharex=False, sharey=False, figsize=(2.5 * nm, 2.5 *ns))
    sns.set(font_scale = 3)
    sns.set_theme(style='white')
    for k in range(strains_oi.size):
        strain = strains_oi[k]
        if strain == 'NILS18':
            indexes = [ind for key, ind in exps_ref_index.items() if strain in key and
                       ('mono' in key or 'at2' in key or 'at4' in key or 'at6' in key) and
                       '+' not in key and key[-2:] != '_1']
            exp_conds = [key for key, ind in exps_ref_index.items() if strain in key and
                         ('mono' in key or 'at2' in key or 'at4' in key or 'at6' in key) and
                         '+' not in key and  key[-2:] != '_1']
        elif strain == 'IB302':
            exp_conds = [key for key, ind in exps_ref_index.items() if strain in key and '+' not in key and
                         ('mono' in key or '16at2' in key or '16at4' in key or '16at6' in key)]
            indexes = [ind for key, ind in exps_ref_index.items() if strain in key and '+' not in key and
                         ('mono' in key or '16at2' in key or '16at4' in key or '16at6' in key)]
        elif strain == 'IB307':
            indexes = [ind for key, ind in exps_ref_index.items() if strain in key and
                       ('mono_' in key) and
                       '+' not in key and key[-2:] != '_1']  + [ind for key, ind in exps_ref_index.items() if strain in key and
                       ('at2' in key or 'at4' in key or 'at6' in key) and
                       '+' not in key and key[-2:] != '_1']
            exp_conds = [key for key, ind in exps_ref_index.items() if strain in key and
                         ('mono_' in key ) and
                         '+' not in key and key[-2:] != '_1'] + [key for key, ind in exps_ref_index.items() if strain in key and
                         ('at2' in key or 'at4' in key or 'at6' in key) and
                         '+' not in key and key[-2:] != '_1']
        # print(strain, exp_conds)
        for i in range(len(indexes)):
            if 'mono' in exp_conds[i]:
                title = 'single treatment (T)'
            else:
                a1, t1 = (exp_conds[i].split('_')[1]).split('at')
                title = 'T + ' + a1 + ' mg/L at ' + t1 + 'hours'
            for exp in exps[indexes[i]].exps:
                sns.scatterplot(ax = ax1[k, i], x = exp.times, y = exp.ods, s=8, #label=exp.a0 if (i == nm - 1) else None ,
                                  color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend = False ).set( #i == nm - 1
                                    yscale = 'log', ylim = (2e-4, 1), xlim = (0, 25), title = title, ylabel = strain + ', OD', xlabel = 'time')
        # sns.move_legend(ax1[k, -1], "upper left", bbox_to_anchor=(1, 1))
    f1.tight_layout()
    f1.savefig( 'plots/figure3_experimentaldata.png')
    f1.savefig( 'plots/figure3_experimentaldata.svg', format = 'svg')

    ### Load fit summary
    fits_summary_all = pd.read_excel(
        os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/fits_summary_all_strains_ddose.xlsx'),
        header=0, index_col=0, sheet_name='full_2501')
    fits_summary_all = fits_summary_all[fits_summary_all.cost < 1000]
    paramlist = list(esbl.params.BOUNDS.keys())
    fits_summary_all = fits_summary_all.drop_duplicates(paramlist)


    ### Plot histograms

    plt.rcParams['svg.fonttype'] = 'none'
    sns.set(rc={'axes.labelsize': 14})
    sns.set(font_scale=1.5)
    f = plt.figure()
    colors = ["magenta", "green"]
    sns.set_palette(sns.color_palette(colors))
    col_order = ['IB302', 'NILS18', 'IB307']
    col_wrap = 1
    exp_cond = {
        'IB302': 'mono+16at2+16at4+16at6',
        'IB306': 'mono+64at2+64at4+64at6',
        'IB307': 'mono+128at2+128at4+128at6',
        'IB308': 'mono+64at2+64at4+128at4',
        'IB311': 'mono+32at2+32at4+32at6',
        'NILS18': 'mono+64at2+64at4+64at6'
    }
    strains = np.unique(fits_summary_all.strain.to_numpy())
    mask_hist = (fits_summary_all['exp_cond'] == 'mono') & (fits_summary_all['strain'].isin(strains_oi))  # (fits_summary_all['strain'] == strains[0]) & (fits_summary_all['exp_cond'] == exp_cond[strains[0]])
    for i in range(strains_oi.size):
        mask_hist = mask_hist | (
                    (fits_summary_all['strain'] == strains_oi[i]) & (fits_summary_all['exp_cond'] == exp_cond[strains[i]]))

    df = fits_summary_all[mask_hist]

    g = sns.displot(data=df, x='cost_cfu', col='strain',
                    hue='fit_type', hue_order=['mono', 'mono+3r2'], height=5, kind='hist', bins=100, common_bins=True,
                    # kde = True,
                    col_wrap=1, col_order=col_order,
                    facet_kws={'sharex': False, 'sharey': False}).set(xlim=(175, 215))

    medians = {}
    # Loop through each subplot
    for ax in g.axes.flat:
        # Calculate the median for each subset of data
        median_value_mono = df.loc[(df['strain'] == ax.title.get_text().split(' ')[2]) & (df['fit_type'] == 'mono') & (
                    df['cost_cfu'] < 220), 'cost_cfu'].median()
        median_value_r2 = df.loc[
            (df['strain'] == ax.title.get_text().split(' ')[2]) & (df['fit_type'] == 'mono+3r2') & (
                        df['cost_cfu'] < 220), 'cost_cfu'].median()
        medians[ax.title.get_text().split(' ')[2] + '_mono'] = median_value_mono
        medians[ax.title.get_text().split(' ')[2] + '_mono+3r2'] = median_value_r2
        # print(ax.title.get_text().split(' ')[2], median_value_mono, median_value_r2)

        # Display a vertical line at the median
        ax.axvline(x=median_value_mono, color=colors[0], linestyle='dashed', linewidth=2, )
        ax.axvline(x=median_value_r2, color=colors[1], linestyle='dashed', linewidth=2, )

    # Show the legend outside the subplots
    # g.add_legend()

    plt.savefig('plots/figure3_hist_cfuscore.png', format='png')
    plt.savefig('plots/figure3_hist_cfuscore.svg', format='svg')


    ### CFU simulations
    fits_to_plot = []
    for s in strains_oi :
        for e in ['mono', exp_cond[s]]:
            m = medians[s + '_mono' if e == 'mono' else s + '_mono+3r2']
            df = fits_summary_all[(fits_summary_all.strain == s) & (fits_summary_all.exp_cond == e) & (fits_summary_all.cost_cfu - m <= 1) & (fits_summary_all.cost_cfu - m >= -1)]
            ind_min = df['cost_all'].argmin()
            fits_to_plot.append(pd.DataFrame([df.iloc[ind_min, :]], index = [0]))
            # print(df.iloc[ind_min, :])
    fits_to_plot = pd.concat(fits_to_plot, ignore_index = True)

    exps_ref_cfu = np.array(['IB302_mono', 'NILS18_mono_0', 'IB307_mono_0'])
    nx = 2
    ny = strains_oi.size
    plt.rcParams['svg.fonttype'] = 'none'
    sns.set(rc={'axes.labelsize': 14})
    sns.set(rc={'axes.labelsize': 14,
                'axes.facecolor': 'white', 'figure.facecolor': 'white', 'axes.edgecolor': 'black',
                'axes.spines.right': False, 'axes.spines.top': False,
                'axes.grid': False,  # 'grid.color': 'lightgrey',
                'xtick.color': 'black', 'ytick.color': 'black', })
    f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx))
    colors = plt.cm.tab20(np.linspace(0, 1, 10)) #max(10, concentrations.size)))

    for i in range (strains_oi.size):
        for j in range(2):
            strain = strains_oi[i]
            cond = 'mono' if j == 0 else exp_cond[strain]
            if j == 0 and strain != 'IB302': continue
            fit = fits_to_plot.loc[(fits_to_plot.strain == strain) & (fits_to_plot.exp_cond == cond), paramlist].iloc[0].to_dict()
            exp_ref = exps[exps_ref_index[exps_ref_cfu[i]]].exps[0]
            times_end = exp_ref.timescfus[0]
            eta = exp_ref.od0 / exp_ref.cfus[0]

            ax1[j, i].set(title = strain + ', ' + cond)
            sns.set(font_scale=2)
            sns.set_theme(style = 'white')
            for exp in exps[exps_ref_index[exps_ref_cfu[i]]].exps:
                    ind_c = np.where(concentrations == int(exp.a0))[0][0]

                    # CFU experiment
                    sns.lineplot(ax=ax1[j, i], x=exp.timescfus, y=exp.cfus,
                                 color=colors[ind_c]).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 31) if strain == 'IB302' else (0, 15))
                    sns.scatterplot(ax=ax1[j, i], x=exp.timescfus, y=exp.cfus,
                                    color=colors[ind_c])

                    # CFU simulation
                    solts = exp.simulate(fit, odejax=True)
                    times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                    cfus = solts[0] / eta  # fit['eta']
                    cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                    sns.lineplot(ax=ax1[j, i], x=times_selected_points, y=cfus_selected_points,
                                 color=colors[ind_c],
                                 linewidth=2, legend=False, linestyle='--')

        # sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)
        for k in range(1, ny):
            ax1[0, k].set_axis_off()
        f1.tight_layout()
        f1.subplots_adjust(wspace=0.25)
        f1.savefig('plots/figure3_cfusim.png')
        f1.savefig('plots/figure3_cfusim.svg', format = 'svg')
        plt.close()








