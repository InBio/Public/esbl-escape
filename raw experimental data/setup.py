from setuptools import setup

setup(
   name='dputolerance',
   version='0.1.0',
   author='Viktoriia Gross',
   packages=['dputolerance'],
   install_requires=['pandas', 'numpy', 'scipy', 'matplotlib'],)

