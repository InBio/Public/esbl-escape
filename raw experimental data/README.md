# A quantitative approach to easily characterize and predict cell death and escape to beta-lactam treatments  

## About

This folder repository contains raw experimental data. Together with the code available at https://gitlab.inria.fr/InBio/Public/esbl-escape.git this data was used to generate figures for the paper []. 

Each folder corresponds to one experiment. Each experimental folder typically contains: 
- excel file with plate layout 
- excel file with OD measurements of an empty plate 
- multiple excel files with OD measurements 
- cfu folder that contains photos of the petri dishes and the intermediate results of colony counts for each plate 
- jupyter notebook for post-processing of CFU results (from 40 csv files to 1 excel file) 
- jupyter notebook for post-processing of OD and CFU results (removing background, matching experimental conditions such as strain, antibiotic concentration, treatment strategy with OD and CFU data, putting results in a easy-to-use format for model fitting) 

## Structure 

Experiments presented in this folder: 
- 20230207: repeated treatment experiment for IB302 
- 20230627: characterization of population response to cefotaxime for 6 strains 
- 20230706: characterization of population response to cefotaxime for 6 strains 
- 20230712: repeated treatment experiment for IB311 
- 20230725: characterization of population response to cefotaxime for 6 strains
- 20230727: repeated treatment experiment for NILS18
- 20230802: repeated treatment experiment for IB307, IB308
- 20230907: characterization of population response to cefotaxime for 2 strains
- 20231109: characterization of population response to cefotaxime for 3 strains