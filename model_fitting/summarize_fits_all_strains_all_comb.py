#! ~/.virtualenvs/esbl2/bin/python

import os.path
import seaborn as sns
import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp
import yaml
import os
import glob
from pathlib import Path
import itertools

plt.rcParams['svg.fonttype'] = 'none'
# ROOT = '/pasteur/gaia/homes/vgross/cluster'
# ROOT = '/pasteur/appa/homes/vgross/fitting_jobs'
ROOT = '/mnt/y/cluster'

def run_search(experiment, params={}, fixed={}, odejax = True, method = 'cmaes', samples = 100, include_cfus = False,
               media_condition='IB302_glucose+tween', verb_filenameprefix='outcmaes_parallel_new/'):

    try:
        if method == 'cmaes':
            sols = experiment.search(params=params, fixed = fixed, method=method,
                                     odejax=odejax, verbose=1, include_cfus=include_cfus,
                                     media_condition=media_condition, verb_filenameprefix=verb_filenameprefix),
        elif method == 'lhs':
            sols = experiment.search(params=params, fixed=fixed, method=method, samples=samples, include_cfus=include_cfus,
                                     odejax=odejax, verbose=1, media_condition=media_condition)
        return sols
    except:
        return -1


if __name__ == "__main__":

    ### 1/ Get experimental data ###
    '''
        common excel file with all experiemntal OD data 
        sheet names correspond to different experiments with different strains 
    '''
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    data_filename = 'data.xlsx'
    sheet_names = np.array(['IB302', 'IB311', 'NILS18', 'IB306,IB307,IB308', 'IB311_1'])

    strains_total = np.array([])
    exps = []
    exps_ref_index = {}
    already_used = {}

    for n in range(sheet_names.size):
        if n != 1:
            xlsx = pd.read_excel(os.path.join(path_data, data_filename), sheet_name=sheet_names[n], header=0)
            times = xlsx["Unnamed: 0"].to_numpy()[5:].astype(np.float64)
            strains = xlsx.iloc[0, :].to_numpy()[1:]
            strains_total = np.append(strains_total, np.unique(strains))

            a0s = xlsx.iloc[1, :].to_numpy()[1:].astype(np.float64)
            timesinjs = xlsx.iloc[2, :].to_numpy()[1:].astype(str)  # .astype(np.float64)
            # timesinjs = np.
            volsinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(str)  # .astype(np.float64)
            concinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
            od0 = 5e-4
            v0 = 200
            wells = xlsx.iloc[5:, 1:].astype(np.float64)

            cond = np.unique(np.array(
                [strains[i] + '_' + str(volsinjs[i]) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size)]))
            cond = cond[~ np.char.endswith(cond, 'nanatnan')]

            n_exps = cond.size
            n_exps_1 = len(exps)
            strains_unique = np.unique(strains)
            exps = exps + [esbl.Experiments() for i in range(n_exps + strains_unique.size)] if n != 2 else exps + [
                esbl.Experiments() for i in range(2 * (n_exps + 1))]
            already_used = {}

            for i in range(wells.shape[1]):
                a0 = a0s[i]
                strain = strains[i]
                if not (n == 1 and strain != 'IB311'):
                    tinjs = np.array([]) if timesinjs[i] == 'nan' else np.array([float(timesinjs[i])]) if '+' not in \
                                                                                                          timesinjs[
                                                                                                              i] else np.array(
                        timesinjs[i].split('+')).astype(float)
                    vinjs = np.array([]) if volsinjs[i] == 'nan' else np.array([float(volsinjs[i])]) if '+' not in \
                                                                                                        volsinjs[
                                                                                                            i] else np.array(
                        volsinjs[i].split('+')).astype(float)
                    concinj = concinjs[i]

                    ods = wells.iloc[:, i].to_numpy().astype(np.float64)
                    mask = np.isnan(ods) == 0
                    try:
                        ods = esbl.utils.smooth(ods[mask])
                    except:
                        ods = ods[mask]

                    if tinjs.size > 0:
                        exp_cond_simple = strain + '_' + str(
                            round(1000 * concinj / 200 * vinjs[0])) + 'at' + np.array2string(
                            np.round(tinjs).astype(int),
                            separator='+')[1:-1]
                        exp_cond_ref = strain + '_' + np.array2string(vinjs, separator='+')[
                                                      1:-1] + 'at' + np.array2string(tinjs, separator='+')[
                                                                     1:-1]
                        rep_od = 2 if exp_cond_ref + '_' + str(a0) in list(already_used.keys()) else 1
                        if n == 0:
                            try:
                                ind = n_exps_1 + np.where(cond == exp_cond_ref)[0][
                                    0] + 1 if n != 2 else n_exps_1 + 2 * (
                                            np.where(cond == exp_cond_ref)[0][0] + 1) + rep_od - 1
                            except:
                                v, t = exp_cond_ref.split('at')
                                exp_cond_ref = v + 'at' + t[1:]
                                v, t = exp_cond_simple.split('at')
                                exp_cond_simple = v + 'at' + t[1:]
                                ind = np.where(cond == exp_cond_ref)[0][0] + 1
                        else:
                            ind_s = np.where(strains_unique == strain)[0][0]
                            ind = n_exps_1 + np.where(cond == exp_cond_ref)[0][
                                0] + 1 + ind_s if n != 2 else n_exps_1 + 2 * (
                                        np.where(cond == exp_cond_ref)[0][0] + 1) + rep_od - 1

                        exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                              v0=v0, concinj=concinj,
                                              timesinjs=tinjs, volsinjs=vinjs,
                                              # timescfus=times_cfus, cfus=cfus, stdcfus=cfus,
                                              strain=strain)
                        exps[ind].add(exp)
                        if n != 2 and n != 4:
                            exps_ref_index[exp_cond_simple] = ind
                        else:
                            exps_ref_index[exp_cond_simple + '_' + str(rep_od - 1)] = ind
                        already_used[exp_cond_ref + '_' + str(a0)] = True
                    else:
                        exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods,
                                              # timescfus=times_cfus, cfus=cfus, stdcfus=std,
                                              strain=strain)  # [mask])
                        exp_cond_simple = strain + '_mono'
                        rep_od = 2 if exp_cond_simple + '_' + str(a0) in list(already_used.keys()) else 1
                        ind_s = np.where(strains_unique == strain)[0][0]
                        if n == 2:
                            ind = n_exps_1 + rep_od - 1
                        elif n == 3:
                            ind = n_exps_1 + (cond.size // strains_unique.size + 1) * ind_s
                        else:
                            ind = n_exps_1
                        exps[ind].add(exp)
                        if n != 2 and n != 4:
                            exps_ref_index[exp_cond_simple] = ind
                        else:
                            exps_ref_index[exp_cond_simple + '_' + str(rep_od - 1)] = ind
                        already_used[exp_cond_simple + '_' + str(a0)] = True

    # print(exps_ref_index)
    strains_total = np.unique(strains_total)
    # print(strains_total)

    ### PLOT ###
    ns = strains_total.size
    nm = 8
    concentrations = np.array([0] + [2**i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, 10)) #max(10, concentrations.size)))

    f1, ax1 = plt.subplots(ns, nm, sharex=False, sharey=False, figsize=(2.5 * nm, 2.5 *ns))
    sns.set(font_scale = 3)
    sns.set_theme(style='white')
    for k in range(strains_total.size):
        strain = strains_total[k]
        if strain != 'IB302':
            indexes = [ind for key, ind in exps_ref_index.items() if strain in key and
                       # ('mono' in key or 'at2' in key or 'at4' in key or 'at6' in key) and
                       '+' not in key and key[-2:] != '_1']
            exp_conds = [key for key, ind in exps_ref_index.items() if strain in key and
                         # ('mono' in key or 'at2' in key or 'at4' in key or 'at6' in key) and
                         '+' not in key and  key[-2:] != '_1']
        else:
            exp_conds = [key for key, ind in exps_ref_index.items() if strain in key and '+' not in key]
            indexes = [ind for key, ind in exps_ref_index.items() if strain in key and '+' not in key]
        # print(strain, exp_conds)
        for i in range(len(indexes)):
            if 'mono' in exp_conds[i]:
                title = 'single treatment (T)'
            else:
                a1, t1 = (exp_conds[i].split('_')[1]).split('at')
                title = 'T + ' + a1 + ' mg/L at ' + t1 + 'hours'
            for exp in exps[indexes[i]].exps:
                sns.scatterplot(ax = ax1[k, i], x = exp.times, y = exp.ods, s=8, #label=exp.a0 if (i == nm - 1) else None ,
                                  color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend = False ).set( #i == nm - 1
                                    yscale = 'log', ylim = (2e-4, 1), xlim = (0, 25), title = title, ylabel = strain + ', OD', xlabel = 'time')
        # sns.move_legend(ax1[k, -1], "upper left", bbox_to_anchor=(1, 1))
    f1.tight_layout()
    f1.savefig( 'experiments_double_dose_all_1.png')
    f1.savefig( 'experiments_double_dose_all_1.svg', format = 'svg')


    ### EXPERIMENTS FOR FITS ###
    exps_for_fit = []
    names_for_fit = []
    names_for_all = {}

    strains_for_fits = strains_total #np.array(['NILS18', 'IB306', 'IB302'])

    for k in range(strains_for_fits.size):
        strain = strains_for_fits[k]
        indexes = np.array([ind for key, ind in exps_ref_index.items() if strain in key and '+' not in key and key[-2:] != '_1'])
        exp_conds = np.array([key for key, ind in exps_ref_index.items() if strain in key and '+' not in key and key[-2:] != '_1'])
        n_exps_conds = exp_conds.size
        # print(strain, exp_conds)

        # create all combinations
        for i in range(1, n_exps_conds + 1):
            combinations = itertools.combinations(exp_conds, i)
            for r in combinations:
                # print(r)
                exps_fit = []
                name = ""
                for r1 in r:
                    # print(r1, exps_ref_index[r1])
                    exps_fit += exps[exps_ref_index[r1]].exps
                    if name == "":
                        name += r1[:-2] if strain in ['NILS18', 'IB311'] else r1
                    else:
                        if r1.count('_') == 2:
                            _, r1, _ = r1.split('_')
                        else:
                            _, r1 = r1.split('_')
                        name += '+' + r1
                exps_for_fit.append(esbl.Experiments(exps_fit))
                names_for_fit.append(name)
                if i == n_exps_conds:
                    names_for_all[strain] = name
        # print(names_for_fit)
    names_for_fit = np.array(names_for_fit)
    # print(names_for_fit.size)
    # print(names_for_all)
    # with open('names_fits.txt' )




    ###  Summarize all fits into one excel file ###


    # path = '/mnt/c/Users/vgross/PhD/data/results/platereader/tween fits/new_esbl_version'
    # get previous fits
    fits_summary = pd.read_excel(os.path.join(path_fits, 'fits_summary_all_strains_ddose.xlsx'), sheet_name='2102_cfu2',
                                 header = 0, index_col=0)

    res_fits = []
    for p in sorted(Path(os.path.join(os.getcwd(), 'fits_comb_1')).glob('*.yaml')):
        filename = os.path.basename(p)
        name_parts = list((filename.split('.yam')[0]).split('_'))
        # print(name_parts)
        if len(name_parts) > 2:
            if len(name_parts) == 6:
                method, strain, exp_cond, _, rep = name_parts[1:]
            elif len(name_parts) == 5:
                method, strain, exp_cond, rep = name_parts[1:]
            with open(p) as file:
                sols = yaml.safe_load(file)
            if sols is not None:
                cost = exps_for_fit[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].cost(sols, odejax = 'True')
                cost_all = exps_for_fit[np.where(names_for_fit == names_for_all[strain])[0][0]].cost(sols, odejax = 'True')

                fit_type = 'mono' * ('mono' in exp_cond) + '+' * (('mono' in exp_cond) & ('+' in exp_cond)) + \
                           (str(int(exp_cond.count('at'))) + 'r2') * ('at' in exp_cond)
                num = int(('mono' in exp_cond)) + int(exp_cond.count('at'))
                # print(exp_cond, fit_type, num)
                res_fits.append(pd.DataFrame({'fitname' : filename, 'method' : method, 'strain' : strain, 'exp_cond' : exp_cond,
                                              'fit_type' : fit_type, 'number_of_exps' : num,
                                              'cost' : cost, 'cost_all' : cost_all, 'cost_cfu' : None} | sols, index = [0]))

    # recover cmaes files
    ps = esbl.ParamScaler()
    for p in sorted(Path(os.path.join(os.getcwd(), 'fits_comb_1/outcmaes')).glob('*xrecentbest*')):
        filename = os.path.basename(p)
        # print(filename)
        fitname = filename.split('xrecent')[0]
        if ('IB307' in filename or 'IB308' in filename or 'IB311' in filename) and not os.path.exists('fits_comb/res_cmaes_' + fitname + '.yaml') :
            # print(fitname)
            with open(p, 'r') as f:
                n = len(list(f))
            try:
                xrecentbest = np.loadtxt(p, skiprows=1, max_rows=n - 2)
            except:
                try:
                    xrecentbest = np.loadtxt(p, skiprows=1, max_rows=n - 2, usecols=22)
                except:
                    xrecentbest = np.array([])

            if xrecentbest.size > 0:
                # print(fitname, xrecentbest.shape)
                if xrecentbest.shape[0] != xrecentbest.size:
                    sols = ps.detransform(xrecentbest[-1, 5:])
                else:
                    sols = ps.detransform(xrecentbest[5:])
                sols = {name: float(value) for name, value in sols.items()}
                # print(sols)
                with open(f"fits_comb/res_cmaes_{fitname}.yaml", "w") as file:
                    yaml.dump(sols, file)
                strain, exp_cond, _, rep = fitname.split('_')

                fit_type = 'mono' * ('mono' in exp_cond) + '+' * (('mono' in exp_cond) & ('+' in exp_cond)) + (str(
                    int(exp_cond.count('at'))) + 'r2') * ('at' in exp_cond)
                num = int(('mono' in exp_cond)) + int(exp_cond.count('at'))
                # print(exp_cond, fit_type, num)

                cost = exps_for_fit[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].cost(sols, odejax='True')
                cost_all = exps_for_fit[np.where(names_for_fit == names_for_all[strain])[0][0]].cost(sols, odejax='True')
                res_fits.append(pd.DataFrame({'fitname': f"res_cmaes_{fitname}.yaml", 'method': 'cmaes', 'strain': strain, 'exp_cond': exp_cond,
                                              'fit_type': fit_type, 'number_of_exps': num,
                                              'cost': cost, 'cost_all': cost_all, 'cost_cfu': None} | sols, index=[0]))

    res_fits = pd.concat(res_fits, ignore_index=True)
    # fits_summary = pd.concat(res_fits, ignore_index=True)

    fits_summary = pd.concat([res_fits, fits_summary], ignore_index=True)

    paramlist = list(esbl.params.BOUNDS.keys())
    #
    # # res_fits = res_fits[res_fits['cost'] < 500]
    # fits_summary = fits_summary.drop_duplicates(subset=['cost', 'cost_all'] + list(paramlist))
    #
    # # with pd.ExcelWriter('fits_summary_clustercomb.xlsx', mode = 'a') as writer:
    # #     res_fits.to_excel(writer, sheet_name='1')
    with pd.ExcelWriter(os.path.join(path_fits, 'fits_summary_all_strains_ddose.xlsx'), mode = 'w') as writer:
        fits_summary.to_excel(writer, sheet_name='complete_0104')