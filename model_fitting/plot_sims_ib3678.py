import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp
from collections import OrderedDict
import yaml
import os
import glob
from pathlib import Path
import seaborn as sns

plt.rcParams['svg.fonttype'] = 'none'

if __name__ == "__main__":
    ### GET EXPERIMENTAL DATA ###
    filenames = ['20230725_OD.xlsx', '20230802_OD_IB30678_double_dose.xlsx', '20230907_OD.xlsx']
    strains_oi = np.array(['IB306', 'IB307', 'IB308'])
    exps_ref_ind = {}
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')


    # mono dose from characterization
    xlsx = pd.read_excel(os.path.join(path_data, filenames[0]), sheet_name='OD')
    cfu_data = pd.read_excel(os.path.join(path_data, filenames[0]), sheet_name='CFU_3', header = 0, index_col=0)
    times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
    strains = xlsx.iloc[0, :].to_numpy()[1:]
    medias = xlsx.iloc[1, :].to_numpy()[1:]
    a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[6:, 1:].astype(np.float64)
    strains_unique = np.unique(strains)
    concentrations = np.unique(a0s)
    exps = [esbl.Experiments() for i in range (strains_oi.size)]
    already_used = {s + '_' + str(c) : False for s in strains_oi for c in np.unique(a0s)}
    for i in range(wells.shape[1]):
        strain = strains[i]
        if strain in strains_oi:
            a0 = a0s[i]
            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]
            ind_s = np.where(strains_oi == strain)[0][0]

            cfu_c = cfu_data[(cfu_data['time'] > 0) & (cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) & (cfu_data['CFU.well'] > 0)]
            # print(cfu_c)
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus =  temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            # print(times_cfus, cfus, cfu_c.groupby('time')['CFU.well'].std().to_numpy())
            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus = times_cfus, cfus = cfus, stdcfus =  cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain = strain) #[mask])
                exps[ind_s].add(exp)
                exps_ref_ind[strain + '_mono0'] = ind_s
            already_used[strain + '_' + str(a0)] = True

    n_exps1 = len(exps)
    # get data from last experiment
    xlsx = pd.read_excel(os.path.join(path_data, filenames[1]))
    times = xlsx["Unnamed: 0"].to_numpy()[5:].astype(np.float64)
    strains = xlsx.iloc[0, :].to_numpy()[1:]
    a0s = xlsx.iloc[1, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[5:, 1:].astype(np.float64)
    cond = np.unique(
        np.array([strains[i] + ',' + str(int(volsinjs[i] * 5. * concinjs[i])) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size) if
                  type(timesinjs[i]) == str or np.isfinite(timesinjs[i])]))
    cond = cond[cond != 'nanatnan']
    strains_unique = np.unique(strains)
    concentrations = np.union1d(concentrations, np.unique(a0s))
    n_exps = (cond.size + strains_unique.size)
    exps = exps + [esbl.Experiments() for i in range(n_exps)]  # save only IB311
    for i in range(wells.shape[1]):
        strain = strains[i]
        if strain in strains_oi:
            a0 = a0s[i]
            tinjs = timesinjs[i]
            vinjs = volsinjs[i]
            concinj = concinjs[i]
            # ods = esbl.utils.smooth(wells.iloc[:, i])
            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]
            ind_s = np.where(strains_unique == strain)[0][0]
            if type(tinjs) == str or np.isfinite(tinjs):
                exp_cond = strain + ',' + str(int(vinjs * 5 * concinj)) + 'at' + str(tinjs)
                ind = np.where(cond == exp_cond)[0][0] + 1

                exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                          v0=v0, volsinjs=np.array([vinjs]).astype(np.float64), concinj=concinj,
                                          timesinjs=np.array([tinjs]).astype(np.float64), strain=strain)
                exps[n_exps1 + ind + ind_s].add(exp)
                exps_ref_ind[exp_cond] = n_exps1 + ind + ind_s
            else:
                exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + (cond.size // strains_oi.size + 1) * ind_s].add(exp)
                exps_ref_ind[strain + '_mono'] = n_exps1 + (cond.size // strains_oi.size + 1) * ind_s

    n_exps1 = len(exps)

    # get data from last experiment with CFU
    xlsx = pd.read_excel(os.path.join(path_data, filenames[2]), sheet_name='OD')
    cfu_data = pd.read_excel(os.path.join(path_data, filenames[2]), sheet_name='CFU', header=0, index_col=0)

    times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
    strains = xlsx.iloc[0, :].to_numpy()[1:]
    medias = xlsx.iloc[1, :].to_numpy()[1:]
    a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[6:, 1:].astype(np.float64)
    strains_unique = np.unique(strains)
    concentrations = np.unique(a0s)

    exps = exps + [esbl.Experiments() for i in range(strains_oi.size)]

    already_used = {s + '_' + str(c): False for s in strains_oi for c in np.unique(a0s)}

    for i in range(wells.shape[1]):
        strain = strains[i]
        if strain in strains_oi:
            a0 = a0s[i]
            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]
            ind_s = np.where(strains_oi == strain)[0][0]

            cfu_c = cfu_data[(cfu_data['time'] > 0) & (cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                        cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) & (cfu_data['CFU.well'] > 0)]
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus=times_cfus,
                                          cfus=cfus,
                                          stdcfus=cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + ind_s].add(exp)
                exps_ref_ind[strain + '_mono2'] = n_exps1 + ind_s
            already_used[strain + '_' + str(a0)] = True

    exps_for_fit = [] #cond.size + strains_oi.size
    for c in cond:
        s, _ = c.split(',')
        exps_for_fit.append(esbl.Experiments(exps[exps_ref_ind[s + '_mono']].exps + exps[exps_ref_ind[c]].exps))
    for s in strains_oi:
        exps_for_fit.append(esbl.Experiments(
            [e for key, ind in exps_ref_ind.items() for e in exps[ind].exps if s in key and 'mono0' not in key]
        ))

    # ### PLOT ###
    ns = strains_oi.size
    nm = len(exps) // ns
    f1, ax1 = plt.subplots(ns, nm, figsize=(3 * nm, 3 * ns))

    media_plots = np.array(['glucose+tween0.1%'])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))

    for k in range(strains_oi.size):
        s = strains_oi[k]
        indexes = [ind for key, ind in exps_ref_ind.items() if s in key]
        for i in range(len(indexes)):
            for j in range(len(exps[indexes[i]].exps)):
                exp = exps[indexes[i]].exps[j]
                label = str(exp.a0)
                if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                    exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                    # label = ''
                    # for t in range(exp.timesinjs.size):
                    #     # exp_cond += str(exp.volsinjs[i]) + 'at' + str(exp.timesinjs[i])
                    #     label += (t > 0) * '+' + str(int(1000 * exp.concinj / exp.v0 * exp.volsinjs[t])) + 'at' + str(int(exp.timesinjs[t]))
                    for t in exp.timesinjs:
                        ax1[k, i].axvline(x=t, color='red', linestyle='--', linewidth=2)
                    ax1[k, i].set_title(exp_cond)

                ind_c = np.where(concentrations == exp.a0)[0][0]
                sns.scatterplot(ax = ax1[k, i], x = exp.times, y = exp.ods, s = 6, label = label,
                                color = colors[ind_c], legend = (i == len(indexes) - 1)).set( yscale = 'log', ylim = (2e-4, 1.2), xlim = (0, 27))

        sns.move_legend(ax1[k, -1], "upper left", bbox_to_anchor=(1, 1))
        ax1[k, 0].set_ylabel(s)
    f1.tight_layout()
    f1.savefig('plots/experiments_ib3678.png')
    plt.close()

    # plot mono dose with cfus
    ns = strains_oi.size
    nm = 2 #len(exps) // ns
    f1, ax1 = plt.subplots(nm, ns, figsize=(4 * ns, 4 * nm))
    plt.rcParams.update({'font.size': 15})

    media_plots = np.array(['glucose+tween0.1%'])
    colors = plt.cm.tab20(np.linspace(0, 1, max(20, concentrations.size)))

    for k in range(strains_oi.size):
        s = strains_oi[k]
        i = exps_ref_ind[s + '_mono2']
        # indexes = [ind for key, ind in exps_ref_ind.items() if s in key]
        # for i in range(len(indexes)):
        for j in range(len(exps[i].exps)):
            exp = exps[i].exps[j]
            label = str(exp.a0)
            ind_c = 2 * (np.where(concentrations == exp.a0)[0][0]) + int(j >= len(exps[i].exps) // 2)
            sns.scatterplot(ax=ax1[0, k], x=exp.times, y=exp.ods, s=6, label=label,
                            color=colors[ind_c], legend=(k == strains_oi.size - 1)).set(yscale='log', ylim=(2e-4, 1.2),
                                                                                     xlim=(0, 13))

            sns.scatterplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus, s=6,
                            color=colors[ind_c]).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 13))
            sns.lineplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus, color=colors[ind_c])
            ax1[1, k].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle = '--')
            ax1[1, k].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle = '--')

        ax1[0, k].set_title(s)
    sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1))
    f1.tight_layout()
    f1.savefig('plots/experiments_ib3678_cfus_1.png')
    plt.close()


    exps_for_fit_cfu = []
    for i in range(strains_oi.size):
        exp1, exp2 = np.array_split(np.array(exps[exps_ref_ind[strains_oi[i] + '_mono2']].exps), 2)
        exps_for_fit_cfu.append(esbl.Experiments(list(exp1)))
        exps_for_fit_cfu.append(esbl.Experiments(list(exp2)))

    # fits_summary = pd.read_excel('/mnt/y/cluster/fits_summary_clustercomb.xlsx', sheet_name='3', header=0, index_col=0)
    # path = '/mnt/c/Users/vgross/PhD/data/results/platereader/tween fits/new_esbl_version'
    fits_summary = pd.read_excel(os.path.join(path_fits, 'fits_summary_all_strains_ddose.xlsx'), sheet_name='complete_2102', header=0, index_col=0)
    paramlist = list(esbl.params.BOUNDS.keys())#(fits_summary.columns).to_numpy()[7:]
    # # fits_summary = fits_summary.drop_duplicates(subset=['cost', 'cost_all'] + list(paramlist))
    # # print(fits_summary)
    fit_names = (fits_summary['fitname']).to_numpy()
    # exp_conds = np.array(list(exps_ref_index.keys()))
    ind_cfu = np.where(np.array(fits_summary.columns) == 'cost_cfu')[0][0]

    strains_1 = ['IB307', 'IB308']
    # calculate cost_cfu
    for n in range(fits_summary.shape[0]):
        if fits_summary.iloc[n, :]['strain'] in strains_1: # == strain:
            cost_cfu = np.mean([exp.cost_cfu(fits_summary.iloc[n, :][paramlist].to_dict(),
                                    odejax=True) for exp in
                                     exps_for_fit_cfu[2 * np.where(strains_oi == fits_summary.iloc[n, :]['strain'])[0][0] + 1].exps])
            fits_summary.iloc[n, ind_cfu] = cost_cfu

    with pd.ExcelWriter(os.path.join(path, 'fits_summary_all_strains_ddose.xlsx'), mode='a') as writer:
        fits_summary.to_excel(writer, sheet_name='2102_cfu1')

    # fits_summary = res_fits
    paramlist = (fits_summary.columns).to_numpy()[7:]
    # fits_summary = fits_summary.drop_duplicates(subset = ['cost', 'cost_all'] + list(paramlist))
    # fits_summary = fits_summary[fits_summary['strain'] == strain]


    ### PLOT SIMULATIONS

    timescfus = np.array([i for i in range(0, 13, 2)])

    for k in range(strains_oi.size):
        s = strains_oi[k]
        fits = fits_summary[fits_summary['strain'] == s]
        nf = fits.shape[0]
        indexes = np.array([exps_ref_ind[s + '_mono']] + [ind for key, ind in exps_ref_ind.items() if s in key and 'mono' not in key])
        names = np.array([s + '_mono'] + [key for key, ind in exps_ref_ind.items() if s in key and 'mono' not in key])

        exp_ref = exps[exps_ref_ind[s + '_mono2']].exps[0]
        times_end = exp_ref.timescfus[0]
        eta = exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))] / exp_ref.cfus[0] #exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))]

        nx = 2
        ny = indexes.size
        for n in range(nf) :
            fit = fits.iloc[n, :][paramlist].to_dict()
            fitname = str(fits.iloc[n, :]['fitname'])
            # if not os.path.exists(os.path.join(os.getcwd(), 'plots_new_fits/' + fitname + '_cfu_1.png')):
            f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx))
            f1.suptitle(s + ', ' + fitname)

            sns.set(font_scale=2)
            sns.set_theme(style='white')
            # fitting results
            for j in range(len(exps[exps_ref_ind[s + '_mono2']].exps)):
                exp = exps[exps_ref_ind[s + '_mono2']].exps[j]
                label = str(exp.a0)
                ind_c = 2 * (np.where(concentrations == exp.a0)[0][0]) + int(j >= len(exps[exps_ref_ind[s + '_mono2']].exps) // 2)

                sns.scatterplot(ax=ax1[0, 0], x=exp.times, y=exp.ods, s=10, label=label if j < len(exps[exps_ref_ind[s + '_mono2']].exps) // 2 else None ,
                                    color=colors[ind_c], legend=False).set(yscale='log', ylim=(2e-4, 1.2),
                                                                                                xlim=(0, 13), title = 'monodose, OD')

                sns.lineplot(ax=ax1[0, 1], x=exp.timescfus, y=exp.cfus, label=label if j < len(exps[exps_ref_ind[s + '_mono2']].exps) // 2 else None ,
                                color=colors[ind_c], legend = True).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 13), title = 'monodose, CFU')
                ax1[0, 1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
                ax1[0, 1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

                # if (j < len(exps[i].exps) // 2 and 'monocfu_0_' in fitname) or (j >= len(exps[i].exps) // 2 and 'monocfu_1_' in fitname):


                solts = exp.simulate(fit, odejax=True)
                times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in timescfus])
                ods = solts[0] * solts[1] + solts[5] + solts[6]
                cfus = solts[0] / fit['eta']
                cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in timescfus])
                sns.lineplot(ax=ax1[0, 0], x=exp.times, y=ods, color=colors[ind_c], linewidth=3, linestyle = '--',
                             legend=False)
                sns.lineplot(ax=ax1[0, 1], x=times_selected_points, y=cfus_selected_points, color=colors[ind_c],
                             linewidth=2, legend=False, linestyle='--')#.set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))

            for y in range(2, ax1.shape[1]):
                ax1[0, y].set_axis_off()
            sns.move_legend(ax1[0, 1], "upper left", bbox_to_anchor=(1, 1))

            # prediction results
            for i in range(indexes.size):
                for j in range(len(exps[indexes[i]].exps)):
                    exp = exps[indexes[i]].exps[j]
                    label = str(exp.a0)
                    if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                        exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                        for t in exp.timesinjs:
                            ax1[1, i].axvline(x=t, color='red', linestyle='--', linewidth=2)
                        ax1[1, i].set_title(exp_cond)
                    else:
                        ax1[1, i].set_title(names[i])

                    ind_c = 2 * np.where(concentrations == exp.a0)[0][0]
                    sns.scatterplot(ax=ax1[1, i], x=exp.times, y=exp.ods, s=10, label=label,
                                        color=colors[ind_c], legend=(i == ny - 1)).set(yscale='log',
                                                                                                 ylim=(2e-4, 1.2),
                                                                                                 xlim=(0, 27))


                    solts = exp.simulate(fit, odejax=True)
                    ods = solts[0] * solts[1] + solts[5] + solts[6]
                    sns.lineplot(ax=ax1[1, i], x=exp.times, y=ods, linewidth = 3, alpha = 0.6, linestyle = '--', label = str(exp.a0) + ',sim', color=colors[ind_c], legend = (i == ny - 1))

            sns.move_legend(ax1[1, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)

            f1.tight_layout()
            f1.subplots_adjust(wspace = 0.2)
            f1.savefig('plots_new_fits/' + fitname + '_cfu_1.png')
            f1.savefig('plots_new_fits/' + fitname + '_cfu_1.svg')
            plt.close()
