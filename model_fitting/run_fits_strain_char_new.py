import esbl
import esbl.params
import esbl.model

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import yaml
import multiprocessing as mp
import os
import seaborn as sns
import glob

import warnings
warnings.filterwarnings('ignore', category = RuntimeWarning)

def run_search(out_q, experiment, params={}, fixed={}, odejax = True, method = 'cmaes', samples = 500, include_cfus = False,
               media_condition='IB302_glucose+tween', verb_filenameprefix='outcmaes_parallel_new/'):

    # run fits
    # try:
        if method == 'cmaes':
            if params != {}:
                sols = experiment.search(params=params, fixed = fixed, method=method,
                                     odejax=odejax, verbose=1, include_cfus=include_cfus,
                                     media_condition=media_condition, verb_filenameprefix=verb_filenameprefix)
            else:
                sols = experiment.search(fixed=fixed, method=method,
                                         odejax=odejax, verbose=1, include_cfus=include_cfus,
                                         media_condition=media_condition, verb_filenameprefix=verb_filenameprefix)
        elif method == 'lhs':
            if params != {}:
                sols = experiment.search(params=params, fixed=fixed, method=method, samples=samples, include_cfus=include_cfus,
                                     odejax=odejax, verbose=1, media_condition=media_condition)
            else:
                sols = experiment.search(fixed=fixed, method=method, samples = samples, include_cfus=include_cfus,
                                         odejax=odejax, verbose=1, media_condition=media_condition)
        out_q.put(sols)

        return sols

if __name__ == "__main__":
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    filename = ['20230627_OD.xlsx', '20230706_OD.xlsx', '20230907_OD.xlsx', '20231109_OD.xlsx']
    exps_ref_ind = {}
    strains_oi = np.array(['NILS1', 'NILS11', 'NILS12', 'NILS19',
                           'NILS42', 'NILS56',
                           'IB306', 'IB307', 'IB308', 'IB302', 'IB311', 'NILS31',
                           'NILS64'])
    exps = []
    n_exps1 = 0
    for f in range(len(filename)):
        # elif f == 1: cfu_data = pd.read_excel(filename[f], sheet_name='CFU_1', header=0, index_col=0)
        if f == 2:
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(filename[f], sheet_name='OD_1', header=0)
        elif f < 2:
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU_2', header=0, index_col=0)
            od_data = pd.read_excel(filename[f], sheet_name='OD', header=0)
        else:
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD', header=0)

        times = od_data["Unnamed: 0"].to_numpy()[6:].astype(np.float64)

        strains = od_data.iloc[0, :].to_numpy()[1:]
        strains_unique = np.unique(strains)
        if f < 2:
            a0s = od_data.iloc[1, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[2, :].to_numpy()[1:]
        else:
            a0s = od_data.iloc[2, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[1, :].to_numpy()[1:]
        # timesinjs = xlsx.iloc[2, :].to_numpy()[1:]
        # volsinjs = xlsx.iloc[3, :].to_numpy()[1:]
        # concinjs = xlsx.iloc[4, :].to_numpy()[1:]
        od0 = 5e-4
        v0 = 200
        wells = od_data.iloc[6:, 1:].astype(np.float64)
        print(wells.shape, a0s.size, strains_unique)
        for i in range(2 * strains_unique.size):
            exps.append(esbl.Experiments())

        already_used = {s + '_' + str(a0): False for s in strains_unique for a0 in np.unique(a0s)}

        bound = wells.shape[1] if f < 3 else wells.shape[1] // 2

        for i in range(min(96, bound)):
            strain = strains[i]
            a0 = a0s[i]
            m = media[i]

            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]

            ind_s = 2 * np.where(strains_unique == strain)[0][0] + int(already_used[strain + '_' + str(a0)])

            cfu_c = cfu_data[(cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                        cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) &
                             (cfu_data['CFU.well'] > 0) & (cfu_data['time'] > 0)]
            # print(cfu_c)
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
            # print(times_cfus, cfus, cfu_c.groupby('time')['CFU.well'].std().to_numpy())

            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus=times_cfus,
                                          cfus=cfus,
                                          stdcfus=cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + ind_s].add(exp)
                exps_ref_ind[strain + '_monocfu' + str(int(already_used[strain + '_' + str(a0)]))] = n_exps1 + ind_s
            else:
                print(strain, a0, i)
            already_used[strain + '_' + str(a0)] = True

        n_exps1 += 2 * strains_unique.size

    # strains_oi = np.array(['NILS1', 'NILS11', 'NILS12', 'NILS19', 'NILS42', 'NILS56',
    #                        'IB306', 'IB307', 'IB308', 'IB302', 'IB311', 'NILS31'])

    max_conc = {'NILS1' : 8, 'NILS11' : 8, 'NILS12' : 32, 'NILS19' : 16, 'NILS42' : 64, 'IB311' : 128, 'NILS31' : 128}
    exps_for_fit = []
    exps_for_fit_growth = []
    names_for_fit = []
    for s in strains_oi:
        if s not in max_conc.keys() :
            exps_for_fit.append(exps[exps_ref_ind[s + '_monocfu1']])
        else:
            exps_for_fit.append(esbl.Experiments([
                e for e in exps[exps_ref_ind[s + '_monocfu1']] if e.a0 <= max_conc[s]
            ]))
        exps_for_fit_growth.append(esbl.Experiments([
            e for e in exps[exps_ref_ind[s + '_monocfu1']] if e.a0 == 0]))
        names_for_fit.append(s + '_monocfu1')
    names_for_fit = np.array(names_for_fit)


    ### PLOT ###
    nx = 4
    ny = (strains_oi.size + 1) // 2
    concentrations = np.array([0] + [2 ** i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    fig, ax1 = plt.subplots(nx, ny, sharey = False, sharex = False, figsize = (4 * ny, 4 * nx))
    for k in range(strains_oi.size):
        i = np.where(names_for_fit == strains_oi[k] + '_monocfu1')[0][0] #exps_ref_ind[strains_oi[k] + '_monocfu0']
        for j in range (len(exps_for_fit[i].exps)):
            exp = exps_for_fit[i].exps[j]
            sns.scatterplot(ax=ax1[2 * (k // ny), k % ny], x=exp.times, y=exp.ods, s=4, label=exp.a0,
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=k == ny - 1).set(
                yscale='log', ylim=(2e-4, 1), xlim=(0, 25), title=strains_oi[k])
            sns.lineplot(ax=ax1[2 * (k // ny) + 1, k % ny], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale='log', ylim=(1e2, 5e9),
                                                                                          xlim=(0, 25))
            sns.scatterplot(ax=ax1[2 * (k // ny) + 1, k % ny], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]])
            ax1[2 * (k // ny) + 1, k % ny].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[2 * (k // ny) + 1, k % ny].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[2 * (k // ny), k % ny].axvline(1, color='gray', linestyle = '--', linewidth = 2)

    sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1))
    fig.tight_layout()
    fig.savefig('experiments_cfus_fits_1.png')
    plt.close()

    strains_nonsus = np.array(['IB302', 'IB306', 'IB307', 'IB308', 'IB311', 'NILS31', 'NILS42', 'NILS64'])
    nx = 2
    ny = (strains_nonsus.size + 2) // 2
    concentrations = np.array([0] + [2 ** i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    plt.rcParams['svg.fonttype'] = 'none'
    fig, ax1 = plt.subplots(nx, ny, sharey=False, sharex=False, figsize=(3 * ny, 3 * nx))

    sns.set(font_scale=1.5)
    sns.set_theme(style='white')
    sns.set(rc={'axes.labelsize': 14})

    for k in range(strains_nonsus.size):
        i = np.where(names_for_fit == strains_nonsus[k] + '_monocfu1')[0][0]  # exps_ref_ind[strains_oi[k] + '_monocfu0']
        for j in range(len(exps_for_fit[i].exps)):
            exp = exps_for_fit[i].exps[j]
            sns.scatterplot(ax=ax1[(k // ny), k % ny], x=exp.times, y=exp.ods, s=4,
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=False).set(
                yscale='log', ylim=(2e-4, 1), xlim=(0, 25), title=strains_nonsus[k])

    # sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1))
    fig.tight_layout()
    fig.savefig('experiments_od.png')
    fig.savefig('experiments_od.svg', format = 'svg')
    plt.close()

    # Run fits ###
    strains_to_fit = strains_oi #np.array(['IB311', 'NILS64'])
    rep_number = {}
    for strain in strains_to_fit:
        list_of_fits = glob.glob(f'res*{strain}*.yaml')  # * means all if need specific format then *.csv
        if len(list_of_fits) > 0:
            latest_file = max(list_of_fits, key=os.path.getctime)
            name = os.path.basename(latest_file)
            name_parts = list((name.split('.yam')[0]).split('_'))
            rep_number[strain] = int(name_parts[-1])
            print(latest_file, rep_number)
        else:
            rep_number[strain] = 0

    ### STEP 1: fit growth curves without treatment
    unfixed_params_names = ['beta', 'mu', 'ks', 'lambda', 'eta']
    ps = esbl.params.ParamScaler()
    middle = ps.middle()
    fixed = {name : float(middle[name]) for name in middle.keys() if name not in unfixed_params_names}

    processes = []
    out_q = mp.Queue()
    for n in range(5):
        for i in range(len(exps_for_fit_growth)):
            s, _ = names_for_fit[i].split('_', 2)
            if s in strains_to_fit:
                exp_cond = names_for_fit[i] + '_growth_' + str(rep_number[s] + 1 + n)
                p = mp.Process(
                    target=run_search,
                    args=(out_q, exps_for_fit_growth[i], middle, fixed, True, 'lhs', 2000, True, exp_cond,
                          'outcmaes_mp_new/' + exp_cond)
                )
                processes.append(p)
                p.start()

                p = mp.Process(
                    target=run_search,
                    args=(out_q, exps_for_fit_growth[i], middle, fixed, True, 'cmaes', 2000, True, exp_cond,
                          'outcmaes_mp_new/' + exp_cond)
                )
                processes.append(p)
                p.start()

    for i in range(len(processes)):
        res = out_q.get()
    for p in processes:
        p.join()

    ### STEP 2: fit other curves, while fixing previously identified paramters

    fixed_params_names = ['beta', 'mu', 'ks', 'lambda', 'eta']
    # best_method = {s : 'lhs' if s in ['IB311'] else 'cmaes' for s in strains_to_fit} # growth
    best_method = {s : 'lhs' if s in ['NILS31', 'IB311', 'IB308', 'IB306', 'NILS56', 'NILS12', 'NILS1'] else 'cmaes' for s in strains_oi} # from growth
    fit_ref = {}
    # # # fixed_all = {}
    for s in strains_oi:
        if s not in ['IB311', 'NILS64']:
            with open('res_' + best_method[s] + '_' + s + '_monocfu1_growth_9.yaml') as file:
                fit_ref['s'] = yaml.safe_load(file)
    with open('res_lhs_IB311_monocfu1_growth_20.yaml') as file:
        fit_ref['IB311'] = yaml.safe_load(file)
    with open('res_cmaes_NILS64_monocfu1_growth_5.yaml') as file:
        fit_ref['NILS64'] = yaml.safe_load(file)

    processes = []
    out_q = mp.Queue()

    for n in range(5):
        for i in range(len(exps_for_fit)):
            s, _ = names_for_fit[i].split('_', 2)
            if s in strains_to_fit:
                print(s)
                exp_cond = names_for_fit[i] + '_fromgrowth_' + str(rep_number[s] + 1 + n)

                fixed = {key : value for key, value in fit_ref[s].items() if key in fixed_params_names}

                p = mp.Process(
                    target=run_search,
                    args=(out_q, exps_for_fit[i], fit_ref[s], fixed, True, 'lhs', 2000, True, exp_cond,
                          'outcmaes_mp_new/' + exp_cond)
                )
                processes.append(p)
                p.start()

                p = mp.Process(
                    target=run_search,
                    args=(out_q, exps_for_fit[i], fit_ref[s], fixed, True, 'cmaes', 2000, True, exp_cond,
                          'outcmaes_mp_new/' + exp_cond)
                )
                processes.append(p)
                p.start()

    for i in range(len(processes)):
        res = out_q.get()
    for p in processes:
        p.join()
