import os.path
import seaborn as sns
import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp
from collections import OrderedDict
from matplotlib.ticker import MultipleLocator
import yaml
import os
import glob
from pathlib import Path
import itertools


if __name__ == "__main__":
    # experimental data
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')
    path_plots = os.getcwd()

    xlsx = pd.read_excel(os.path.join(path_data, "20230207_OD_IB302_ddose.xlsx"), sheet_name='OD_1')
    cfu_data = pd.read_excel(os.path.join(path_data, "20230207_OD_IB302_ddose.xlsx"), sheet_name='CFU', header = 0, index_col=0) # get new data file

    times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
    a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(str)#.astype(np.float64)
    volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(str)#.astype(np.float64)
    concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[6:, 1:].astype(np.float64)
    cond = np.unique(np.array([str(volsinjs[i]) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size)]))
    cond = cond[cond != 'nanatnan']
    n_exps = cond.size
    exps = [esbl.Experiments() for i in range(n_exps + 1)]  # separate repicates into diferent experiments
    exps_ref_index = {}
    strain = 'IB302'
    for i in range(wells.shape[1]):
        a0 = a0s[i]
        tinjs = np.array([]) if timesinjs[i] == 'nan' else np.array([float(timesinjs[i])]) if '+' not in timesinjs[i] else np.array(timesinjs[i].split('+')).astype(float)
        vinjs = np.array([]) if volsinjs[i] == 'nan' else np.array([float(volsinjs[i])]) if '+' not in volsinjs[i] else np.array(volsinjs[i].split('+')).astype(float)
        concinj = concinjs[i]
        ods = wells.iloc[:, i].to_numpy().astype(np.float64)
        mask = np.isnan(ods) == 0
        try:
            ods = esbl.utils.smooth(ods[mask])
        except:
            ods = ods[mask]

        if tinjs.size > 0:
            exp_cond_cfu = str(int(1000 * concinj / 200 * vinjs[0])) + 'at' + np.array2string(tinjs.astype(int), separator='+')[1:-1]
            exp_cond_ref = np.array2string(vinjs, separator='+')[1:-1] + 'at' + np.array2string(tinjs, separator='+')[1:-1]
            rep = 1
            try:
                ind = np.where(cond == exp_cond_ref)[0][0] + 1
            except:
                v, t = exp_cond_ref.split('at')
                exp_cond_ref = v + 'at' + t[1:]
                v, t = exp_cond_cfu.split('at')
                exp_cond_cfu = v + 'at' + t[1:]
                ind = np.where(cond == exp_cond_ref)[0][0] + 1
            cfu_i = cfu_data[
                (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (cfu_data['rep'] == rep) & (
                        cfu_data['conc'] == a0) & (cfu_data.time > 0)]
            times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
            cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
            std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()

            exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                  v0=v0, concinj=concinj,
                                  timesinjs=tinjs, volsinjs=vinjs,
                                  timescfus=times_cfus, cfus=cfus, stdcfus=cfus, strain=strain)
            exps[ind].add(exp)
            exps_ref_index[exp_cond_cfu] = ind
        else:
            exp_cond_cfu = 'mono'
            rep = 1
            cfu_i = cfu_data[
                (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (cfu_data['rep'] == rep) & (
                        cfu_data['conc'] == a0)]
            times_cfus =  np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
            cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
            std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()

            exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, timescfus=times_cfus, cfus=cfus, stdcfus=std,
                                  strain=strain)
            exps[0].add(exp)
            exps_ref_index['mono'] = 0

    ### PLOT ###
    ns = 3
    media_plots = np.array(['glucose+tween0.1%'])
    concentrations = np.unique(a0s.astype(int))
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    exp_conds_to_plot = np.array(['mono', '16at2', '16at4', '16at6'])
    nm = exp_conds_to_plot.size
    ns = 2
    plt.rcParams['svg.fonttype'] = 'none'
    f1, ax1 = plt.subplots(ns, nm, sharex=False, sharey=False, figsize=(3 * nm, 3*ns))
    sns.set(font_scale = 1.5)
    sns.set_theme(style = 'white')
    exp_conds = np.array(list(exps_ref_index.keys()))
    for k in range(exp_conds_to_plot.size):
        for exp in exps[exps_ref_index[exp_conds_to_plot[k]]].exps:
            sns.scatterplot(ax = ax1[0, k], x = exp.times, y = exp.ods, s=10, label=str(int(exp.a0)) + ' mg/L' if k == nm - 1 else None,
                              color=colors[np.where(concentrations == int(exp.a0))[0][0]], ).set(yscale = 'log',
                                ylim = (2e-4, 1), xlim = (0, 25), title = exp_conds_to_plot[k])
            sns.lineplot(ax=ax1[0, k], x=exp.times[exp.times > 5], y=esbl.utils.smooth(exp.ods[exp.times > 5]),
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]])
            sns.lineplot(ax = ax1[1, k], x = exp.timescfus, y = exp.cfus, color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale = 'log', ylim = (1e2, 5e9), xlim = (0, 25))
            sns.scatterplot(ax = ax1[1, k], x = exp.timescfus, y = exp.cfus, s = 20, color=colors[np.where(concentrations == int(exp.a0))[0][0]])
            ax1[1, k].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1, k].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')
    sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), markerscale = 4)
    f1.tight_layout()
    f1.savefig('experiments_double_dose_IB302_1.png')
    f1.savefig('experiments_double_dose_IB302_1.svg', format = 'svg')

    ### EXPERIMENTS FOR FITS ###
    exps_for_fit = []
    names_for_fit = []
    ''' 
        1/ mono + 1 r2 
        2/ mono + 3r2 : t2, t4, t6 
        3/ all of the experiments 

        for mono use all rep 2 , for the rest separate the replicates but use both (not sumultaneously) 
    '''
    exps_for_fit = []
    names_for_fits = []
    exp_conds_ddose = np.array([key for key in list(exps_ref_index.keys()) if 'at' in key and '+' not in key])

    # mono
    exps_for_fit.append(exps[exps_ref_index['mono']])
    names_for_fits.append('mono')

    # mono + 1r2
    for i in range(exp_conds_ddose.size):
        exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono']].exps +
                                                 exps[exps_ref_index[exp_conds_ddose[i]]].exps))
        names_for_fits.append('mono+' + exp_conds_ddose[i])

    # mono + 2 r2
    ddose_combinations = itertools.combinations(exp_conds_ddose, 2)
    for r1, r2 in ddose_combinations:
        exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono']].exps +
                                             exps[exps_ref_index[r1]].exps +
                                             exps[exps_ref_index[r2]].exps))
        names_for_fits.append('mono+' + r1 + '+' + r2)

    # mono + 3r2
    ddose_combinations = itertools.combinations(exp_conds_ddose, 3)
    for r1, r2, r3 in ddose_combinations:
        exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono']].exps +
                                             exps[exps_ref_index[r1]].exps +
                                             exps[exps_ref_index[r2]].exps +
                                             exps[exps_ref_index[r3]].exps))
        names_for_fits.append('mono+' + r1 + '+' + r2 + '+' + r3)

    # # mono + 4r2
    # ddose_combinations = itertools.combinations(exp_conds[exp_conds != 'mono'], 4)
    # for r1, r2, r3, r4 in ddose_combinations:
    #     exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps +
    #                                          exps[exps_ref_index[r1 + '_1']].exps +
    #                                          exps[exps_ref_index[r2 + '_1']].exps +
    #                                          exps[exps_ref_index[r3 + '_1']].exps +
    #                                          exps[exps_ref_index[r4 + '_1']].exps))
    #     names_for_fits.append('mono+' + r1 + '+' + r2 + '+' + r3 + '+' + r4)

    exps_for_fit.append(
        esbl.Experiments(exps[exps_ref_index['mono']].exps +
                         [e for cond in exp_conds_ddose for e in exps[exps_ref_index[cond]].exps]))
    names_for_fits.append('all')
    names_for_fits = np.array(names_for_fits)


    # fits_summary = pd.read_excel('ib302/fits_summary_ib302.xlsx', sheet_name='15', header = 0, index_col=0)
    # fits_summary = pd.read_excel('/mnt/y/cluster/fits_summary_clustercomb.xlsx', sheet_name='1', header = 0, index_col=0)
    fits_summary = pd.read_excel('fits_summary_all_strains_ddose.xlsx', sheet_name='full_2501', header = 0, index_col=0)
    paramlist = esbl.params.BOUNDS.keys() #(fits_summary.columns).to_numpy()[7:]
    # # fits_summary = fits_summary.drop_duplicates(subset=['cost', 'cost_all'] + list(paramlist))
    fit_names = (fits_summary['fitname']).to_numpy()
    exp_conds = np.array(list(exps_ref_index.keys()))
    ind_cfu = np.where(np.array(fits_summary.columns) == 'cost_cfu')[0][0]

    # calculate cost_cfu
    for n in range(fits_summary.shape[0]):
        if fits_summary.iloc[n, :]['strain'] == strain:
            cost_cfu_mono = np.mean([exp.cost_cfu(
                fits_summary.iloc[n, :][paramlist].to_dict(),
                odejax=True) for exp in exps[exps_ref_index['mono']].exps])
            fits_summary.iloc[n, ind_cfu] = cost_cfu_mono

    # print(fits_summary[fits_summary['strain'] == strain])
    # with pd.ExcelWriter('/mnt/y/cluster/fits_summary_clustercomb.xlsx', mode = 'a') as writer:
    with pd.ExcelWriter('fits_summary_all_strains_ddose.xlsx', mode = 'a') as writer:
        fits_summary.to_excel(writer, sheet_name='2501_cfu1')


    nx = 2
    ny = 4
    exp_conds_unique = np.unique([s[:-2] for s in exp_conds])[::-1]
    exp_ref = exps[exps_ref_index['mono']].exps[0]
    times_end = exp_ref.timescfus[0]
    eta = exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))] / exp_ref.cfus[0] #exp_ref.od0 / exp_ref.cfus[0]  # exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))]
    # exp_conds_to_plot = np.array([c for c in exp_conds if ('mono' in c or '32at2' in c or '32at4' in c or '32at6' in c) and ('+' not in c) ])
    exp_conds_to_plot = np.array(['mono', '16at2', '16at4', '16at6'])
    # print(exp_conds_to_plot)
    # fits_summary_all = pd.read_excel('fits_summary_all_strains_ddose.xlsx', header = 0, index_col = 0)
    # fits_summary_all = fits_summary_all[(fits_summary_all.strain == strain) & (fits_summary_all.fit_type == 'mono+3r2') & (fits_summary_all.exp_cond == 'mono+32at2+32at4+32at6')]
    fits_summary_all = fits_summary[(fits_summary['strain'] == 'IB302') & (fits_summary['exp_cond'] == 'mono+16at2+16at4+16at6') & (fits_summary.cost < 300)]
    # fits_summary_all = fits_summary[fits_summary['exp_cond'] == 'mono+16at2+16at4+16at6']
    nf = fits_summary_all.shape[0]
    # print(nf)
    # print(fits_summary_all)

    totalas = np.array([32, 16])

    for n in range(nf):
        sns.set(font_scale=2)
        # sns.set_theme(style='white')
        sns.set(rc={'axes.labelsize': 16,
                    'axes.facecolor': 'white', 'figure.facecolor': 'white', 'axes.edgecolor': 'black',
                    'axes.spines.right': False, 'axes.spines.top': False,
                    'axes.grid': True, 'grid.color': 'lightgrey',
                    'xtick.color': 'black', 'ytick.color': 'black', })

        f1, ax1 = plt.subplots(nx, ny, figsize=(3.5 * ny, 3.5 * nx))

        fit = fits_summary_all.iloc[n, :][paramlist].to_dict()
        fitname = str(fits_summary_all.iloc[n, :]['fitname'])
        f1.suptitle(strain + ', ' + fitname)
        # sns.set(font_scale = 3)
        # sns.set_theme(style = 'white')
        sns.color_palette("husl")
        counts = [0, 0]

        for i in range(exp_conds.size):
            # fitting results
            if exp_conds[i] in exp_conds_to_plot:
                y = np.where(exp_conds_to_plot == exp_conds[i])[0][0]
                for j in range(len(exps[exps_ref_index[exp_conds[i]]].exps)):
                    exp = exps[exps_ref_index[exp_conds[i]]].exps[j]
                    label = str(exp.a0)
                    if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                        exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                        for t in exp.timesinjs:
                            ax1[0, y].axvline(x=t, color='red', linestyle='--', linewidth=2)
                        ax1[0, y].set_title(exp_cond)
                    else:
                        ax1[0, y].set_title(exp_conds[i])

                    ind_c =  np.where(concentrations == exp.a0)[0][0]
                    sns.scatterplot(ax=ax1[0, y], x=exp.times, y=exp.ods, s=10, label=label,
                                    color=colors[ind_c], legend=(y == ny - 1)).set(yscale='log',
                                                                                   ylim=(2e-4, 1.2),
                                                                                   xlim=(0, 27))
                    solts = exp.simulate(fit, odejax=True)
                    ods = solts[0] * solts[1] + solts[5] + solts[6]
                    sns.lineplot(ax=ax1[0, y], x=exp.times, y=ods, linewidth=3, alpha=0.6, linestyle='--',
                                 label=str(exp.a0) + ',sim', color=colors[ind_c], legend=(y == ny - 1))

            # delayed treatment
            # ind_c = 2 * np.where(concentrations == exp.a0)[0][0]
            for a in range(totalas.size):
                totala = totalas[a]
                if '+' not in exp_conds[i] and (exp_conds[i] == 'mono' or str(totala) + 'at' in exp_conds[i]):
                    if exp_conds[i] == 'mono':
                        exp = [e for e in exps[exps_ref_index[exp_conds[i]]].exps if e.a0 == totala][0]
                        label = str(int(exp.a0)) + 'mg/L at t0'
                        color = colors[np.where(concentrations == exp.a0)[0][0]]
                    elif str(totala) + 'at' in exp_conds[i]:
                        exp = exps[exps_ref_index[exp_conds[i]]].exps[0]
                        label = str(totala) + 'mg/L at t' +str(int(exp.timesinjs[0]))
                        color = sns.color_palette("husl")[counts[a]] #plt.cm.lajolla(i / 10) #np.linspace(0, 1, max(10, concentrations.size)))
                        # print(a, counts)
                        counts[a] += 1
                    sns.scatterplot(ax=ax1[1, 2 * a], x=exp.times, y=exp.ods, s=10,  #exp_conds[i],
                                    color = color , legend = False)
                    # sns.lineplot(ax=ax1[1, 2 * a + 1], x=exp.timescfus, y=exp.cfus, label=label,
                    #              #label=label, #exp_conds[i],
                    #              color = color , )
                    ax1[1, 2 * a + 1].plot(exp.timescfus, exp.cfus,  linewidth = 2,
                                 color=color, )
                    sns.scatterplot(ax=ax1[1, 2 * a + 1], x=exp.timescfus, y=exp.cfus, s = 20, label=label,
                                              color = color)
                    solts = exp.simulate(fit, odejax=True)
                    ods = solts[0] * solts[1] + solts[5] + solts[6]
                    # print(totala, ods)
                    # sns.lineplot(ax=ax1[1, 2 * a], x=exp.times, y=ods, linewidth=2, alpha=0.6, linestyle='--',
                    #               color = color) #colors[ i])
                    ax1[1, 2 * a].plot(exp.times, ods,  alpha=0.6, linestyle='--', color=color, linewidth = 3)

                    times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                    cfus = solts[0] / eta  # fit['eta']
                    cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                    # sns.lineplot(ax=ax1[1, 2 * a + 1], x=times_selected_points, y=cfus_selected_points, color = color, # if exp_conds[i] == 'mono' else None,
                    #              linewidth=3, linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))
                    ax1[1, 2 * a + 1].plot(times_selected_points, cfus_selected_points, color=color, linestyle='--', linewidth = 3)

                ax1[1, 2 * a].set(yscale='log',ylim=(2e-4, 1.2),xlim=(0, 25))
                ax1[1, 2 * a + 1].set(yscale='log',ylim=(1e2, 5e9),xlim=(0, 25))
                ax1[1, 2 * a].xaxis.set_major_locator(MultipleLocator(2))
                ax1[1, 2 * a + 1].xaxis.set_major_locator(MultipleLocator(2))

        sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)
        sns.move_legend(ax1[1, -1], "upper left", bbox_to_anchor=(1, 1))
        # sns.move_legend(ax1[1,1], "upper left", bbox_to_anchor=(1, 1), ncol=2)

        f1.tight_layout()
        f1.subplots_adjust(wspace=0.2)
        f1.savefig('ib302/plots_delayed_treatments/' + fitname + '_cfu_delayed_white_2_newcolor.png')
        f1.savefig('ib302/plots_delayed_treatments/' + fitname + '_cfu_delayed_white_2_newcolor.svg')
        plt.close()


    # plot cfu simulations
    exp_conds = np.array(list(exps_ref_index.keys()))
    exp_conds_to_plot = np.array(['mono', '16at2', '16at4', '16at6'])
    # fits_selection = fits_summary[fits_summary['fitname'].isin(['res_lhs_IB302_mono_cluster_18.yaml', 'res_lhs_IB302_mono+16at2+16at4+16at6_cluster_5.yaml'])]
    # fits_selection = fits_summary[fits_summary['exp_cond'].isin(['mono', 'mono+16at2+16at4+16at6'])]
    fits_selection = fits_summary[fits_summary['strain'] == strain]
    nm = exp_conds_to_plot.size
    # timescfus = np.array([i for i in range(0, 13, 2)])
    nf = fits_selection.shape[0]
    exp_ref = exps[exps_ref_index['mono']].exps[0]
    times_end = exp_ref.timescfus[0]
    eta = exp_ref.od0 / exp_ref.cfus[0]  # exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))]
    # print(nf, names_for_fit)
    nx = 2
    ny = exp_conds_to_plot.size #+ 1) // 2
    for n in range(nf):
        fit = fits_selection.iloc[n, :][paramlist].to_dict()
        fitname = str(fits_selection.iloc[n, :]['fitname'])
        # if not os.path.exists(os.path.join(os.getcwd(), 'ib302/plots_1/' + fitname + '_cfu_pred.png')):
        f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx))
        f1.suptitle(strain + ', ' + fitname)
        sns.set(font_scale=2)
        sns.set_theme(style = 'white')
        # first row - OD double dose + simulations
        # second row - all CFUs + simulations
        for k in range(exp_conds_to_plot.size):
            for exp in exps[exps_ref_index[exp_conds_to_plot[k]]].exps:
                ind_c = np.where(concentrations == int(exp.a0))[0][0]
                # OD experiment
                # x = k #// ny
                # y =  #% ny
                sns.scatterplot(ax=ax1[0, k], x=exp.times, y=exp.ods, s=10, label=exp.a0,
                                color=colors[ind_c], legend=k == ny - 1).set(
                    yscale='log', ylim=(2e-4, 1), xlim=(0, 31), title=exp_conds_to_plot[k])

                # OD simulation
                solts = exp.simulate(fit, odejax=True)
                ods = solts[0] * solts[1] + solts[5] + solts[6]
                sns.lineplot(ax=ax1[0, k], x=exp.times, y=ods, linewidth=3, alpha=0.6, linestyle='--',
                             label=str(exp.a0) + ',sim', color=colors[ind_c], legend=(k == ny - 1))

                if exp_conds_to_plot[k] == 'mono':
                    # CFU experiment
                    sns.lineplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus,
                                 color=colors[ind_c]).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 31))
                    sns.scatterplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus,
                                    color=colors[ind_c])

                    # CFU simulation
                    # solts = exp.simulate(fit, odejax=True)
                    times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                    cfus = solts[0] / eta  # fit['eta']
                    cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                    sns.lineplot(ax=ax1[1, k], x=times_selected_points, y=cfus_selected_points,
                                 color=colors[ind_c],
                                 linewidth=2, legend=False, linestyle='--')

                    # detecton limits
                    ax1[1, k].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
                    ax1[1, k].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

        for k in range(1, ny):
            ax1[1, k].set_axis_off()
        sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)
        f1.tight_layout()
        f1.subplots_adjust(wspace=0.25)
        f1.savefig('ib302/plots_2/' + fitname + '_cfu_pred_white.png')
        f1.savefig('ib302/plots_2/' + fitname + '_cfu_pred_white.svg')
        plt.close()





