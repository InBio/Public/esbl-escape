import os.path
import seaborn as sns
import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp
from collections import OrderedDict
import yaml
import os
import glob
from pathlib import Path

if __name__ == "__main__":
    # experimental data
    path_plots = os.getcwd()
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    xlsx = pd.read_excel(os.path.join(path_data, "20230727_OD_NILS18_doubledose.xlsx"), sheet_name='OD')
    cfu_data = pd.read_excel(os.path.join(path_data, "20230727_OD_NILS18_doubledose.xlsx"), sheet_name='CFU_4',
                             header=0, index_col=0)  # get new data file

    # xlsx = pd.read_excel("experimental_data_3exepriments.xlsx")
    times = xlsx["Unnamed: 0"].to_numpy()[5:].astype(np.float64)
    a0s = xlsx.iloc[1, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[5:, 1:].astype(np.float64)
    cond = np.unique(np.array([str(volsinjs[i]) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size) if
                               type(timesinjs[i]) == str or np.isfinite(timesinjs[i])]))
    cond = cond[cond != 'nanatnan']
    n_exps = cond.size
    exps = [esbl.Experiments() for i in range(2 * (n_exps + 1))]  # separate repicates into diferent experiments
    exps_ref_index = {}
    already_used = {}
    already_used_od = {}

    strain = 'NILS18'

    for i in range(wells.shape[1]):
        a0 = a0s[i]
        tinjs = timesinjs[i]
        vinjs = volsinjs[i]
        concinj = concinjs[i]
        # ods = esbl.utils.smooth(wells.iloc[:, i])
        ods = wells.iloc[:, i].to_numpy().astype(np.float64)
        mask = np.isnan(ods) == 0
        try:
            ods = esbl.utils.smooth(ods[mask])
        except:
            ods = ods[mask]

        if type(tinjs) == str or np.isfinite(tinjs):
            # ind = np.where(cond == str(vinjs) + 'at' + str(tinjs))[0][0] + 1
            exp_cond_cfu = str(int(1000 * concinj / 200 * vinjs)) + 'at' + str((int(tinjs)))
            exp_cond_ref = str(int(1000 * concinj / 200 * vinjs)) + 'at' + str(tinjs)
            rep = 1
            if exp_cond_cfu + '_' + str(a0) in list(already_used.keys()): rep = 2
            cfu_i = cfu_data[
                (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (cfu_data['rep'] == rep) & (
                            cfu_data['conc'] == a0)]
            times_cfus = np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
            cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
            std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()

            # if '+' in str(tinjs):
            #     tinjs_1 = np.array(list(tinjs.split('+'))).astype(np.float64)
            #     vinjs_1 = np.array(list(vinjs.split('+'))).astype(np.float64)
            #     exp = esbl.Experiment(a0, od0, 1, times[mask], ods, #[mask],
            #                           v0=v0, volsinjs=vinjs_1, concinj=concinj, timesinjs=tinjs_1, timescfus = times_cfus, cfus = cfus, stdcfus = std)
            # else:
            exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                  v0=v0, volsinjs=np.array([vinjs]).astype(np.float64), concinj=concinj,
                                  timesinjs=np.array([tinjs]).astype(np.float64), timescfus=times_cfus, cfus=cfus,
                                  stdcfus=cfus, strain=strain)
            # ind_dd = np.where(cond_ddose == str(vinjs) + 'at' + str(tinjs))[0][0]
            # exps_ddose[ind_dd].add(exp)
            rep_od = 2 if exp_cond_ref + '_' + str(a0) in list(already_used_od.keys()) else 1
            ind = 2 * (np.where(cond == str(vinjs) + 'at' + str(tinjs))[0][0] + 1) + rep_od - 1

            exps[ind].add(exp)
            exps_ref_index[exp_cond_ref + '_' + str(rep - 1)] = ind
            already_used[exp_cond_cfu + '_' + str(a0)] = True
            already_used_od[exp_cond_ref + '_' + str(a0)] = True
        else:
            exp_cond_cfu = 'mono'
            rep = 1
            if exp_cond_cfu + '_' + str(a0) in list(already_used.keys()): rep = 2
            cfu_i = cfu_data[
                (cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0) & (cfu_data['rep'] == rep) & (
                        cfu_data['conc'] == a0)]
            times_cfus =  np.array(cfu_i.groupby('time', sort=True)['CFU.well'].median().index)
            cfus = cfu_i.groupby('time', sort=True)['CFU.well'].median().to_numpy()
            std = cfu_i.groupby('time', sort=True)['CFU.well'].std().to_numpy()
            exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, timescfus=times_cfus, cfus=cfus, stdcfus=std,
                                  strain=strain)  # [mask])
            rep_od = 2 if exp_cond_cfu + '_' + str(a0) in list(already_used_od.keys()) else 1
            exps[rep_od - 1].add(exp)
            exps_ref_index['mono_' + str(rep_od - 1)] = rep_od - 1
            already_used[exp_cond_cfu + '_' + str(a0)] = True
            already_used_od[exp_cond_cfu + '_' + str(a0)] = True

    exps_for_fit = []  # cond.size + strains_oi.size
    # _, mono = np.array_split(np.array(exps[0].exps), 2)
    ''' 
    1/ mono + 1 r2 
    2/ mono + 3r2 : t2, t4, t6 
    3/ all of the experiments 

    for mono use all rep 2 , for the rest separate the replicates but use both (not sumultaneously) 
    '''
    # mono + 1r2
    for i in range(cond.size):
        exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps + exps[2 * (i + 1)].exps))
        exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps + exps[2 * (i + 1) + 1].exps))

    # mono + 3r2
    indexes_2 = [ind for key, ind in exps_ref_index.items() if 'at2' in key]
    indexes_4 = [ind for key, ind in exps_ref_index.items() if 'at4' in key]
    indexes_6 = [ind for key, ind in exps_ref_index.items() if 'at6' in key]

    exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps +
                                         exps[indexes_2[0]].exps +
                                         exps[indexes_4[0]].exps +
                                         exps[indexes_6[0]].exps))
    exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps +
                                         exps[indexes_2[1]].exps +
                                         exps[indexes_4[1]].exps +
                                         exps[indexes_6[1]].exps))

    exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps +
                                         [e for ind in range(2, len(exps), 2) for e in exps[ind].exps]))
    exps_for_fit.append(esbl.Experiments(exps[exps_ref_index['mono_1']].exps +
                                         [e for ind in range(3, len(exps), 2) for e in exps[ind].exps]))

    ### PLOT ###
    ns = 3
    media_plots = np.array(['glucose+tween0.1%'])
    concentrations = np.unique(a0s.astype(int))
    colors = plt.cm.tab20(np.linspace(0, 1, 20)) #max(20, concentrations.size)))
    # cond_ddose_plot = cond
    nm = cond.size + 1
    ns = 2
    f1, ax1 = plt.subplots(ns, nm, sharex=True, sharey='row', figsize=(18, 4*ns))
    for k in range(len(exps)):
        for exp in exps[k].exps:
            if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                # exp_cond = np.array2string(exp.volsinjs, separator='+')[1:-1] + 'at' + np.array2string(np.array(exp.timesinjs), separator='+')[1:-1]
                exp_cond = str(exp.volsinjs[0]) + 'at' + str(exp.timesinjs[0])
                exp_cond_cfu = str(int(1000 * exp.concinj / exp.v0 * exp.volsinjs[0])) + 'at' + str(
                    (int(exp.timesinjs[0])))
                label = ''
                for i in range(exp.timesinjs.size):
                    # exp_cond += str(exp.volsinjs[i]) + 'at' + str(exp.timesinjs[i])
                    label += (i > 0) * '+' + str(int(1000 * exp.concinj / exp.v0 * exp.volsinjs[i])) + 'at' + str((int(exp.timesinjs[i])))
                ind_c = np.where(np.array(cond) == exp_cond)[0][0] + 1
                for t in exp.timesinjs:
                    ax1[0, ind_c].axvline(x=t, color='red', linestyle='--', linewidth=2)
                ax1[0, ind_c].set_title(label)
            else:
                ind_c = 0
                exp_cond = 'mono'
                exp_cond_cfu = 'mono'
            sns.scatterplot(ax = ax1[0, ind_c], x = exp.times, y = exp.ods, s=6, label=exp.a0,
                              color=colors[2 * np.where(concentrations == int(exp.a0))[0][0] + k % 2], legend = ind_c == nm - 1).set(yscale = 'log', ylim = (2e-4, 1), xlim = (0, 25))
            sns.lineplot(ax=ax1[1, ind_c],
                         x=exp.timescfus, y=exp.cfus, color = colors[2 * np.where(concentrations == int(exp.a0))[0][0] + k % 2]).set(yscale='log',
                                                                                                 ylim=(1e2, 5e9),
                                                                                                 xlim=(0, 25))
            # sns.lineplot(ax = ax1[1, ind_c], data = cfu_data[(cfu_data['exp_cond'] == exp_cond_cfu) & (cfu_data['CFU.well'] > 0)],
            #                 x = 'time', y = 'CFU.well', estimator="median", errorbar = None,  #err_style="band",
            #              # err_kws={'linestyle': '--', 'hatch': 'none', 'fc': 'none'},
            #              hue = 'conc', style = 'rep', palette = 'tab10', legend  = (ind_c == nm - 1)).set(yscale = 'log', ylim = (1e2, 5e9), xlim = (0, 25))
            ax1[1, ind_c].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1, ind_c].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

    sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol = 2)

    f1.tight_layout()
    f1.savefig('nils18/experiments_double_dose_2_N18_1.png')


    # fits_summary = pd.read_excel('nils18/fits_summary_n18_new_data_1.xlsx', header = 0, index_col = 0, sheet_name='15')
    fits_summary = pd.read_excel('fits_summary_all_strains_ddose.xlsx', header = 0, index_col = 0, sheet_name='2501_cfu1')
    # # fits_summary = pd.read_excel('/mnt/y/cluster/fits_summary_clustercomb.xlsx', sheet_name='2', header=0, index_col=0)
    # # fits_summary = res_fits
    paramlist = esbl.params.BOUNDS.keys() #(fits_summary.columns).to_numpy()[7:]
    # # # fits_summary = fits_summary.drop_duplicates(subset=['cost', 'cost_all'] + list(paramlist))
    # fit_names = (fits_summary['fitname']).to_numpy()
    # exp_conds = np.array(list(exps_ref_index.keys()))
    ind_cfu = np.where(np.array(fits_summary.columns) == 'cost_cfu')[0][0]

    # calculate cost_cfu
    for n in range(fits_summary.shape[0]):
        if fits_summary.iloc[n, :]['strain'] == strain:
            cost_cfu_mono = np.mean([exp.cost_cfu(
                fits_summary.iloc[n, :][paramlist].to_dict(),
                odejax=True) for exp in exps[exps_ref_index['mono_1']].exps])
            fits_summary.iloc[n, ind_cfu] = cost_cfu_mono

    # # with pd.ExcelWriter('/mnt/y/cluster/fits_summary_clustercomb.xlsx', mode='a') as writer:
    with pd.ExcelWriter('fits_summary_all_strains_ddose.xlsx', mode='a') as writer:
        fits_summary.to_excel(writer, sheet_name='2501_cfu2')

    # fits_summary = res_fits
    # paramlist = (fits_summary.columns).to_numpy()[7:]
    # fits_summary = fits_summary.drop_duplicates(subset = ['cost', 'cost_all'] + list(paramlist))
    # fits_summary = fits_summary[fits_summary['strain'] == strain]
    # # fits_summary = fits_summary[fits_summary['cost_all'] < 1000]
    # fit_names = (fits_summary['fitname']).to_numpy()

    # fit_names_1r = np.array([name for name in fit_names if 'mono' not in name and 'all' not in name])
    # fit_names_1r_1, fit_names_1r_2 = np.array_split(fit_names_1r, 2)
    # fit_names_sr = np.array([name for name in fit_names if name not in fit_names_1r])

    # plot OD simulations for all the fits on the same plot

    exp_conds = np.array(list(exps_ref_index.keys()))
    nm = exp_conds.size // 2
    # plot od and cfu predictions for top 5 fits
    # fits_selection = (fits_summary.sort_values(by ='cost_all')).head(5)
    timescfus = np.array([i for i in range(0, 13, 2)])
    # for k in range(strains_oi.size):
    # s = strains_oi[k]
    # fits = fits_selection[fits_selection['strain'] == s]
    nf = fits_summary.shape[0]
    indexes = np.array(
        [exps_ref_ind[s + '_mono']] + [ind for key, ind in exps_ref_ind.items() if s in key and 'mono' not in key])
    names = np.array([s + '_mono'] + [key for key, ind in exps_ref_ind.items() if s in key and 'mono' not in key])

    exp_ref = exps[exps_ref_index['mono_1']].exps[0]
    # times_end = exp_ref.timescfus[0]
    eta = exp_ref.od0 / exp_ref.cfus[0]  # exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))]

    nx = 2
    ny = exp_conds.size // 2
    exp_conds_unique = np.unique([s[:-2] for s in exp_conds])[::-1]
    for n in range(nf):
        fit = fits_summary.iloc[n, :][paramlist].to_dict()
        fitname = str(fits_summary.iloc[n, :]['fitname'])
        if not os.path.exists(os.path.join(os.getcwd(), 'nils18/plots_new_fits/' + fitname + '_all_cfu_pred.png')):
            f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx))
            f1.suptitle(strain + ', ' + fitname)
            # prediction results
            for i in range(exp_conds.size):
                if (exp_conds[i] != 'mono_0'):  # ((exp_conds[i] == 'mono_1') or ('_1' in fitname and '_1' in exp_conds[i]) or ('_0' in fitname and '_0' in exp_conds[i])) and
                    y = np.where(exp_conds_unique == exp_conds[i][:-2])[0][0]
                    for j in range(len(exps[exps_ref_index[exp_conds[i]]].exps)):
                        exp = exps[exps_ref_index[exp_conds[i]]].exps[j]
                        label = str(exp.a0)
                        if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                            exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                            for t in exp.timesinjs:
                                ax1[0, y].axvline(x=t, color='red', linestyle='--', linewidth=2)
                            ax1[0, y].set_title(exp_cond)
                        else:
                            ax1[0, y].set_title(exp_conds[i])

                        ind_c = 2 * np.where(concentrations == exp.a0)[0][0]
                        sns.scatterplot(ax=ax1[0, y], x=exp.times, y=exp.ods, s=10, label=label,
                                        color=colors[ind_c], legend=(y == ny - 1)).set(yscale='log',
                                                                                       ylim=(2e-4, 1.2),
                                                                                       xlim=(0, 27))

                        sns.lineplot(ax=ax1[1, y], x=exp.timescfus, y=exp.cfus,
                                     label=label if j <len(exps[exps_ref_index[exp_conds[i]]].exps) // 2 else None,
                                     color=colors[ind_c], legend=False ).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 13))

                        ax1[1, y].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
                        ax1[1, y].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')


                        solts = exp.simulate(fit, odejax=True)
                        ods = solts[0] * solts[1] + solts[5] + solts[6]
                        sns.lineplot(ax=ax1[0, y], x=exp.times, y=ods, linewidth=3, alpha=0.6, linestyle='--',
                                     label=str(exp.a0) + ',sim', color=colors[ind_c], legend=(y == ny - 1))

                        times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                        ods = solts[0] * solts[1] + solts[5] + solts[6]
                        cfus = solts[0] / eta  # fit['eta']
                        cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])

                        sns.lineplot(ax=ax1[1, y], x=times_selected_points, y=cfus_selected_points, color=colors[ind_c],
                                     linewidth=2, legend=False,
                                     linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))


                    # y += 1

            sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)

            f1.tight_layout()
            f1.subplots_adjust(wspace=0.2)
            f1.savefig('nils18/plots_new_fits/' + fitname + '_all_cfu_pred.png')
            f1.savefig('nils18/plots_new_fits/' + fitname + '_all_cfu_pred.svg')
            plt.close()

    # cfu of delayed treatments
    # first row plot all fitting data set
    # second row OD + CFU for delayed treatments
    # only for fits on mono+3r2

    nx = 2
    ny = 4 #exp_conds.size // 2
    exp_conds_unique = np.unique([s[:-2] for s in exp_conds])[::-1]
    paramlist = list(esbl.params.BOUNDS.keys())
    exp_conds_to_plot = np.array([c for c in exp_conds_unique if 'mono' in c or 'at2' in c or 'at4' in c or 'at6' in c])
    fits_summary_all = pd.read_excel('fits_summary_all_strains_ddose.xlsx', header = 0, index_col = 0, sheet_name='full_2501')

    fits_summary_all = fits_summary_all[(fits_summary_all.strain == strain) & (fits_summary_all.fit_type == 'mono+3r2') & (fits_summary_all.exp_cond.isin(['mono+64at2.11+64at4.04+64at6.0', 'mono+64at2+64at4+64at6']))]
    nf = fits_summary_all.shape[0]

    plt.rcParams['svg.fonttype'] = 'none'
    # sns.set(rc={'axes.labelsize': 14})
    sns.set(font_scale=2)
    # sns.set_theme(style='white')
    sns.set(rc={'axes.labelsize': 16,
                'axes.facecolor': 'white', 'figure.facecolor': 'white', 'axes.edgecolor': 'black',
                'axes.spines.right': True, 'axes.spines.top': False,
                'axes.grid': True, 'grid.color': 'lightgrey',
                'xtick.color': 'black', 'ytick.color': 'black', })

    for n in range(nf):
        f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx))
        fit = fits_summary_all.iloc[n, :][paramlist].to_dict()
        fitname = str(fits_summary_all.iloc[n, :]['fitname'])
        f1.suptitle(strain + ', ' + fitname)
        for i in range(exp_conds_unique.size):
            # fitting results
            if exp_conds_unique[i] in exp_conds_to_plot:
                y = np.where(exp_conds_to_plot == exp_conds_unique[i])[0][0]

                for j in range(len(exps[exps_ref_index[exp_conds_unique[i] + '_1']].exps)):
                    exp = exps[exps_ref_index[exp_conds_unique[i] + '_1']].exps[j]
                    label = str(exp.a0)
                    if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                        exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                        for t in exp.timesinjs:
                            ax1[0, y].axvline(x=t, color='red', linestyle='--', linewidth=2)
                        ax1[0, y].set_title(exp_cond)
                    else:
                        ax1[0, y].set_title(exp_conds[i])

                    ind_c = 2 * np.where(concentrations == exp.a0)[0][0]
                    sns.scatterplot(ax=ax1[0, y], x=exp.times, y=exp.ods, s=10, label=label,
                                    color=colors[ind_c], legend=(y == ny - 1)).set(yscale='log',
                                                                                   ylim=(2e-4, 1.2),
                                                                                   xlim=(0, 27))
                    solts = exp.simulate(fit, odejax=True)
                    ods = solts[0] * solts[1] + solts[5] + solts[6]
                    sns.lineplot(ax=ax1[0, y], x=exp.times, y=ods, linewidth=3, alpha=0.6, linestyle='--',
                                 label=str(exp.a0) + ',sim', color=colors[ind_c], legend=(y == ny - 1))

            # delayed treatment
            # ind_c = 2 * np.where(concentrations == exp.a0)[0][0]
            color = sns.color_palette("husl")[i]
            if exp_conds_unique[i] != 'mono':
                exp = exps[exps_ref_index[exp_conds_unique[i] + '_1']].exps[0]
                label = str(int(exp.volsinjs[0] * exp.concinj * 5)) + 'mg/L at ' + str(int(exp.timesinjs[0])) + 'h'
            else:
                exp = [e for e in exps[exps_ref_index[exp_conds_unique[i] + '_1']].exps if e.a0 == 64][0]
                label = str(int(exp.a0)) + 'mg/L at 0h'
            sns.scatterplot(ax=ax1[1, 0], x=exp.times, y=exp.ods, s=10, #label=exp_conds_unique[i],
                            color = color).set(yscale='log',ylim=(2e-4, 1.2),xlim=(0, 25))
            sns.lineplot(ax=ax1[1, 1], x=exp.timescfus, y=exp.cfus,
                         label= label, #exp_conds_unique[i],
                         color=color, legend=True).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 13))
            sns.scatterplot(ax=ax1[1, 1], x=exp.timescfus, y=exp.cfus, color=color)
            # ax1[1, 1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            # ax1[1, 1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1, 2].set_axis_off()
            ax1[1, 3].set_axis_off()

            solts = exp.simulate(fit, odejax=True)
            ods = solts[0] * solts[1] + solts[5] + solts[6]
            sns.lineplot(ax=ax1[1, 0], x=exp.times, y=ods, linewidth=3, alpha=0.6, linestyle='--',
                          color=color)

            times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
            ods = solts[0] * solts[1] + solts[5] + solts[6]
            cfus = solts[0] / eta  # fit['eta']
            cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])

            sns.lineplot(ax=ax1[1, 1], x=times_selected_points, y=cfus_selected_points, color=color,
                         linewidth=2, legend=False,
                         linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))

        sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)
        sns.move_legend(ax1[1,1], "upper left", bbox_to_anchor=(1, 1), ncol=2)

        f1.tight_layout()
        f1.subplots_adjust(wspace=0.2)
        f1.savefig('nils18/plots_delayed_treatments/' + fitname + '_cfu_delayed_full_od_newcolor.png')
        f1.savefig('nils18/plots_delayed_treatments/' + fitname + '_cfu_delayed_full_od_newcolor.svg')
        plt.close()
