import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp
from collections import OrderedDict
import yaml
import os
import glob
from pathlib import Path
import seaborn as sns
import itertools


if __name__ == "__main__":
    ### GET EXPERIMENTAL DATA ###
    strain_oi = 'IB311'
    filenames = ['experimental_data_N18_IB311.xlsx', '20230712_OD_IB311_ddose.xlsx', '20231109_OD.xlsx']
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    xlsx = pd.read_excel(os.path.join(path_data, filenames[0]))
    times = xlsx["Unnamed: 0"].to_numpy()[5:].astype(np.float64)
    strains = xlsx.iloc[0, :].to_numpy()[1:]
    a0s = xlsx.iloc[1, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[5:, 1:].astype(np.float64)
    cond = np.unique(np.array([str(int(volsinjs[i] * 5. * concinjs[i])) + 'at' + str(timesinjs[i]) for i in range(timesinjs.size) if
                               type(timesinjs[i]) == str or np.isfinite(timesinjs[i])]))
    cond = cond[cond != 'nanatnan']

    strains_unique = np.unique(strains)
    concentrations = np.unique(a0s)
    n_exps = (cond.size + 2)

    exps = [esbl.Experiments() for i in range(n_exps)] # save only IB311
    exps_ind_ref = {}

    for i in range(wells.shape[1]):
        strain = strains[i]
        # if strain == 'IB311':
        a0 = a0s[i]
        tinjs = timesinjs[i]
        vinjs = volsinjs[i]
        concinj = concinjs[i]
        # ods = esbl.utils.smooth(wells.iloc[:, i])
        ods = wells.iloc[:, i].to_numpy().astype(np.float64)
        mask = np.isnan(ods) == 0
        try:
            ods = esbl.utils.smooth(ods[mask])
        except:
            ods = ods[mask]
        ind_s = np.where(strains_unique == strain)[0][0]
        if type(tinjs) == str or np.isfinite(tinjs):
            exp_cond = str(int(vinjs * 5 * concinj)) + 'at' + str(tinjs)
            exp_cond_ref = str(int(vinjs * 5 * concinj)) + 'at' + str(round(tinjs))
            ind = np.where(cond == exp_cond)[0][0] + 1

            if '+' in str(tinjs):
                tinjs_1 = np.array(list(tinjs.split('+'))).astype(np.float64)
                vinjs_1 = np.array(list(vinjs.split('+'))).astype(np.float64)
                exp = esbl.Experiment(a0, od0, 1, times[mask], ods, #[mask],
                                      v0=v0, volsinjs=vinjs_1, concinj=concinj, timesinjs=tinjs_1, strain = strain)
            else:
                exp = esbl.Experiment(a0, od0, 1, times[mask], ods, #[mask],
                                      v0=v0, volsinjs=np.array([vinjs]).astype(np.float64), concinj=concinj,
                                      timesinjs=np.array([tinjs]).astype(np.float64), strain = strain)
            exps[ind + ind_s].add(exp)
            exps_ind_ref[strain + '_' + exp_cond_ref] = ind + ind_s
                # print(ind + ind_s)
                # print(exp.timesinjs, exp.volsinjs, exp.concinj)
        else:
            exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain = strain) #[mask])
            exps[(cond.size // 2 + 1)  * ind_s].add(exp)
            exps_ind_ref[strain + '_mono'] = (cond.size // 2 + 1)  * ind_s

    # print(exps_ind_ref)
    exps_all_ddose_ib311 = esbl.Experiments([e for es in exps for e in es.exps if e.strain == strain_oi])
    exp_control_opt = exps[-1]

    # get data from last experiment
    xlsx = pd.read_excel(os.path.join(path_data, filenames[1]))
    times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
    strains = xlsx.iloc[0, :].to_numpy()[1:]
    media = xlsx.iloc[1, :].to_numpy()[1:]
    a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[6:, 1:].astype(np.float64)

    strains_unique = np.unique(strains)
    concentrations = np.union1d(concentrations, np.unique(a0s))
    # print(np.unique(strains), np.unique(a0s))

    exps.append(esbl.Experiments())

    for i in range(wells.shape[1]):
        strain = strains[i]
        if strain == 'IB311':
            a0 = a0s[i]
            tinjs = timesinjs[i]
            vinjs = volsinjs[i]
            concinj = concinjs[i]
            # ods = esbl.utils.smooth(wells.iloc[:, i])
            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]
            if type(tinjs) == str or np.isfinite(tinjs):
                if '+' in str(tinjs):
                    tinjs_1 = np.array(list(tinjs.split('+'))).astype(np.float64)
                    vinjs_1 = np.array(list(vinjs.split('+'))).astype(np.float64)
                    exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                          v0=v0, volsinjs=vinjs_1, concinj=concinj, timesinjs=tinjs_1, strain=strain)
                else:
                    exp = esbl.Experiment(a0, od0, 1, times[mask], ods,  # [mask],
                                          v0=v0, volsinjs=np.array([vinjs]).astype(np.float64), concinj=concinj,
                                          timesinjs=np.array([tinjs]).astype(np.float64), strain=strain)
                exps[-1].add(exp)
            else:
                exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[-1].add(exp)


    exp_list = []
    strain_oi = 'IB311'

    # add experiment with CFU
    xlsx = pd.read_excel(os.path.join(path_data, filenames[2]), sheet_name='OD')
    cfu_data = pd.read_excel(os.path.join(path_data, filenames[2]), sheet_name='CFU', header=0, index_col=0)

    times = xlsx["Unnamed: 0"].to_numpy()[6:].astype(np.float64)
    strains = xlsx.iloc[0, :].to_numpy()[1:]
    medias = xlsx.iloc[1, :].to_numpy()[1:]
    a0s = xlsx.iloc[2, :].to_numpy()[1:].astype(np.float64)
    timesinjs = xlsx.iloc[3, :].to_numpy()[1:].astype(np.float64)
    volsinjs = xlsx.iloc[4, :].to_numpy()[1:].astype(np.float64)
    concinjs = xlsx.iloc[5, :].to_numpy()[1:].astype(np.float64)
    od0 = 5e-4
    v0 = 200
    wells = xlsx.iloc[6:, 1:].astype(np.float64)
    # print(wells.shape, a0s.size)
    strains_unique = np.unique(strains)
    concentrations = np.unique(a0s)

    # print(np.unique(strains), np.unique(a0s))

    n_exps1 = len(exps)
    exps = exps + [esbl.Experiments(), esbl.Experiments()]# for i in range(strains_oi.size)]
    already_used = {strain_oi + '_' + str(c): False for c in np.unique(a0s)}
    for i in range(wells.shape[1] // 2):
        strain = strains[i]
        if strain == strain_oi:
            a0 = a0s[i]

            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]
            ind_s = int(already_used[strain + '_' + str(a0)]) #np.where(strains_oi == strain)[0][0]

            cfu_c = cfu_data[(cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                        cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) & (cfu_data['CFU.well'] > 0)]
            # print(cfu_c)
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
            # print(times_cfus, cfus, cfu_c.groupby('time')['CFU.well'].std().to_numpy())

            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus=times_cfus,
                                          cfus=cfus,
                                          stdcfus=cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + ind_s].add(exp)
                exps_ind_ref[strain + '_monocfu' + str(int(already_used[strain + '_' + str(a0)]))] = n_exps1 + ind_s
            already_used[strain + '_' + str(a0)] = True

    # ### PLOT ###
    ns = 2
    nm = (len(exps) + 1) // ns
    f1, ax1 = plt.subplots(ns, nm, sharex=True, sharey='row', figsize=(5 * nm, 5 * ns))
    media_plots = np.array(['glucose+tween0.1%'])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    for k in range(len(exps)):
        for j in range(len(exps[k].exps)):
            exp = exps[k].exps[j]
            ind_c = 0
            label = str(exp.a0)
            if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                label = ''
                for i in range(exp.timesinjs.size):
                    label += (i > 0) * '+' + str(int(1000 * exp.concinj / exp.v0 * exp.volsinjs[i])) + 'at' + str(int(exp.timesinjs[i]))
                for t in exp.timesinjs:
                    ax1[k // nm, k % nm].axvline(x=t, color='red', label='treat', linestyle='--', linewidth=2)
                ax1[k // nm, k % nm].set_title(exp_cond)
            # ax1[k // nm, k % nm].scatter(exp.times, exp.ods, s=4, label=exp.a0,
            #                   color=colors[np.where(concentrations == int(exp.a0))[0][0]])
            sns.scatterplot(ax = ax1[k // nm, k % nm], x = exp.times, y = exp.ods, s = 6, label = label,
                            color = colors[np.where(concentrations == exp.a0)]).set_yscale('log')

    # metrics = ['OD, classic', 'OD, center']
    for i in range(ax1.shape[0]):
        for j in range(ax1.shape[1]):
            ax1[i, j].set_yscale('log')
            ax1[i, j].set_ylim(2 * 10 ** (-4), 1.2)
            ax1[i, j].set_xlim(0, 25)
            ax1[i, j].tick_params(axis='both', which='major', labelsize=12)
            ax1[i, j].set_xlabel('time, h', fontsize=12)
            handles, labels = ax1[i, j].get_legend_handles_labels()
            by_label = OrderedDict(zip(labels, handles))
            ax1[i, j].legend(by_label.values(), by_label.keys(), loc='lower right', markerscale=3, fontsize=12, ncol=2)

    f1.tight_layout()
    f1.savefig('ib311/experiments_ib311.png')
    plt.close()

    f, ax = plt.subplots(2, 1, figsize = (5, 10))
    for j in range(len(exps[exps_ind_ref['IB311_monocfu1']].exps)):
        exp = exps[exps_ind_ref['IB311_monocfu1']].exps[j]
        ind_c = 0
        label = str(exp.a0)
        sns.scatterplot(ax=ax[0], x=exp.times, y=exp.ods, s=6, label=label,
                        color=colors[np.where(concentrations == exp.a0)])
        ax[1].plot(exp.timescfus, exp.cfus, label=label,
                        color=colors[np.where(concentrations == exp.a0)])
        sns.scatterplot(ax=ax[1], x=exp.timescfus, y=exp.cfus, s=6, label=label,
                    color=colors[np.where(concentrations == exp.a0)])

    ax[0].set(yscale = 'log', ylim = (2e-4, 1))
    ax[1].set(yscale = 'log', ylim = (1e2, 1e10))
    f.tight_layout()
    f.savefig('ib311/ib311_monocfu.png')


    # Run fits on mono, mono + 1r2, mono + 3r2
    exps_for_fits = []
    names_for_fits = []
    strain_oi = 'IB311'

    #mono
    exps_for_fits.append(exps[exps_ind_ref[strain_oi + '_mono']])
    names_for_fits.append(strain_oi + '_mono')

    #mono + 1r2
    ddose_indexes = [ind for key, ind in exps_ind_ref.items() if strain_oi in key and 'at' in key]
    ddose_names = [key for key, ind in exps_ind_ref.items() if strain_oi in key and 'at' in key]
    # print(ddose_names)
    for i in range(len(ddose_indexes)):
        exps_for_fits.append(esbl.Experiments(
            exps[exps_ind_ref[strain_oi + '_mono']].exps +
            exps[ddose_indexes[i]].exps
        ))
        _, cond = ddose_names[i].split('_')
        names_for_fits.append(strain_oi + '_mono+' + cond)

    #mono + 3r2
    ddose_combinations = itertools.combinations(ddose_names, 3)
    for r1, r2, r3 in ddose_combinations:
        exps_for_fits.append(esbl.Experiments(exps[exps_ind_ref[strain_oi + '_mono']].exps +
                                             exps[exps_ind_ref[r1]].exps +
                                             exps[exps_ind_ref[r2]].exps +
                                             exps[exps_ind_ref[r3]].exps))
        _, n1 = r1.split('_')
        _, n2 = r2.split('_')
        _, n3 = r3.split('_')
        names_for_fits.append(strain_oi + '_mono+' + n1 + '+' + n2 + '+' + n3)

    names_for_fits = np.array(names_for_fits)


    fits_summary = pd.read_excel(os.path.join(path_fits, 'fits_summary_all_strains_ddose.xlsx'), sheet_name='2102_cfu1', header = 0, index_col=0)
    paramlist = list(esbl.params.BOUNDS.keys())
    exp_ref = exps[exps_ind_ref[strain_oi + '_monocfu1']].exps[0]
    times_end = exp_ref.timescfus[0]
    eta = exp_ref.od0 / exp_ref.cfus[0]

    # fits_selection = fits_summary[fits_summary.strain == strain_oi]
    # ind_cfu = np.where(fits_selection.columns.to_numpy() == 'cost_cfu')[0][0]
    # for i in range(fits_selection.shape[0]):
    #     sols = fits_selection.iloc[i, :][paramlist].to_dict()
    #     print(sols)
    #     sols['eta'] = eta
    #     fits_selection.iloc[i, ind_cfu] = np.mean([exp.cost_cfu(sols, odejax=True)
    #                                                      for exp in exps[exps_ind_ref[strain_oi + '_monocfu1']].exps])
    #     print(fits_selection.iloc[i, ind_cfu])
    #
    # with pd.ExcelWriter('fits_summary_all_strains_ddose.xlsx', mode='a') as writer:
    #     fits_selection.to_excel(writer, sheet_name='ib311_new_costcfu_1_eta')

    fit_names = (fits_summary['fitname']).to_numpy()
    ind_cfu = np.where(np.array(fits_summary.columns) == 'cost_cfu')[0][0]

    # calculate cost_cfu
    for n in range(fits_summary.shape[0]):
        if (fits_summary.iloc[n, :]['strain'] == strain_oi) and (np.isnan(fits_summary.iloc[n, ind_cfu])):  # == strain:
            cost_cfu = np.mean([exp.cost_cfu(fits_summary.iloc[n, :][paramlist].to_dict(),
                                             odejax=True) for exp in exps[exps_ind_ref[strain_oi + '_monocfu1']].exps])
            fits_summary.iloc[n, ind_cfu] = cost_cfu

    # print(fits_summary[fits_summary['strain'] == strain_oi])
    with pd.ExcelWriter('fits_summary_all_strains_ddose.xlsx', mode='w') as writer:
        fits_summary.to_excel(writer, sheet_name='2102_cfu2')



    nf = fits_summary.shape[0]
    exp_ref = exps[exps_ind_ref[strain_oi + '_monocfu1']].exps[0]
    times_end = exp_ref.timescfus[0]
    eta = exp_ref.od0 / exp_ref.cfus[0]  # exp_ref.ods[np.argmin(np.abs(exp_ref.times - times_end))]

    indexes = np.array(
        [exps_ind_ref[strain_oi + '_mono']] + [ind for key, ind in exps_ind_ref.items() if strain_oi in key and 'mono' not in key])
    names = np.array([strain_oi + '_mono'] + [key for key, ind in exps_ind_ref.items() if strain_oi in key and 'mono' not in key])

    nx = 2
    ny = indexes.size
    for n in range(nf):
        fit = fits_summary.iloc[n, :][paramlist].to_dict()
        fitname = str(fits_summary.iloc[n, :]['fitname'])
        if not os.path.exists(os.path.join(os.getcwd(), 'ib311/plots/' + fitname + '_cfu.svg')):
            f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx))
            f1.suptitle(strain_oi + ', ' + fitname)

            # double dose simulations
            for i in range(indexes.size):
                for j in range(len(exps[indexes[i]].exps)):
                    exp = exps[indexes[i]].exps[j]
                    label = str(exp.a0)
                    if exp.timesinjs is not None and (np.array(exp.timesinjs)).size > 0:
                        exp_cond = str(int(exp.volsinjs[0] * 5 * exp.concinj)) + 'at' + str(exp.timesinjs[0])
                        for t in exp.timesinjs:
                            ax1[0, i].axvline(x=t, color='red', linestyle='--', linewidth=2)
                        ax1[0, i].set_title(exp_cond)
                    else:
                        ax1[0, i].set_title(names[i])

                    ind_c = np.where(concentrations == exp.a0)[0][0]
                    sns.scatterplot(ax=ax1[0, i], x=exp.times, y=exp.ods, s=10, label=label,
                                    color=colors[ind_c], legend=(i == ny - 1)).set(yscale='log',
                                                                                   ylim=(2e-4, 1.2),
                                                                                   xlim=(0, 27))

                    solts = exp.simulate(fit, odejax=True)
                    ods = solts[0] * solts[1] + solts[5] + solts[6]
                    ax1[0, i].plot(exp.times, ods, linewidth=3, alpha=0.6, linestyle='--',
                                 label=str(exp.a0) + ',sim' if i == ny - 1 else None , color=colors[ind_c])


            # cfu predictions
            for j in range(len(exps[exps_ind_ref[strain_oi + '_monocfu1']].exps)):
                exp = exps[exps_ind_ref[strain_oi + '_monocfu1']].exps[j]
                label = str(exp.a0)
                ind_c = (np.where(concentrations == exp.a0)[0][0])

                sns.scatterplot(ax=ax1[1, 0], x=exp.times, y=exp.ods, s=10,
                                label=label,
                                color=colors[ind_c], legend=False)
                ax1[1, 0].set(yscale='log', ylim=(2e-4, 1.2),
                                                                       xlim=(0, 13), title='monodose, OD')

                ax1[1, 1].plot(exp.timescfus, exp.cfus,
                             label=label,
                             color=colors[ind_c])

                ax1[1, 1].set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 13),
                                                                   title='monodose, CFU')
                ax1[1, 1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
                ax1[1, 1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

                # if (j < len(exps[i].exps) // 2 and 'monocfu_0_' in fitname) or (j >= len(exps[i].exps) // 2 and 'monocfu_1_' in fitname):

                solts = exp.simulate(fit, odejax=True)
                times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                ods = solts[0] * solts[1] + solts[5] + solts[6]
                cfus = solts[0] / eta  # fit['eta']
                cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                # sns.lineplot(ax=ax1[1, 0], x=exp.times, y=ods, color=colors[ind_c], linewidth=3, linestyle='--',
                #              legend=False)
                # sns.lineplot(ax=ax1[1, 1], x=times_selected_points, y=cfus_selected_points, color=colors[ind_c],
                #              linewidth=2, legend=False, linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))
                ax1[1, 0].plot(exp.times, ods, color=colors[ind_c], linewidth=3, linestyle='--')
                ax1[1, 1].plot(times_selected_points, cfus_selected_points, color=colors[ind_c],
                             linewidth=2, linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))

            for y in range(2, ax1.shape[1]):
                ax1[1, y].set_axis_off()
            # sns.move_legend(ax1[1, 1], "upper left", bbox_to_anchor=(1, 1))

            # sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1), ncol=2)

            f1.tight_layout()
            f1.subplots_adjust(wspace=0.2)
            f1.savefig('ib311/plots/' + fitname + '_cfu_1.png')
            f1.savefig('ib311/plots/' + fitname + '_cfu_1.svg')
            plt.close()

