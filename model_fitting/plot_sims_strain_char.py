import esbl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
import yaml
import multiprocessing as mp
from pathlib import Path
import os
import seaborn as sns
import glob

import warnings
warnings.filterwarnings('ignore', category = RuntimeWarning)
plt.rcParams['svg.fonttype'] = 'none'


def run_search(out_q, experiment, params={}, fixed={}, odejax = True, method = 'cmaes', samples = 500, include_cfus = False,
               media_condition='IB302_glucose+tween', verb_filenameprefix='outcmaes_parallel_new/'):

    # run fits
    if method == 'cmaes':
        if params != {}:
            sols = experiment.search(params=params, fixed = fixed, method=method,
                                 odejax=odejax, verbose=1, include_cfus=include_cfus,
                                 media_condition=media_condition, verb_filenameprefix=verb_filenameprefix)
        else:
            sols = experiment.search(fixed=fixed, method=method,
                                     odejax=odejax, verbose=1, include_cfus=include_cfus,
                                     media_condition=media_condition, verb_filenameprefix=verb_filenameprefix)
    elif method == 'lhs':
        if params != {}:
            sols = experiment.search(params=params, fixed=fixed, method=method, samples=samples, include_cfus=include_cfus,
                                 odejax=odejax, verbose=1, media_condition=media_condition)
        else:
            sols = experiment.search(fixed=fixed, method=method, samples = samples, include_cfus=include_cfus,
                                     odejax=odejax, verbose=1, media_condition=media_condition)
    out_q.put(sols)

    return sols


if __name__ == "__main__":
    path_data = os.path.join(os.path.dirname(__file__), '../consolidated_exp_data/')
    path_fits = os.path.join(os.path.dirname(__file__), '../consolidated_fitting_results/')

    filename = ['20230627_OD.xlsx', '20230706_OD.xlsx', '20230907_OD.xlsx', '20231109_OD.xlsx']
    exps_ref_ind = {}
    strains_oi = np.array(['NILS1', 'NILS11', 'NILS12', 'NILS19',
                           'NILS42', 'NILS56',
                           'IB306', 'IB307', 'IB308', 'IB302', 'IB311', 'NILS31',
                           'NILS64'])
    exps = []
    n_exps1 = 0

    for f in range(len(filename)):
        # elif f == 1: cfu_data = pd.read_excel(filename[f], sheet_name='CFU_1', header=0, index_col=0)
        if f == 2:
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD_1', header=0)
        elif f < 2:
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU_2', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD', header=0)
        else:
            cfu_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='CFU', header=0, index_col=0)
            od_data = pd.read_excel(os.path.join(path_data, filenames[f]), sheet_name='OD', header=0)

        times = od_data["Unnamed: 0"].to_numpy()[6:].astype(np.float64)

        strains = od_data.iloc[0, :].to_numpy()[1:]
        strains_unique = np.unique(strains)
        if f < 2:
            a0s = od_data.iloc[1, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[2, :].to_numpy()[1:]
        else:
            a0s = od_data.iloc[2, :].to_numpy()[1:].astype(np.float64)
            media = od_data.iloc[1, :].to_numpy()[1:]
        # timesinjs = xlsx.iloc[2, :].to_numpy()[1:]
        # volsinjs = xlsx.iloc[3, :].to_numpy()[1:]
        # concinjs = xlsx.iloc[4, :].to_numpy()[1:]
        od0 = 5e-4
        v0 = 200
        wells = od_data.iloc[6:, 1:].astype(np.float64)
        for i in range(2 * strains_unique.size):
            exps.append(esbl.Experiments())

        already_used = {s + '_' + str(a0): False for s in strains_unique for a0 in np.unique(a0s)}

        bound = wells.shape[1] if f < 3 else wells.shape[1] // 2

        for i in range(min(96, bound)):
            strain = strains[i]
            a0 = a0s[i]
            m = media[i]

            ods = wells.iloc[:, i].to_numpy().astype(np.float64)
            mask = np.isnan(ods) == 0
            try:
                ods = esbl.utils.smooth(ods[mask])
            except:
                ods = ods[mask]

            ind_s = 2 * np.where(strains_unique == strain)[0][0] + int(already_used[strain + '_' + str(a0)])

            cfu_c = cfu_data[(cfu_data['strain'] == strain) & (cfu_data['conc'] == a0) & (
                        cfu_data['rep'] == 1 + int(already_used[strain + '_' + str(a0)])) &
                             (cfu_data['CFU.well'] > 0) & (cfu_data['time'] > 0)]
            # print(cfu_c)
            # times_cfus = np.unique(cfu_c['time'].to_numpy())
            temp = cfu_c.groupby('time')[['time', 'CFU.well']].median()
            times_cfus, cfus = temp['time'].to_numpy(), temp['CFU.well'].to_numpy()
            stdcfus = (cfu_c.groupby('time')[['time', 'CFU.well']].std())['CFU.well'].to_numpy()
            # print(times_cfus, cfus, cfu_c.groupby('time')['CFU.well'].std().to_numpy())

            if np.sum(mask) > 0:
                if times_cfus.size > 0:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain, timescfus=times_cfus,
                                          cfus=cfus,
                                          stdcfus=cfu_c.groupby('time')['CFU.well'].std().to_numpy())  # [mask])
                else:
                    exp = esbl.Experiment(a0, 5e-4, 1.0, times[mask], ods, strain=strain)  # [mask])
                exps[n_exps1 + ind_s].add(exp)
                exps_ref_ind[strain + '_monocfu' + str(int(already_used[strain + '_' + str(a0)]))] = n_exps1 + ind_s
            else:
                print(strain, a0, i)
            already_used[strain + '_' + str(a0)] = True

        n_exps1 += 2 * strains_unique.size

    ### PLOT ###
    nx = 2
    ny = strains_oi.size
    concentrations = np.array([0] + [2 ** i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    fig, ax1 = plt.subplots(nx, ny, sharey = False, sharex = False, figsize = (4 * ny, 4 * nx))
    for k in range(strains_oi.size):
        i = exps_ref_ind[strains_oi[k] + '_monocfu0']
        for j in range (len(exps[i].exps)):
            exp = exps[i].exps[j]
            sns.scatterplot(ax=ax1[0, k], x=exp.times, y=exp.ods, s=4, label=exp.a0,
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=k == ny - 1).set(
                yscale='log', ylim=(2e-4, 1), xlim=(0, 25), title=strains_oi[k])
            sns.lineplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale='log', ylim=(1e2, 5e9),
                                                                                          xlim=(0, 25))
            sns.scatterplot(ax=ax1[1, k], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]])
            ax1[1, k].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1, k].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

    sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1))
    fig.tight_layout()
    fig.savefig('plots/experiments_cfus.png')
    plt.close()

    nx = 2
    ny = 1  # strains_oi.size
    concentrations = np.array([0] + [2 ** i for i in range(1, 9)])
    colors = plt.cm.tab20(np.linspace(0, 1, max(10, concentrations.size)))
    fig, ax1 = plt.subplots(nx, ny, sharey=False, sharex=False, figsize=(4 * ny, 5 * nx))

    sns.reset_defaults()
    sns.set(style='whitegrid')
    # sns.set(font_scale=1.5)
    strain = 'NILS42'
    conc_to_plot = np.array([0])
    i = exps_ref_ind[strain + '_monocfu1']

    for j in range(len(exps[i].exps)):
        exp = exps[i].exps[j]
        if exp.a0 in conc_to_plot:
            sns.scatterplot(ax=ax1[0], x=exp.times, y=exp.ods, s=8, label=exp.a0,
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=True).set(
                yscale='log', ylim=(2e-4, 1), xlim=(0, 14), title=strain)
            sns.lineplot(ax=ax1[1], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale='log', ylim=(1e2, 5e9),
                                                                                          xlim=(0, 14))
            ax1[1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

    conc_to_plot = np.array([8, 64])
    i = exps_ref_ind[strain + '_monocfu0']

    for j in range(len(exps[i].exps)):
        exp = exps[i].exps[j]
        if exp.a0 in conc_to_plot:
            sns.scatterplot(ax=ax1[0], x=exp.times, y=exp.ods, s=8, label=exp.a0,
                            color=colors[np.where(concentrations == int(exp.a0))[0][0]], legend=True).set(
                xlabel = 'time', ylabel = 'OD', yscale='log', ylim=(2e-4, 1), xlim=(0, 14), title=strain)
            sns.lineplot(ax=ax1[1], x=exp.timescfus, y=exp.cfus,
                         color=colors[np.where(concentrations == int(exp.a0))[0][0]]).set(yscale='log', ylim=(1e2, 5e9),
                                                                                          xlabel = 'time', ylabel = 'CFU',
                                                                                          xlim=(0, 14))
            ax1[1].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
            ax1[1].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

    sns.move_legend(ax1[0], "upper left", bbox_to_anchor=(1, 1))
    fig.tight_layout()
    fig.savefig('nils42.png')
    fig.savefig('nils42.svg')


    # strains_oi = np.array(['NILS1', 'NILS11', 'NILS12', 'NILS19', 'NILS42', 'NILS56',
    #                        'IB306', 'IB307', 'IB308', 'IB302', 'IB311', 'NILS31'])

    max_conc = {'NILS1': 8, 'NILS11': 8, 'NILS12': 32, 'NILS19': 16, 'NILS42': 64, 'IB311': 128, 'NILS31': 128}
    exps_for_fit = []
    exps_for_fit_growth = []
    names_for_fit = []
    for s in strains_oi:
        if s not in max_conc.keys():
            exps_for_fit.append(exps[exps_ref_ind[s + '_monocfu1']])
        else:
            exps_for_fit.append(esbl.Experiments([
                e for e in exps[exps_ref_ind[s + '_monocfu1']] if e.a0 <= max_conc[s]
            ]))
        exps_for_fit_growth.append(esbl.Experiments([
            e for e in exps[exps_ref_ind[s + '_monocfu1']] if e.a0 == 0]))
        names_for_fit.append(s + '_monocfu1')

    names_for_fit = np.array(names_for_fit)
    # strains_to_fit = np.array(['IB311', 'NILS64'])
    strains_to_fit = strains_oi


    ### 1. Summarize fits generated only on growth curve without antibitiotic
    res_fits = []
    # eta = {strains_oi[i] : exps[exps_ref_ind[strains_oi[i] + '_monocfu0']].exps[0].od0 / exps[exps_ref_ind[strains_oi[i] + '_monocfu0']].exps[0].cfus[0] for i in range(strains_oi.size)}
    # print(eta)

    for p in sorted(Path(os.path.join(os.getcwd(), 'fits_cfu_1')).glob('*_growth_*.yaml')):
        filename = os.path.basename(p)
        name_parts = list((filename.split('.yam')[0]).split('_'))
        print(name_parts)
        if len(name_parts) > 2:
            if len(name_parts) > 5:
                method, strain, exp_cond, _, rep = name_parts[1:]
                # exp_cond = exp_cond + '0'
                # exp_cond = exp_cond_n + '_' + rep
            elif len(name_parts) > 4:
                method, strain, exp_cond, rep = name_parts[1:]
                exp_rep = 1

            # print(strain, exp_rep)
            with open(p) as file:
                sols = yaml.safe_load(file)
            if sols is not None:
                cost = exps_for_fit_growth[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].cost_with_cfu(sols, odejax=True)
                cost_od = exps_for_fit_growth[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].cost(sols, odejax=True)
                cost_cfu = np.mean(
                        [exp.cost_cfu(sols, odejax=True) for exp in
                         exps_for_fit_growth[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].exps])

                res_fits.append(pd.DataFrame({'method' : method, 'strain': strain, 'fitname' : filename, 'cost' : cost, 'cost_od' : cost_od,
                                              'cost_cfu' : cost_cfu,
                                              # 'cost_k2=k1': cost_k2, 'cost_od_k2=k1': cost_od_k2,
                                              # 'cost_cfu_k2=k1': cost_cfu_k2,
                                              } | sols, index = [0]))

    res_fits = pd.concat(res_fits, ignore_index = True)
    # paramlist = np.array(res_fits.columns) [3:]
    # print(paramlist)

    with pd.ExcelWriter('fit_summary_monocfu.xlsx', mode = 'a') as writer:
        res_fits.to_excel(writer, sheet_name = 'growth_1')

    fits_summary = res_fits#[res_fits.cost < 500]
    # fits_summary = pd.read_excel('fit_summary_monocfu.xlsx', sheet_name='12', header = 0 )
    # # fits_summary = fits_summary[fits_summary.cost < 500]
    # print(fits_summary)
    paramlist = np.array(fits_summary.columns)[6:]
    # plt.rcParams.update({'font.size': 12})
    colors = plt.cm.tab10(np.linspace(0, 1, 10))

    for k in range(strains_to_fit.size):
        fits_strain = fits_summary[(fits_summary['strain'] == strains_to_fit[k]) & (fits_summary['cost'] < 1000) ]
        nf = fits_strain.shape[0]
        if nf > 0:
            nx = 2
            ny = nf + 1
            f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx), sharex=False, sharey=False)
            f1.suptitle(strains_to_fit[k])

            for j in range (len(exps_for_fit_growth[np.where(names_for_fit == strains_to_fit[k] + '_monocfu1')[0][0]].exps)): #(len(exps[exps_ref_ind[strains_oi[k] + '_monocfu1']].exps)): #exps_for_fit[np.where(names_for_fit == strains_oi[k] + '_monocfu1')[0][0]].exps)):
                exp = exps_for_fit_growth[np.where(names_for_fit == strains_to_fit[k] + '_monocfu1')[0][0]].exps[j]
                if exp.strain not in max_conc.keys() or exp.a0 <= max_conc[exp.strain]:
                    label = str(exp.a0) + 'mg/L'
                    ind_c = (np.where(concentrations == exp.a0)[0][0])

                    for ax in [ax1]: #, ax2]:
                        for y in range(ax.shape[1]):
                            sns.scatterplot(ax=ax[0, y], x=exp.times, y=exp.ods, s=10,
                                            label=label if y == (ax.shape[1] - 1) else None ,
                                            color=colors[ind_c], legend= y == ax.shape[1] - 1).set(yscale='log', ylim=(2e-4, 1.2),
                                                                                   xlim=(0, 13), xlabel = 'time', ylabel = 'OD')
                            sns.lineplot(ax=ax[1, y], x=exp.timescfus, y=exp.cfus,
                                         color=colors[ind_c], legend=False).set(yscale='log', ylim=(1e2, 5e9), xlim=(0, 13),
                                                                               xlabel = 'time', ylabel = 'CFU')
                            sns.scatterplot(ax=ax[1, y], x=exp.timescfus, y=exp.cfus,
                                         color=colors[ind_c], legend=False)
                            ax[1, y].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
                            ax[1, y].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')


                    for n in range(nf):
                        fit = fits_strain.iloc[n, :][paramlist].to_dict()
                        fitname = str(fits_strain.iloc[n, :]['fitname'])
                        fitname, _ = fitname.split('.')
                        _, method, strain, expcond, _, rep = fitname.split('_')
                        title = method + '_' + strain + '_' + rep
                        title += '\ncost OD+CFU:' + str(int(fits_strain.iloc[n, :]['cost']))
                        solts = exp.simulate(fit, odejax=True)
                        times_selected_points = np.array([exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                        ods = solts[0] * solts[1] + solts[5] + solts[6]
                        cfus = solts[0] / fit['eta']#eta[strains_oi[k]] #fit['eta']
                        cfus_selected_points = np.array([cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                        sns.lineplot(ax=ax1[0, n + 1], x=exp.times, y=ods, color=colors[ind_c], linewidth=3, linestyle='--').set(
                            title = title)#  + '\ncost OD+CFU:' + str(int(fits_strain.iloc[n, :]['cost_k2=k1'])) if fits_strain.iloc[n, :]['cost_k2=k1'] is not None else title)
                        sns.lineplot(ax=ax1[1, n + 1], x=times_selected_points, y=cfus_selected_points, color=colors[ind_c],
                                     linewidth=2, linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))

            sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1)) #, ncol=2)
            # sns.move_legend(ax2[0, -1], "upper left", bbox_to_anchor=(1, 1)) #, ncol=2)

            f1.tight_layout()
            f1.savefig('plots/fits_cfu_1/' + strains_to_fit[k] + '_cfu_growth_new.png')
            f1.savefig('plots/fits_cfu_1/' + strains_to_fit[k] + '_cfu_growth_new.svg', format = 'svg')
            plt.close()

        ### 2. Summarize fits generated on antibiotic response
        res_fits = []
        # eta = {strains_oi[i] : exps[exps_ref_ind[strains_oi[i] + '_monocfu0']].exps[0].od0 / exps[exps_ref_ind[strains_oi[i] + '_monocfu0']].exps[0].cfus[0] for i in range(strains_oi.size)}
        # print(eta)

        for p in sorted(Path(os.path.join(os.getcwd(), 'fits_cfu_1')).glob('*_fromgrowth_*.yaml')):
            filename = os.path.basename(p)
            name_parts = list((filename.split('.yam')[0]).split('_'))
            # print(name_parts)
            if len(name_parts) > 2:
                if len(name_parts) > 5:
                    method, strain, exp_cond, _, rep = name_parts[1:]
                    # exp_cond = exp_cond + '0'
                    # exp_cond = exp_cond_n + '_' + rep
                elif len(name_parts) > 4:
                    method, strain, exp_cond, rep = name_parts[1:]
                    exp_rep = 1

                # print(strain, exp_rep)
                with open(p) as file:
                    sols = yaml.safe_load(file)
                if sols is not None:
                    cost = exps_for_fit[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].cost_with_cfu(
                        sols, odejax=True)
                    cost_od = exps_for_fit[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].cost(sols,
                                                                                                                 odejax=True)
                    cost_cfu = np.mean(
                        [exp.cost_cfu(sols, odejax=True) for exp in
                         exps_for_fit[np.where(names_for_fit == strain + '_' + exp_cond)[0][0]].exps])

                    res_fits.append(pd.DataFrame(
                        {'method': method, 'strain': strain, 'fitname': filename, 'cost': cost, 'cost_od': cost_od,
                         'cost_cfu': cost_cfu,
                         } | sols, index=[0]))

        res_fits = pd.concat(res_fits, ignore_index=True)

        with pd.ExcelWriter('fit_summary_monocfu.xlsx', mode='a') as writer:
            res_fits.to_excel(writer, sheet_name='fromgrowth_1')

        fits_summary = res_fits  # [res_fits.cost < 500]
        # fits_summary = pd.read_excel('fit_summary_monocfu.xlsx', sheet_name='12', header = 0 )
        # # fits_summary = fits_summary[fits_summary.cost < 500]
        paramlist = np.array(fits_summary.columns)[6:]
        # print(paramlist)
        # plt.rcParams.update({'font.size': 12})
        colors = plt.cm.tab10(np.linspace(0, 1, 10))

        for k in range(strains_to_fit.size):
            fits_strain = fits_summary[(fits_summary['strain'] == strains_to_fit[k]) & (fits_summary['cost'] < 1000)]
            nf = fits_strain.shape[0]
            if nf > 0:
                nx = 2
                ny = nf + 1
                f1, ax1 = plt.subplots(nx, ny, figsize=(4 * ny, 4 * nx), sharex=False, sharey=False)
                f1.suptitle(strains_to_fit[k])

                for j in range(len(exps_for_fit[np.where(names_for_fit == strains_to_fit[k] + '_monocfu1')[0][0]].exps)):  # (len(exps[exps_ref_ind[strains_oi[k] + '_monocfu1']].exps)): #exps_for_fit[np.where(names_for_fit == strains_oi[k] + '_monocfu1')[0][0]].exps)):
                    exp = exps_for_fit[np.where(names_for_fit == strains_to_fit[k] + '_monocfu1')[0][0]].exps[j]
                    if exp.strain not in max_conc.keys() or exp.a0 <= max_conc[exp.strain]:
                        label = str(exp.a0) + 'mg/L'
                        ind_c = (np.where(concentrations == exp.a0)[0][0])

                        for ax in [ax1]:  # , ax2]:
                            for y in range(ax.shape[1]):
                                sns.scatterplot(ax=ax[0, y], x=exp.times, y=exp.ods, s=10,
                                                label=label if y == (ax.shape[1] - 1) else None,
                                                color=colors[ind_c], legend=y == ax.shape[1] - 1).set(yscale='log',
                                                                                                      ylim=(2e-4, 1.2),
                                                                                                      xlim=(0, 13),
                                                                                                      xlabel='time',
                                                                                                      ylabel='OD')
                                sns.lineplot(ax=ax[1, y], x=exp.timescfus, y=exp.cfus,
                                             color=colors[ind_c], legend=False).set(yscale='log', ylim=(1e2, 5e9),
                                                                                    xlim=(0, 13),
                                                                                    xlabel='time', ylabel='CFU')
                                sns.scatterplot(ax=ax[1, y], x=exp.timescfus, y=exp.cfus,
                                                color=colors[ind_c], legend=False)
                                ax[1, y].axhline(10 / 5 * 200, color='gray', linewidth=2, linestyle='--')
                                ax[1, y].axhline(25e6 / 5 * 200, color='gray', linewidth=2, linestyle='--')

                        for n in range(nf):
                            fit = fits_strain.iloc[n, :][paramlist].to_dict()
                            fitname = str(fits_strain.iloc[n, :]['fitname'])
                            fitname, _ = fitname.split('.')
                            _, method, strain, expcond, _, rep = fitname.split('_')
                            title = method + '_' + strain + '_' + rep
                            # if fits_strain.iloc[n, :]['cost_k2=k1'] is not None and np.isfinite(fits_strain.iloc[n, :]['cost_k2=k1']):
                            title += '\ncost OD+CFU:' + str(int(fits_strain.iloc[n, :]['cost']))
                            solts = exp.simulate(fit, odejax=True)
                            times_selected_points = np.array(
                                [exp.times[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                            ods = solts[0] * solts[1] + solts[5] + solts[6]
                            cfus = solts[0] / fit['eta']  # eta[strains_oi[k]] #fit['eta']
                            cfus_selected_points = np.array(
                                [cfus[np.abs(exp.times - t).argmin()] for t in exp.timescfus])
                            sns.lineplot(ax=ax1[0, n + 1], x=exp.times, y=ods, color=colors[ind_c], linewidth=3,
                                         linestyle='--').set(
                                title=title)  # + '\ncost OD+CFU:' + str(int(fits_strain.iloc[n, :]['cost_k2=k1'])) if fits_strain.iloc[n, :]['cost_k2=k1'] is not None else title)
                            sns.lineplot(ax=ax1[1, n + 1], x=times_selected_points, y=cfus_selected_points,
                                         color=colors[ind_c],
                                         linewidth=2,
                                         linestyle='--')  # .set(yscale='log', xlim=(0, 24), ylim=(1e2, 1e9))

                sns.move_legend(ax1[0, -1], "upper left", bbox_to_anchor=(1, 1))  # , ncol=2)
                # sns.move_legend(ax2[0, -1], "upper left", bbox_to_anchor=(1, 1)) #, ncol=2)

                f1.tight_layout()
                f1.savefig('plots/fits_cfu_1/' + strains_to_fit[k] + '_cfu_fromgrowth.png')
                f1.savefig('plots/fits_cfu_1/' + strains_oi[k] + '_cfu_fromgrowth_.svg')
                plt.close()