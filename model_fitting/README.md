# A quantitative approach to easily characterize and predict cell death and escape to beta-lactam treatments  

## About

This repository hosts the example scripts that were used for model fitting. 

## Structure 

This folder contains several parts: 
- run_fits_strain_char.py: script to fit the model on OD and CFU of single treatments 
- plot_sims_strain_char.py: script to combine individual fits and plot simulations. 
- run_fits_all_strains_all_comb.py: script to fit the model on OD of a combination of repeated treatments 
- summarize_fits_all_strains_all_comb.py: script to combine individual fits on repeated treatments into one excel file 

